<?php
// File name: C:\wamp64\www\onlinebiztraining\php\test.php
// Created by Access2PostgreSQL Pro http://www.data-conversions.net
//---------------------------------------------------------

// Increasing maximum exection time of the script.
set_time_limit(120);

// Connecting to PostgreSQL server.
$dbconn = pg_connect("host=localhost port=5432 dbname=template1 user=postgres password=1");
if( !$dbconn )
{
	die("Error:Could not connect");
};

if ( !pg_Exec( $dbconn, 'CREATE TABLE "actor" (  "act_id" integer NOT NULL  ,
  "act_fname" CHAR(20) NULL DEFAULT NULL ,
  "act_lname" CHAR(20) NULL DEFAULT NULL ,
  "act_gender" CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY ("act_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "address" (  "city" VARCHAR(15) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "soccer_country" (  "country_id" numeric NOT NULL  ,
  "country_abbr" VARCHAR(4) NOT NULL  ,
  "country_name" VARCHAR(40) NOT NULL  ,
  PRIMARY KEY ("country_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "bh" (  "name" VARCHAR(20) NOT NULL  ,
  "emp_id" integer NOT NULL  ,
  "process" VARCHAR(20) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "casino" (  "pricerange" numeric(6,0) NOT NULL  ,
  "events" VARCHAR(50) NULL DEFAULT NULL ,
  "location" VARCHAR(40) NOT NULL  ,
  "casino" VARCHAR(75) NOT NULL  ,
  PRIMARY KEY ("casino")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "coach_mast" (  "coach_id" numeric NOT NULL  ,
  "coach_name" VARCHAR(40) NOT NULL  ,
  PRIMARY KEY ("coach_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "col1" (  "?column?" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "company_mast" (  "com_id" integer NOT NULL  ,
  "com_name" VARCHAR(20) NOT NULL  ,
  PRIMARY KEY ("com_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "countries" (  "country_id" VARCHAR(2) NULL DEFAULT NULL ,
  "country_name" VARCHAR(40) NULL DEFAULT NULL ,
  "region_id" numeric(10,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "salesman" (  "salesman_id" numeric(5,0) NOT NULL  ,
  "name" VARCHAR(30) NOT NULL  ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "commission" numeric(5,2) NULL DEFAULT NULL ,
  PRIMARY KEY ("salesman_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "customer_backup" (  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "departments" (  "department_id" numeric(4,0) NOT NULL  ,
  "department_name" VARCHAR(30) NOT NULL  ,
  "manager_id" numeric(6,0) NULL DEFAULT 0 ,
  "location_id" numeric(4,0) NULL DEFAULT 0 ,
  PRIMARY KEY ("department_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "director" (  "dir_id" integer NOT NULL  ,
  "dir_fname" CHAR(20) NULL DEFAULT NULL ,
  "dir_lname" CHAR(20) NULL DEFAULT NULL ,
  PRIMARY KEY ("dir_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "elephants" (  "id" integer NOT NULL  ,
  "name" text NULL DEFAULT NULL ,
  "date" date NULL DEFAULT NULL ,
  PRIMARY KEY ("id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "emp" (  "ename" VARCHAR(13) NULL DEFAULT NULL ,
  "salary" numeric(6,0) NULL DEFAULT NULL ,
  "eid" numeric(3,0) NOT NULL  ,
  PRIMARY KEY ("eid")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "emp_department" (  "dpt_code" integer NOT NULL  ,
  "dpt_name" CHAR(15) NOT NULL  ,
  "dpt_allotment" integer NOT NULL  ,
  PRIMARY KEY ("dpt_code")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "emp_details" (  "emp_idno" integer NOT NULL  ,
  "emp_fname" CHAR(15) NOT NULL  ,
  "emp_lname" CHAR(15) NOT NULL  ,
  "emp_dept" integer NOT NULL  ,
  PRIMARY KEY ("emp_idno"),FOREIGN KEY ("emp_dept") REFERENCES "emp_department" ( "dpt_code" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "employee" (  "empno" integer NULL DEFAULT NULL ,
  "ename" VARCHAR(20) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "employees" (  "employee_id" numeric(6,0) NOT NULL DEFAULT 0 ,
  "first_name" VARCHAR(20) NULL DEFAULT \'NULL::character varying\' ,
  "last_name" VARCHAR(25) NOT NULL  ,
  "email" VARCHAR(25) NOT NULL  ,
  "phone_number" VARCHAR(20) NULL DEFAULT \'NULL::character varying\' ,
  "hire_date" date NOT NULL  ,
  "job_id" VARCHAR(10) NOT NULL  ,
  "salary" numeric(8,2) NULL DEFAULT 0 ,
  "commission_pct" numeric(2,2) NULL DEFAULT 0 ,
  "manager_id" numeric(6,0) NULL DEFAULT 0 ,
  "department_id" numeric(4,0) NULL DEFAULT 0 ,
  PRIMARY KEY ("employee_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "game_scores" (  "id" integer NOT NULL  ,
  "name" text NULL DEFAULT NULL ,
  "score" integer NULL DEFAULT NULL ,
  PRIMARY KEY ("id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "genres" (  "gen_id" integer NOT NULL  ,
  "gen_title" CHAR(20) NULL DEFAULT NULL ,
  PRIMARY KEY ("gen_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "playing_position" (  "position_id" CHAR(2) NOT NULL  ,
  "position_desc" VARCHAR(15) NOT NULL  ,
  PRIMARY KEY ("position_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "grade" (  "city" VARCHAR(15) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "grades" (  "id" integer NOT NULL  ,
  "grade_1" integer NULL DEFAULT NULL ,
  "grade_2" integer NULL DEFAULT NULL ,
  "grade_3" integer NULL DEFAULT NULL ,
  PRIMARY KEY ("id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "hello" (  "number_one" integer NULL DEFAULT NULL ,
  "number_two" integer NULL DEFAULT NULL ,
  "number_three" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "hello1_1122" (  "abc" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "hello1_12" (  "abc" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "hello_12" (  "abc" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "item_mast" (  "pro_id" integer NOT NULL  ,
  "pro_name" VARCHAR(25) NOT NULL  ,
  "pro_price" numeric(8,2) NOT NULL  ,
  "pro_com" integer NOT NULL  ,
  PRIMARY KEY ("pro_id"),FOREIGN KEY ("pro_com") REFERENCES "company_mast" ( "com_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "job_history" (  "employee_id" numeric(6,0) NOT NULL  ,
  "start_date" date NOT NULL  ,
  "end_date" date NOT NULL  ,
  "job_id" VARCHAR(10) NOT NULL  ,
  "department_id" numeric(4,0) NULL DEFAULT 0 ,
  PRIMARY KEY ("start_date","employee_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "jobs" (  "job_id" VARCHAR(10) NOT NULL  ,
  "job_title" VARCHAR(35) NOT NULL  ,
  "min_salary" numeric(6,0) NULL DEFAULT 0 ,
  "max_salary" numeric(6,0) NULL DEFAULT 0 ,
  PRIMARY KEY ("job_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "locations" (  "location_id" numeric(4,0) NOT NULL DEFAULT 0 ,
  "street_address" VARCHAR(40) NULL DEFAULT \'NULL::character varying\' ,
  "postal_code" VARCHAR(12) NULL DEFAULT \'NULL::character varying\' ,
  "city" VARCHAR(30) NOT NULL  ,
  "state_province" VARCHAR(25) NULL DEFAULT \'NULL::character varying\' ,
  "country_id" VARCHAR(2) NULL DEFAULT \'NULL::character varying\' ,
  PRIMARY KEY ("location_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "player_mast" (  "player_id" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "jersey_no" numeric NOT NULL  ,
  "player_name" VARCHAR(40) NOT NULL  ,
  "posi_to_play" CHAR(2) NOT NULL  ,
  "dt_of_bir" date NULL DEFAULT NULL ,
  "age" numeric NULL DEFAULT NULL ,
  "playing_club" VARCHAR(40) NULL DEFAULT NULL ,
  PRIMARY KEY ("player_id"),FOREIGN KEY ("posi_to_play") REFERENCES "playing_position" ( "position_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "referee_mast" (  "referee_id" numeric NOT NULL  ,
  "referee_name" VARCHAR(40) NOT NULL  ,
  "country_id" numeric NOT NULL  ,
  PRIMARY KEY ("referee_id"),FOREIGN KEY ("country_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "soccer_city" (  "city_id" numeric NOT NULL  ,
  "city" VARCHAR(25) NOT NULL  ,
  "country_id" numeric NOT NULL  ,
  PRIMARY KEY ("city_id"),FOREIGN KEY ("country_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "maxim00" (  "num" integer NULL DEFAULT NULL ,
  "name" VARCHAR(10) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "maximum" (  "num" integer NULL DEFAULT NULL ,
  "name" VARCHAR(10) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "maximum00" (  "num" integer NULL DEFAULT NULL ,
  "name" VARCHAR(10) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "maximum899" (  "num" integer NULL DEFAULT NULL ,
  "name" VARCHAR(10) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "movie" (  "mov_id" integer NOT NULL  ,
  "mov_title" CHAR(50) NULL DEFAULT NULL ,
  "mov_year" integer NULL DEFAULT NULL ,
  "mov_time" integer NULL DEFAULT NULL ,
  "mov_lang" CHAR(15) NULL DEFAULT NULL ,
  "mov_dt_rel" date NULL DEFAULT NULL ,
  "mov_rel_country" CHAR(5) NULL DEFAULT NULL ,
  PRIMARY KEY ("mov_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "movie_cast" (  "act_id" integer NOT NULL  ,
  "mov_id" integer NOT NULL  ,
  "role" CHAR(30) NULL DEFAULT NULL ,FOREIGN KEY ("act_id") REFERENCES "actor" ( "act_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("mov_id") REFERENCES "movie" ( "mov_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "movie_direction" (  "dir_id" integer NOT NULL  ,
  "mov_id" integer NOT NULL  ,FOREIGN KEY ("dir_id") REFERENCES "director" ( "dir_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("mov_id") REFERENCES "movie" ( "mov_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "movie_genres" (  "mov_id" integer NOT NULL  ,
  "gen_id" integer NOT NULL  ,FOREIGN KEY ("gen_id") REFERENCES "genres" ( "gen_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("mov_id") REFERENCES "movie" ( "mov_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "mytest" (  "ord_num" numeric(6,0) NOT NULL  ,
  "ord_amount" numeric(12,2) NULL DEFAULT NULL ,
  "ord_date" date NOT NULL  ,
  "cust_code" CHAR(6) NOT NULL  ,
  "agent_code" CHAR(6) NOT NULL  ,
  UNIQUE ("ord_num")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "mytest1" (  "ord_num" numeric(6,0) NOT NULL  ,
  "ord_amount" numeric(12,2) NULL DEFAULT NULL ,
  "ord_date" date NOT NULL  ,
  "cust_code" CHAR(6) NOT NULL  ,
  "agent_code" CHAR(6) NOT NULL  ,
  UNIQUE ("ord_num")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "new" (  "city" VARCHAR(15) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "new123" (  "Customer" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "Salesman" VARCHAR(30) NULL DEFAULT NULL ,
  "commission" numeric(5,2) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "new_table" (  "salesman_id" numeric(5,0) NULL DEFAULT NULL ,
  "name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "commission" numeric(5,2) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "newtab" (  "ord_no" numeric(5,0) NULL DEFAULT NULL ,
  "purch_amt" numeric(8,2) NULL DEFAULT NULL ,
  "ord_date" date NULL DEFAULT NULL ,
  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "nobel_win" (  "year" integer NULL DEFAULT NULL ,
  "subject" CHAR(25) NULL DEFAULT NULL ,
  "winner" CHAR(45) NULL DEFAULT NULL ,
  "country" CHAR(25) NULL DEFAULT NULL ,
  "category" CHAR(25) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "nros" (  "uno" integer NULL DEFAULT NULL ,
  "dos" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "nuevo" (  "city" VARCHAR(15) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "numbers" (  "one" integer NULL DEFAULT NULL ,
  "two" integer NULL DEFAULT NULL ,
  "three" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "numeri" (  "id" integer NULL DEFAULT NULL ,
  "data" date NULL DEFAULT NULL ,
  "decimali" real NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "numeros" (  "uno" integer NULL DEFAULT NULL ,
  "dos" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "customer" (  "customer_id" numeric(5,0) NOT NULL  ,
  "cust_name" VARCHAR(30) NOT NULL  ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT 0 ,
  "salesman_id" numeric(5,0) NOT NULL  ,
  PRIMARY KEY ("customer_id"),FOREIGN KEY ("salesman_id") REFERENCES "salesman" ( "salesman_id" ) ON UPDATE CASCADE ON DELETE CASCADE
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "orozco" (  "city" VARCHAR(15) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "partest1" (  "ord_num" numeric(6,0) NOT NULL  ,
  "ord_amount" numeric(12,2) NULL DEFAULT NULL ,
  "ord_date" date NOT NULL  ,
  "cust_code" CHAR(6) NOT NULL  ,
  "agent_code" CHAR(6) NOT NULL  ,
  UNIQUE ("ord_num")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "participant" (  "participant_id" integer NOT NULL  ,
  "part_name" VARCHAR(20) NOT NULL  
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "participants" (  "participant_id" integer NOT NULL  ,
  "part_name" VARCHAR(20) NOT NULL  
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "soccer_venue" (  "venue_id" numeric NOT NULL  ,
  "venue_name" VARCHAR(30) NOT NULL  ,
  "city_id" numeric NOT NULL  ,
  "aud_capacity" numeric NOT NULL  ,
  PRIMARY KEY ("venue_id"),FOREIGN KEY ("city_id") REFERENCES "soccer_city" ( "city_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "match_mast" (  "match_no" numeric NOT NULL  ,
  "play_stage" CHAR(1) NOT NULL  ,
  "play_date" date NOT NULL  ,
  "results" CHAR(5) NOT NULL  ,
  "decided_by" CHAR(1) NOT NULL  ,
  "goal_score" CHAR(5) NOT NULL  ,
  "venue_id" numeric NOT NULL  ,
  "referee_id" numeric NOT NULL  ,
  "audence" numeric NOT NULL  ,
  "plr_of_match" numeric NOT NULL  ,
  "stop1_sec" numeric NOT NULL  ,
  "stop2_sec" numeric NOT NULL  ,
  PRIMARY KEY ("match_no"),FOREIGN KEY ("plr_of_match") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("referee_id") REFERENCES "referee_mast" ( "referee_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("venue_id") REFERENCES "soccer_venue" ( "venue_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "persons" (  "personid" integer NULL DEFAULT NULL ,
  "lastname" VARCHAR(255) NULL DEFAULT NULL ,
  "firstname" VARCHAR(255) NULL DEFAULT NULL ,
  "address" VARCHAR(255) NULL DEFAULT NULL ,
  "city" VARCHAR(255) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "player_in_out" (  "match_no" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "player_id" numeric NOT NULL  ,
  "in_out" CHAR(1) NOT NULL  ,
  "time_in_out" numeric NOT NULL  ,
  "play_schedule" CHAR(2) NOT NULL  ,
  "play_half" numeric NOT NULL  ,FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_id") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "goal_details" (  "goal_id" numeric NOT NULL  ,
  "match_no" numeric NOT NULL  ,
  "player_id" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "goal_time" numeric NOT NULL  ,
  "goal_type" CHAR(1) NOT NULL  ,
  "play_stage" CHAR(1) NOT NULL  ,
  "goal_schedule" CHAR(2) NOT NULL  ,
  "goal_half" numeric NULL DEFAULT NULL ,
  PRIMARY KEY ("goal_id"),FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_id") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "match_captain" (  "match_no" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "player_captain" numeric NOT NULL  ,FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_captain") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "asst_referee_mast" (  "ass_ref_id" numeric NOT NULL  ,
  "ass_ref_name" VARCHAR(40) NOT NULL  ,
  "country_id" numeric NOT NULL  ,
  PRIMARY KEY ("ass_ref_id"),FOREIGN KEY ("country_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "reviewer" (  "rev_id" integer NOT NULL  ,
  "rev_name" CHAR(30) NULL DEFAULT NULL ,
  PRIMARY KEY ("rev_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "penalty_gk" (  "match_no" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "player_gk" numeric NOT NULL  ,FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_gk") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "regions" (  "region_id" numeric(10,0) NOT NULL  ,
  "region_name" CHAR(25) NULL DEFAULT NULL ,
  PRIMARY KEY ("region_id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "related" (  "city" VARCHAR(15) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "rating" (  "mov_id" integer NOT NULL  ,
  "rev_id" integer NOT NULL  ,
  "rev_stars" numeric(4,2) NULL DEFAULT NULL ,
  "num_o_ratings" integer NULL DEFAULT NULL ,FOREIGN KEY ("mov_id") REFERENCES "movie" ( "mov_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("rev_id") REFERENCES "reviewer" ( "rev_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "orders" (  "ord_no" numeric(5,0) NOT NULL  ,
  "purch_amt" numeric(8,2) NULL DEFAULT 0 ,
  "ord_date" date NULL DEFAULT NULL ,
  "customer_id" numeric(5,0) NOT NULL  ,
  "salesman_id" numeric(5,0) NOT NULL  ,
  PRIMARY KEY ("ord_no"),FOREIGN KEY ("customer_id") REFERENCES "customer" ( "customer_id" ) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY ("salesman_id") REFERENCES "salesman" ( "salesman_id" ) ON UPDATE CASCADE ON DELETE CASCADE
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "salesman_do1304" (  "salesman_id" VARCHAR(20) NULL DEFAULT NULL ,
  "name" VARCHAR(40) NULL DEFAULT NULL ,
  "city" VARCHAR(30) NULL DEFAULT NULL ,
  "commission" VARCHAR(10) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "sample_table" (  "salesman_id" CHAR(4) NULL DEFAULT NULL ,
  "name" VARCHAR(20) NULL DEFAULT NULL ,
  "city" VARCHAR(20) NULL DEFAULT NULL ,
  "commission" CHAR(10) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "scores" (  "id" integer NOT NULL  ,
  "score" integer NULL DEFAULT NULL ,
  PRIMARY KEY ("id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "penalty_shootout" (  "kick_id" numeric NOT NULL  ,
  "match_no" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "player_id" numeric NOT NULL  ,
  "score_goal" CHAR(1) NOT NULL  ,
  "kick_no" numeric NOT NULL  ,
  PRIMARY KEY ("kick_id"),FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_id") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "soccer_team" (  "team_id" numeric NOT NULL  ,
  "team_group" CHAR(1) NOT NULL  ,
  "match_played" numeric NOT NULL  ,
  "won" numeric NOT NULL  ,
  "draw" numeric NOT NULL  ,
  "lost" numeric NOT NULL  ,
  "goal_for" numeric NOT NULL  ,
  "goal_agnst" numeric NOT NULL  ,
  "goal_diff" numeric NOT NULL  ,
  "points" numeric NOT NULL  ,
  "group_position" numeric NOT NULL  ,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "player_booked" (  "match_no" numeric NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "player_id" numeric NOT NULL  ,
  "booking_time" numeric NOT NULL  ,
  "sent_off" CHAR(1) NULL DEFAULT \'NULL::bpchar\' ,
  "play_schedule" CHAR(2) NOT NULL  ,
  "play_half" numeric NOT NULL  ,FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_id") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "match_details" (  "match_no" numeric NOT NULL  ,
  "play_stage" CHAR(1) NOT NULL  ,
  "team_id" numeric NOT NULL  ,
  "win_lose" CHAR(1) NOT NULL  ,
  "decided_by" CHAR(1) NOT NULL  ,
  "goal_score" numeric NOT NULL  ,
  "penalty_score" numeric NULL DEFAULT NULL ,
  "ass_ref" numeric NOT NULL  ,
  "player_gk" numeric NOT NULL  ,FOREIGN KEY ("ass_ref") REFERENCES "asst_referee_mast" ( "ass_ref_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("match_no") REFERENCES "match_mast" ( "match_no" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("player_gk") REFERENCES "player_mast" ( "player_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "statements" (  "name" text NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "student" (  "roll" integer NULL DEFAULT NULL ,
  "name" VARCHAR NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "student1" (  "roll" integer NULL DEFAULT NULL ,
  "name" VARCHAR NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "sybba" (  "ord_num" numeric(6,0) NOT NULL  ,
  "ord_amount" numeric(12,2) NULL DEFAULT NULL ,
  "ord_date" date NOT NULL  ,
  "cust_code" CHAR(6) NOT NULL  ,
  "agent_code" CHAR(6) NOT NULL  ,
  UNIQUE ("ord_num")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "team_coaches" (  "team_id" numeric NOT NULL  ,
  "coach_id" numeric NOT NULL  ,FOREIGN KEY ("coach_id") REFERENCES "coach_mast" ( "coach_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION,FOREIGN KEY ("team_id") REFERENCES "soccer_country" ( "country_id" ) ON UPDATE NO ACTION ON DELETE NO ACTION
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "temp" (  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "tempa" (  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "tempcustomer" (  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "temphi" (  "salesman_id" numeric(5,0) NULL DEFAULT NULL ,
  "name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "commission" numeric(5,2) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "tempp" (  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "tempp11" (  "customer_id" numeric(5,0) NULL DEFAULT NULL ,
  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "grade" numeric(3,0) NULL DEFAULT NULL ,
  "salesman_id" numeric(5,0) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "tempsalesman" (  "salesman_id" numeric(5,0) NULL DEFAULT NULL ,
  "name" VARCHAR(30) NULL DEFAULT NULL ,
  "city" VARCHAR(15) NULL DEFAULT NULL ,
  "commission" numeric(5,2) NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "testtable" (  "col1" VARCHAR(30) NOT NULL  
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "trenta" (  "numeri" integer NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "vowl" (  "cust_name" VARCHAR(30) NULL DEFAULT NULL ,
  "substring" text NULL DEFAULT NULL 
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'CREATE TABLE "zebras" (  "id" integer NOT NULL  ,
  "score" integer NULL DEFAULT NULL ,
  "date" date NULL DEFAULT NULL ,
  PRIMARY KEY ("id")
); 
' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `actor`.
if ( !pg_Exec( $dbconn, 'INSERT INTO actor (\'act_id\',\'act_fname\',\'act_lname\',\'act_gender\') VALUES (101,\'James\',\'Stewart\',\'M\'),(102,\'Deborah\',\'Kerr\',\'F\'),(103,\'Peter\',\'OToole\',\'M\'),(104,\'Robert\',\'DeNiro\',\'M\'),(105,\'F.Murray\',\'Abraham\',\'M\'),(106,\'Harrison\',\'Ford\',\'M\'),(107,\'Nicole\',\'Kidman\',\'F\'),(108,\'Stephen\',\'Baldwin\',\'M\'),(109,\'Jack\',\'Nicholson\',\'M\'),(110,\'Mark\',\'Wahlberg\',\'M\'),(111,\'Woody\',\'Allen\',\'M\'),(112,\'Claire\',\'Danes\',\'F\'),(113,\'Tim\',\'Robbins\',\'M\'),(114,\'Kevin\',\'Spacey\',\'M\'),(115,\'Kate\',\'Winslet\',\'F\'),(116,\'Robin\',\'Williams\',\'M\'),(117,\'Jon\',\'Voight\',\'M\'),(118,\'Ewan\',\'McGregor\',\'M\'),(119,\'Christian\',\'Bale\',\'M\'),(120,\'Maggie\',\'Gyllenhaal\',\'F\'),(121,\'Dev\',\'Patel\',\'M\'),(122,\'Sigourney\',\'Weaver\',\'F\'),(123,\'David\',\'Aston\',\'M\'),(124,\'Ali\',\'Astin\',\'F\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `address`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "address" ("city") VALUES (\'New York\'),(\'New York\'),(\'Moncow\'),(\'California\'),(\'London\'),(\'Paris\'),(\'Berlin\'),(\'London\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `soccer_country`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "soccer_country" ("country_id","country_abbr","country_name") VALUES (1201,\'ALB\',\'Albania\'),(1202,\'AUT\',\'Austria\'),(1203,\'BEL\',\'Belgium\'),(1204,\'CRO\',\'Croatia\'),(1205,\'CZE\',\'Czech Republic\'),(1206,\'ENG\',\'England\'),(1207,\'FRA\',\'France\'),(1208,\'GER\',\'Germany\'),(1209,\'HUN\',\'Hungary\'),(1210,\'ISL\',\'Iceland\'),(1211,\'ITA\',\'Italy\'),(1212,\'NIR\',\'Northern Ireland\'),(1213,\'POL\',\'Poland\'),(1214,\'POR\',\'Portugal\'),(1215,\'IRL\',\'Republic of Ireland\'),(1216,\'ROU\',\'Romania\'),(1217,\'RUS\',\'Russia\'),(1218,\'SVK\',\'Slovakia\'),(1219,\'ESP\',\'Spain\'),(1220,\'SWE\',\'Sweden\'),(1221,\'SUI\',\'Switzerland\'),(1222,\'TUR\',\'Turkey\'),(1223,\'UKR\',\'Ukraine\'),(1224,\'WAL\',\'Wales\'),(1225,\'SLO\',\'Slovenia\'),(1226,\'NED\',\'Netherlands\'),(1227,\'SRB\',\'Serbia\'),(1228,\'SCO\',\'Scotland\'),(1229,\'NOR\',\'Norway\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `bh`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "bh" ("name","emp_id","process") VALUES (\'Pandey\',333445,\'Mobile\'),(\'Bhupinder\',301144,\'Mobile\'),(\'Kapil\',301146,\'Mobile\'),(\'Sanjay\',301147,\'Mobile\'),(\'Manjeet\',301145,\'Mobile\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `coach_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "coach_mast" ("coach_id","coach_name") VALUES (5550,\'Gianni De Biasi\'),(5551,\'Marcel Koller\'),(5552,\'Marc Wilmots\'),(5553,\'Ante Cacic\'),(5554,\'Pavel Vrba\'),(5555,\'Roy Hodgson\'),(5556,\'Didier Deschamps\'),(5557,\'Joachim Low\'),(5558,\'Bernd Storck\'),(5559,\'Lars Lagerback\'),(5560,\'Heimir Hallgr�msson\'),(5561,\'Antonio Conte\'),(5562,\'Michael ONeill\'),(5563,\'Adam Nawalka\'),(5564,\'Fernando Santos\'),(5565,\'Martin ONeill\'),(5566,\'Anghel Iordanescu\'),(5567,\'Leonid Slutski\'),(5568,\'Jan Kozak\'),(5569,\'Vicente del Bosque\'),(5570,\'Erik Hamren\'),(5571,\'Vladimir Petkovic\'),(5572,\'Fatih Terim\'),(5573,\'Mykhailo Fomenko\'),(5574,\'Chris Coleman\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `col1`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "col1" ("?column?") VALUES (1);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `company_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "company_mast" ("com_id","com_name") VALUES (11,\'Samsung\'),(12,\'iBall\'),(13,\'Epsion\'),(14,\'Zebronics\'),(15,\'Asus\'),(16,\'Frontech\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `countries`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "countries" ("country_id","country_name","region_id") VALUES (\'AR\',\'Argentina\',2),(\'AU\',\'Australia\',3),(\'BE\',\'Belgium\',1),(\'BR\',\'Brazil\',2),(\'CA\',\'Canada\',2),(\'CH\',\'Switzerland\',1),(\'CN\',\'China\',3),(\'DE\',\'Germany\',1),(\'DK\',\'Denmark\',1),(\'EG\',\'Egypt\',4),(\'FR\',\'France\',1),(\'HK\',\'HongKong\',3),(\'IL\',\'Israel\',4),(\'IN\',\'India\',3),(\'IT\',\'Italy\',1),(\'JP\',\'Japan\',3),(\'KW\',\'Kuwait\',4),(\'MX\',\'Mexico\',2),(\'NG\',\'Nigeria\',4),(\'NL\',\'Netherlands\',1),(\'SG\',\'Singapore\',3),(\'UK\',\'United Kingdom\',1),(\'US\',\'United States of America\',2),(\'ZM\',\'Zambia\',4),(\'ZW\',\'Zimbabwe\',4);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `salesman`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "salesman" ("salesman_id","name","city","commission") VALUES (5001,\'James Hoog\',\'New York\',0.15),(5002,\'Nail Knite\',\'Paris\',0.13),(5005,\'Pit Alex\',\'London\',0.11),(5006,\'Mc Lyon\',\'Paris\',0.14),(5007,\'Paul Adam\',\'Rome\',0.13),(5003,\'Lauson Hen\',\'San Jose\',0.12);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `customer_backup`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "customer_backup" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3007,\'Brad Davis\',\'New York\',200,5001),(3003,\'Jozy Altidor\',\'Moncow\',200,5007),(3005,\'Graham Zusi\',\'California\',200,5002),(3008,\'Julian Green\',\'London\',300,5002),(3004,\'Fabian Johnson\',\'Paris\',300,5006),(3009,\'Geoff Cameron\',\'Berlin\',100,5003),(3001,\'Brad Guzan\',\'London\',NULL,5005);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `departments`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "departments" ("department_id","department_name","manager_id","location_id") VALUES (10,\'Administration\',200,1700),(20,\'Marketing\',201,1800),(30,\'Purchasing\',114,1700),(40,\'Human Resources\',203,2400),(50,\'Shipping\',121,1500),(60,\'IT\',103,1400),(70,\'Public Relations\',204,2700),(80,\'Sales\',145,2500),(90,\'Executive\',100,1700),(100,\'Finance\',108,1700),(110,\'Accounting\',205,1700),(120,\'Treasury\',0,1700),(130,\'Corporate Tax\',0,1700),(140,\'Control And Credit\',0,1700),(150,\'Shareholder Services\',0,1700),(160,\'Benefits\',0,1700),(170,\'Manufacturing\',0,1700),(180,\'Construction\',0,1700),(190,\'Contracting\',0,1700),(200,\'Operations\',0,1700),(210,\'IT Support\',0,1700),(220,\'NOC\',0,1700),(230,\'IT Helpdesk\',0,1700),(240,\'Government Sales\',0,1700),(250,\'Retail Sales\',0,1700),(260,\'Recruiting\',0,1700),(270,\'Payroll\',0,1700);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `director`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "director" ("dir_id","dir_fname","dir_lname") VALUES (201,\'Alfred              \',\'Hitchcock           \'),(202,\'Jack                \',\'Clayton             \'),(203,\'David               \',\'Lean                \'),(204,\'Michael             \',\'Cimino              \'),(205,\'Milos               \',\'Forman              \'),(206,\'Ridley              \',\'Scott               \'),(207,\'Stanley             \',\'Kubrick             \'),(208,\'Bryan               \',\'Singer              \'),(209,\'Roman               \',\'Polanski            \'),(210,\'Paul                \',\'Thomas Anderson     \'),(211,\'Woody               \',\'Allen               \'),(212,\'Hayao               \',\'Miyazaki            \'),(213,\'Frank               \',\'Darabont            \'),(214,\'Sam                 \',\'Mendes              \'),(215,\'James               \',\'Cameron             \'),(216,\'Gus                 \',\'Van Sant            \'),(217,\'John                \',\'Boorman             \'),(218,\'Danny               \',\'Boyle               \'),(219,\'Christopher         \',\'Nolan               \'),(220,\'Richard             \',\'Kelly               \'),(221,\'Kevin               \',\'Spacey              \'),(222,\'Andrei              \',\'Tarkovsky           \'),(223,\'Peter               \',\'Jackson             \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `elephants`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "elephants" ("id","name","date") VALUES (1,NULL,'2015-12-15 00:00:00'),(2,NULL,'2015-12-15 00:00:00');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `emp_department`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "emp_department" ("dpt_code","dpt_name","dpt_allotment") VALUES (57,\'IT             \',65000),(63,\'Finance        \',15000),(47,\'HR             \',240000),(27,\'RD             \',55000),(89,\'QC             \',75000);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `emp_details`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "emp_details" ("emp_idno","emp_fname","emp_lname","emp_dept") VALUES (631548,\'Alan           \',\'Snappy         \',27),(839139,\'Maria          \',\'Foster         \',57),(127323,\'Michale        \',\'Robbin         \',57),(526689,\'Carlos         \',\'Snares         \',63),(843795,\'Enric          \',\'Dosio          \',57),(328717,\'Jhon           \',\'Snares         \',63),(444527,\'Joseph         \',\'Dosni          \',47),(659831,\'Zanifer        \',\'Emily          \',47),(847674,\'Kuleswar       \',\'Sitaraman      \',57),(748681,\'Henrey         \',\'Gabriel        \',47),(555935,\'Alex           \',\'Manuel         \',57),(539569,\'George         \',\'Mardy          \',27),(733843,\'Mario          \',\'Saule          \',63);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `employees`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "employees" ("employee_id","first_name","last_name","email","phone_number","hire_date","job_id","salary","commission_pct","manager_id","department_id") VALUES (100,\'Steven\',\'King\',\'SKING\',\'515.123.4567\','2003-06-17 00:00:00',\'AD_PRES\',24000.00,0.00,0,90),(101,\'Neena\',\'Kochhar\',\'NKOCHHAR\',\'515.123.4568\','2005-09-21 00:00:00',\'AD_VP\',17000.00,0.00,100,90),(102,\'Lex\',\'De Haan\',\'LDEHAAN\',\'515.123.4569\','2001-01-13 00:00:00',\'AD_VP\',17000.00,0.00,100,90),(103,\'Alexander\',\'Hunold\',\'AHUNOLD\',\'590.423.4567\','2006-01-03 00:00:00',\'IT_PROG\',9000.00,0.00,102,60),(104,\'Bruce\',\'Ernst\',\'BERNST\',\'590.423.4568\','2007-05-21 00:00:00',\'IT_PROG\',6000.00,0.00,103,60),(105,\'David\',\'Austin\',\'DAUSTIN\',\'590.423.4569\','2005-06-25 00:00:00',\'IT_PROG\',4800.00,0.00,103,60),(106,\'Valli\',\'Pataballa\',\'VPATABAL\',\'590.423.4560\','2006-02-05 00:00:00',\'IT_PROG\',4800.00,0.00,103,60),(107,\'Diana\',\'Lorentz\',\'DLORENTZ\',\'590.423.5567\','2007-02-07 00:00:00',\'IT_PROG\',4200.00,0.00,103,60),(108,\'Nancy\',\'Greenberg\',\'NGREENBE\',\'515.124.4569\','2002-08-17 00:00:00',\'FI_MGR\',12000.00,0.00,101,100),(109,\'Daniel\',\'Faviet\',\'DFAVIET\',\'515.124.4169\','2002-08-16 00:00:00',\'FI_ACCOUNT\',9000.00,0.00,108,100),(110,\'John\',\'Chen\',\'JCHEN\',\'515.124.4269\','2005-09-28 00:00:00',\'FI_ACCOUNT\',8200.00,0.00,108,100),(111,\'Ismael\',\'Sciarra\',\'ISCIARRA\',\'515.124.4369\','2005-09-30 00:00:00',\'FI_ACCOUNT\',7700.00,0.00,108,100),(112,\'Jose Manuel\',\'Urman\',\'JMURMAN\',\'515.124.4469\','2006-03-07 00:00:00',\'FI_ACCOUNT\',7800.00,0.00,108,100),(113,\'Luis\',\'Popp\',\'LPOPP\',\'515.124.4567\','2007-12-07 00:00:00',\'FI_ACCOUNT\',6900.00,0.00,108,100),(114,\'Den\',\'Raphaely\',\'DRAPHEAL\',\'515.127.4561\','2002-12-07 00:00:00',\'PU_MAN\',11000.00,0.00,100,30),(115,\'Alexander\',\'Khoo\',\'AKHOO\',\'515.127.4562\','2003-05-18 00:00:00',\'PU_CLERK\',3100.00,0.00,114,30),(116,\'Shelli\',\'Baida\',\'SBAIDA\',\'515.127.4563\','2005-12-24 00:00:00',\'PU_CLERK\',2900.00,0.00,114,30),(117,\'Sigal\',\'Tobias\',\'STOBIAS\',\'515.127.4564\','2005-07-24 00:00:00',\'PU_CLERK\',2800.00,0.00,114,30),(118,\'Guy\',\'Himuro\',\'GHIMURO\',\'515.127.4565\','2006-11-15 00:00:00',\'PU_CLERK\',2600.00,0.00,114,30),(119,\'Karen\',\'Colmenares\',\'KCOLMENA\',\'515.127.4566\','2007-08-10 00:00:00',\'PU_CLERK\',2500.00,0.00,114,30),(120,\'Matthew\',\'Weiss\',\'MWEISS\',\'650.123.1234\','2004-07-18 00:00:00',\'ST_MAN\',8000.00,0.00,100,50),(121,\'Adam\',\'Fripp\',\'AFRIPP\',\'650.123.2234\','2005-04-10 00:00:00',\'ST_MAN\',8200.00,0.00,100,50),(122,\'Payam\',\'Kaufling\',\'PKAUFLIN\',\'650.123.3234\','2003-05-01 00:00:00',\'ST_MAN\',7900.00,0.00,100,50),(123,\'Shanta\',\'Vollman\',\'SVOLLMAN\',\'650.123.4234\','2005-10-10 00:00:00',\'ST_MAN\',6500.00,0.00,100,50),(124,\'Kevin\',\'Mourgos\',\'KMOURGOS\',\'650.123.5234\','2007-11-16 00:00:00',\'ST_MAN\',5800.00,0.00,100,50),(125,\'Julia\',\'Nayer\',\'JNAYER\',\'650.124.1214\','2005-07-16 00:00:00',\'ST_CLERK\',3200.00,0.00,120,50),(126,\'Irene\',\'Mikkilineni\',\'IMIKKILI\',\'650.124.1224\','2006-09-28 00:00:00',\'ST_CLERK\',2700.00,0.00,120,50),(127,\'James\',\'Landry\',\'JLANDRY\',\'650.124.1334\','2007-01-14 00:00:00',\'ST_CLERK\',2400.00,0.00,120,50),(128,\'Steven\',\'Markle\',\'SMARKLE\',\'650.124.1434\','2008-03-08 00:00:00',\'ST_CLERK\',2200.00,0.00,120,50),(129,\'Laura\',\'Bissot\',\'LBISSOT\',\'650.124.5234\','2005-08-20 00:00:00',\'ST_CLERK\',3300.00,0.00,121,50),(130,\'Mozhe\',\'Atkinson\',\'MATKINSO\',\'650.124.6234\','2005-10-30 00:00:00',\'ST_CLERK\',2800.00,0.00,121,50),(131,\'James\',\'Marlow\',\'JAMRLOW\',\'650.124.7234\','2005-02-16 00:00:00',\'ST_CLERK\',2500.00,0.00,121,50),(132,\'TJ\',\'Olson\',\'TJOLSON\',\'650.124.8234\','2007-04-10 00:00:00',\'ST_CLERK\',2100.00,0.00,121,50),(133,\'Jason\',\'Mallin\',\'JMALLIN\',\'650.127.1934\','2004-06-14 00:00:00',\'ST_CLERK\',3300.00,0.00,122,50),(134,\'Michael\',\'Rogers\',\'MROGERS\',\'650.127.1834\','2006-08-26 00:00:00',\'ST_CLERK\',2900.00,0.00,122,50),(135,\'Ki\',\'Gee\',\'KGEE\',\'650.127.1734\','2007-12-12 00:00:00',\'ST_CLERK\',2400.00,0.00,122,50),(136,\'Hazel\',\'Philtanker\',\'HPHILTAN\',\'650.127.1634\','2008-02-06 00:00:00',\'ST_CLERK\',2200.00,0.00,122,50),(137,\'Renske\',\'Ladwig\',\'RLADWIG\',\'650.121.1234\','2003-07-14 00:00:00',\'ST_CLERK\',3600.00,0.00,123,50),(138,\'Stephen\',\'Stiles\',\'SSTILES\',\'650.121.2034\','2005-10-26 00:00:00',\'ST_CLERK\',3200.00,0.00,123,50),(139,\'John\',\'Seo\',\'JSEO\',\'650.121.2019\','2006-02-12 00:00:00',\'ST_CLERK\',2700.00,0.00,123,50),(140,\'Joshua\',\'Patel\',\'JPATEL\',\'650.121.1834\','2006-04-06 00:00:00',\'ST_CLERK\',2500.00,0.00,123,50),(141,\'Trenna\',\'Rajs\',\'TRAJS\',\'650.121.8009\','2003-10-17 00:00:00',\'ST_CLERK\',3500.00,0.00,124,50),(142,\'Curtis\',\'Davies\',\'CDAVIES\',\'650.121.2994\','2005-01-29 00:00:00',\'ST_CLERK\',3100.00,0.00,124,50),(143,\'Randall\',\'Matos\',\'RMATOS\',\'650.121.2874\','2006-03-15 00:00:00',\'ST_CLERK\',2600.00,0.00,124,50),(144,\'Peter\',\'Vargas\',\'PVARGAS\',\'650.121.2004\','2006-07-09 00:00:00',\'ST_CLERK\',2500.00,0.00,124,50),(145,\'John\',\'Russell\',\'JRUSSEL\',\'011.44.1344.429268\','2004-10-01 00:00:00',\'SA_MAN\',14000.00,0.40,100,80),(146,\'Karen\',\'Partners\',\'KPARTNER\',\'011.44.1344.467268\','2005-01-05 00:00:00',\'SA_MAN\',13500.00,0.30,100,80),(147,\'Alberto\',\'Errazuriz\',\'AERRAZUR\',\'011.44.1344.429278\','2005-03-10 00:00:00',\'SA_MAN\',12000.00,0.30,100,80),(148,\'Gerald\',\'Cambrault\',\'GCAMBRAU\',\'011.44.1344.619268\','2007-10-15 00:00:00',\'SA_MAN\',11000.00,0.30,100,80),(149,\'Eleni\',\'Zlotkey\',\'EZLOTKEY\',\'011.44.1344.429018\','2008-01-29 00:00:00',\'SA_MAN\',10500.00,0.20,100,80),(150,\'Peter\',\'Tucker\',\'PTUCKER\',\'011.44.1344.129268\','2005-01-30 00:00:00',\'SA_REP\',10000.00,0.30,145,80),(151,\'41-TRIAL-David 167\',\'34-TRIAL-Bernstein 100\',\'269-TRIAL-DBERNSTE 124\',\'78-TRIAL-011.4 258\','2005-03-24 00:00:00',\'262-TRIAL-\',9500.00,0.25,145,80),(152,\'5-TRIAL-Peter 245\',\'181-TRIAL-Hall 27\',\'61-TRIAL-PHALL 191\',\'295-TRIAL-011. 242\','2005-08-20 00:00:00',\'27-TRIAL- \',9000.00,0.25,145,80),(153,\'291-TRIAL- 204\',\'2-TRIAL-Olsen 153\',\'292-TRIAL-COLSEN 82\',\'21-TRIAL-011.4 116\','2006-03-30 00:00:00',\'218-TRIAL-\',8000.00,0.20,145,80),(154,\'47-TRIAL-Nanette 126\',\'71-TRIAL-Cambrault 138\',\'69-TRIAL-NCAMBRAU 112\',\'167-TRIAL-011. 199\','2006-12-09 00:00:00',\'235-TRIAL-\',7500.00,0.20,145,80),(155,\'203-TRIAL-Oliver 111\',\'122-TRIAL-Tuvault 33\',\'273-TRIAL-OTUVAULT 164\',\'141-TRIAL-011. 211\','2007-11-23 00:00:00',\'53-TRIAL- \',7000.00,0.15,145,80),(156,\'47-TRIAL-Janette 44\',\'262-TRIAL-King 57\',\'237-TRIAL-JKING 259\',\'23-TRIAL-011.4 141\','2004-01-30 00:00:00',\'229-TRIAL-\',10000.00,0.35,146,80),(157,\'16-TRIAL-Patrick 35\',\'290-TRIAL-Sully 42\',\'288-TRIAL-PSULLY 106\',\'40-TRIAL-011.4 242\','2004-03-04 00:00:00',\'64-TRIAL- \',9500.00,0.35,146,80),(158,\'146-TRIAL-Allan 105\',\'290-TRIAL-McEwen 129\',\'70-TRIAL-AMCEWEN 50\',\'6-TRIAL-011.44 201\','2004-08-01 00:00:00',\'93-TRIAL- \',9000.00,0.35,146,80),(159,\'129-TRIAL- 23\',\'84-TRIAL-Smith 154\',\'156-TRIAL-LSMITH 140\',\'166-TRIAL-011. 176\','2005-03-10 00:00:00',\'131-TRIAL-\',8000.00,0.30,146,80),(160,\'144-TRIAL-Louise 39\',\'26-TRIAL-Doran 223\',\'137-TRIAL-LDORAN 238\',\'218-TRIAL-011. 282\','2005-12-15 00:00:00',\'129-TRIAL-\',7500.00,0.30,146,80),(161,\'33-TRIAL-Sarath 215\',\'139-TRIAL-Sewall 258\',\'204-TRIAL-SSEWALL 30\',\'177-TRIAL-011. 206\','2006-11-03 00:00:00',\'173-TRIAL-\',7000.00,0.25,146,80),(162,\'221-TRIAL-Clara 245\',\'224-TRIAL-Vishney 172\',\'270-TRIAL-CVISHNEY 129\',\'77-TRIAL-011.4 273\','2005-11-11 00:00:00',\'297-TRIAL-\',10500.00,0.25,147,80),(163,\'286-TRIAL- 90\',\'161-TRIAL-Greene 36\',\'155-TRIAL-DGREENE 167\',\'255-TRIAL-011. 274\','2007-03-19 00:00:00',\'131-TRIAL-\',9500.00,0.15,147,80),(164,\'50-TRIAL-Mattea 250\',\'141-TRIAL-Marvins 124\',\'166-TRIAL-MMARVINS 130\',\'207-TRIAL-011. 191\','2008-01-24 00:00:00',\'7-TRIAL- 2\',7200.00,0.10,147,80),(165,\'157-TRIAL-David 287\',\'153-TRIAL-Lee 183\',\'245-TRIAL-DLEE 209\',\'109-TRIAL-011. 158\','2008-02-23 00:00:00',\'221-TRIAL-\',6800.00,0.10,147,80),(166,\'122-TRIAL-Sundar 46\',\'206-TRIAL-Ande 130\',\'213-TRIAL-SANDE 68\',\'0-TRIAL-011.44 191\','2008-03-24 00:00:00',\'162-TRIAL-\',6400.00,0.10,147,80),(167,\'10-TRIAL-Amit 59\',\'24-TRIAL-Banda 137\',\'248-TRIAL-ABANDA 183\',\'295-TRIAL-011. 141\','2008-04-21 00:00:00',\'2-TRIAL- 5\',6200.00,0.10,147,80),(168,\'91-TRIAL-Lisa 236\',\'74-TRIAL-Ozer 220\',\'96-TRIAL-LOZER 21\',\'48-TRIAL-011.4 99\','2005-03-11 00:00:00',\'168-TRIAL-\',11500.00,0.25,148,80),(169,\'181-TRIAL- 234\',\'53-TRIAL-Bloom 199\',\'18-TRIAL-HBLOOM 38\',\'0-TRIAL-011.44 188\','2006-03-23 00:00:00',\'127-TRIAL-\',10000.00,0.20,148,80),(170,\'128-TRIAL-Tayler 193\',\'48-TRIAL-Fox 283\',\'107-TRIAL-TFOX 21\',\'210-TRIAL-011. 17\','2006-01-24 00:00:00',\'13-TRIAL- \',9600.00,0.20,148,80),(171,\'209-TRIAL- 116\',\'35-TRIAL-Smith 51\',\'200-TRIAL-WSMITH 149\',\'19-TRIAL-011.4 56\','2007-02-23 00:00:00',\'298-TRIAL-\',7400.00,0.15,148,80),(172,\'224-TRIAL- 208\',\'144-TRIAL-Bates 209\',\'289-TRIAL-EBATES 2\',\'195-TRIAL-011. 85\','2007-03-24 00:00:00',\'93-TRIAL- \',7300.00,0.15,148,80),(173,\'223-TRIAL- 87\',\'214-TRIAL-Kumar 203\',\'248-TRIAL-SKUMAR 0\',\'258-TRIAL-011. 18\','2008-04-21 00:00:00',\'180-TRIAL-\',6100.00,0.10,148,80),(174,\'98-TRIAL-Ellen 281\',\'89-TRIAL-Abel 98\',\'109-TRIAL-EABEL 157\',\'72-TRIAL-011.4 222\','2004-05-11 00:00:00',\'238-TRIAL-\',11000.00,0.30,149,80),(175,\'38-TRIAL-Alyssa 179\',\'190-TRIAL-Hutton 257\',\'158-TRIAL-AHUTTON 191\',\'15-TRIAL-011.4 88\','2005-03-19 00:00:00',\'256-TRIAL-\',8800.00,0.25,149,80),(176,\'2-TRIAL-Jonathon 234\',\'272-TRIAL-Taylor 255\',\'228-TRIAL-JTAYLOR 146\',\'262-TRIAL-011. 86\','2006-03-24 00:00:00',\'275-TRIAL-\',8600.00,0.20,149,80),(177,\'169-TRIAL-Jack 42\',\'144-TRIAL-Livingston 216\',\'281-TRIAL-JLIVINGS 198\',\'122-TRIAL-011. 51\','2006-04-23 00:00:00',\'121-TRIAL-\',8400.00,0.20,149,80),(178,\'257-TRIAL- 276\',\'292-TRIAL-Grant 89\',\'275-TRIAL-KGRANT 212\',\'200-TRIAL-011. 110\','2007-05-24 00:00:00',\'3-TRIAL- 1\',7000.00,0.15,149,0),(179,\'161-TRIAL- 288\',\'201-TRIAL-Johnson 189\',\'255-TRIAL-CJOHNSON 223\',\'202-TRIAL-011. 85\','2008-01-04 00:00:00',\'182-TRIAL-\',6200.00,0.10,149,80),(180,\'88-TRIAL-Winston 226\',\'117-TRIAL-Taylor 57\',\'232-TRIAL-WTAYLOR 32\',\'269-TRIAL- 54\','2006-01-24 00:00:00',\'221-TRIAL-\',3200.00,0.00,120,50),(181,\'176-TRIAL-Jean 129\',\'268-TRIAL-Fleaur 192\',\'125-TRIAL-JFLEAUR 55\',\'134-TRIAL- 49\','2006-02-23 00:00:00',\'241-TRIAL-\',3100.00,0.00,120,50),(182,\'145-TRIAL-Martha 60\',\'118-TRIAL-Sullivan 153\',\'239-TRIAL-MSULLIVA 123\',\'79-TRIAL- 196\','2007-06-21 00:00:00',\'187-TRIAL-\',2500.00,0.00,120,50),(183,\'49-TRIAL-Girard 37\',\'66-TRIAL-Geoni 49\',\'193-TRIAL-GGEONI 95\',\'297-TRIAL- 16\','2008-02-03 00:00:00',\'86-TRIAL- \',2800.00,0.00,120,50),(184,\'188-TRIAL- 82\',\'155-TRIAL-Sarchand 234\',\'114-TRIAL-NSARCHAN 1\',\'116-TRIAL- 271\','2004-01-27 00:00:00',\'86-TRIAL- \',4200.00,0.00,121,50),(185,\'113-TRIAL-Alexis 55\',\'285-TRIAL-Bull 253\',\'12-TRIAL-ABULL 8\',\'32-TRIAL- 245\','2005-02-20 00:00:00',\'113-TRIAL-\',4100.00,0.00,121,50),(186,\'121-TRIAL-Julia 58\',\'246-TRIAL-Dellinger 82\',\'181-TRIAL-JDELLING 244\',\'96-TRIAL- 122\','2006-06-24 00:00:00',\'229-TRIAL-\',3400.00,0.00,121,50),(187,\'135-TRIAL- 50\',\'73-TRIAL-Cabrio 266\',\'44-TRIAL-ACABRIO 59\',\'192-TRIAL- 39\','2007-02-07 00:00:00',\'153-TRIAL-\',3000.00,0.00,121,50),(188,\'54-TRIAL-Kelly 110\',\'245-TRIAL-Chung 249\',\'286-TRIAL-KCHUNG 213\',\'274-TRIAL- 122\','2005-06-14 00:00:00',\'68-TRIAL- \',3800.00,0.00,122,50),(189,\'187-TRIAL- 5\',\'258-TRIAL-Dilly 191\',\'2-TRIAL-JDILLY 25\',\'77-TRIAL- 214\','2005-08-13 00:00:00',\'14-TRIAL- \',3600.00,0.00,122,50),(190,\'234-TRIAL- 74\',\'72-TRIAL-Gates 59\',\'133-TRIAL-TGATES 170\',\'287-TRIAL- 97\','2006-07-11 00:00:00',\'18-TRIAL- \',2900.00,0.00,122,50),(191,\'73-TRIAL-Randall 170\',\'263-TRIAL-Perkins 268\',\'92-TRIAL-RPERKINS 185\',\'102-TRIAL- 80\','2007-12-19 00:00:00',\'113-TRIAL-\',2500.00,0.00,122,50),(192,\'2-TRIAL-Sarah 199\',\'227-TRIAL-Bell 225\',\'43-TRIAL-SBELL 124\',\'223-TRIAL- 272\','2004-02-04 00:00:00',\'161-TRIAL-\',4000.00,0.00,123,50),(193,\'103-TRIAL- 132\',\'105-TRIAL-Everett 293\',\'225-TRIAL-BEVERETT 131\',\'92-TRIAL- 142\','2005-03-03 00:00:00',\'122-TRIAL-\',3900.00,0.00,123,50),(194,\'164-TRIAL-Samuel 100\',\'287-TRIAL-McCain 260\',\'213-TRIAL-SMCCAIN 74\',\'170-TRIAL- 70\','2006-07-01 00:00:00',\'235-TRIAL-\',3200.00,0.00,123,50),(195,\'211-TRIAL-Vance 260\',\'296-TRIAL-Jones 167\',\'85-TRIAL-VJONES 250\',\'140-TRIAL- 194\','2007-03-17 00:00:00',\'295-TRIAL-\',2800.00,0.00,123,50),(196,\'119-TRIAL-Alana 25\',\'176-TRIAL-Walsh 94\',\'158-TRIAL-AWALSH 202\',\'271-TRIAL- 266\','2006-04-24 00:00:00',\'178-TRIAL-\',3100.00,0.00,124,50),(197,\'151-TRIAL-Kevin 284\',\'118-TRIAL-Feeney 264\',\'119-TRIAL-KFEENEY 52\',\'100-TRIAL- 87\','2006-05-23 00:00:00',\'160-TRIAL-\',3000.00,0.00,124,50),(198,\'10-TRIAL-Donald 257\',\'70-TRIAL-OConnell 215\',\'276-TRIAL-DOCONNEL 227\',\'43-TRIAL- 258\','2007-06-21 00:00:00',\'264-TRIAL-\',2600.00,0.00,124,50),(199,\'82-TRIAL-Douglas 286\',\'165-TRIAL-Grant 187\',\'177-TRIAL-DGRANT 74\',\'225-TRIAL- 127\','2008-01-13 00:00:00',\'229-TRIAL-\',2600.00,0.00,124,50);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "employees" ("employee_id","first_name","last_name","email","phone_number","hire_date","job_id","salary","commission_pct","manager_id","department_id") VALUES (200,\'223-TRIAL- 20\',\'2-TRIAL-Whalen 262\',\'123-TRIAL-JWHALEN 296\',\'137-TRIAL- 61\','2003-09-17 00:00:00',\'295-TRIAL-\',4400.00,0.00,101,10),(201,\'64-TRIAL-Michael 160\',\'202-TRIAL-Hartstein 16\',\'230-TRIAL-MHARTSTE 226\',\'211-TRIAL- 171\','2004-02-17 00:00:00',\'111-TRIAL-\',13000.00,0.00,100,20),(202,\'153-TRIAL-Pat 220\',\'90-TRIAL-Fay 224\',\'188-TRIAL-PFAY 163\',\'140-TRIAL- 151\','2005-08-17 00:00:00',\'62-TRIAL- \',6000.00,0.00,201,20),(203,\'0-TRIAL-Susan 13\',\'58-TRIAL-Mavris 178\',\'265-TRIAL-SMAVRIS 107\',\'77-TRIAL- 0\','2002-06-07 00:00:00',\'258-TRIAL-\',6500.00,0.00,101,40),(204,\'203-TRIAL- 160\',\'157-TRIAL-Baer 224\',\'177-TRIAL-HBAER 8\',\'113-TRIAL- 187\','2002-06-07 00:00:00',\'1-TRIAL- 5\',10000.00,0.00,101,70),(205,\'60-TRIAL-Shelley 228\',\'93-TRIAL-Higgins 84\',\'205-TRIAL-SHIGGINS 240\',\'211-TRIAL- 204\','2002-06-07 00:00:00',\'235-TRIAL-\',12000.00,0.00,101,110),(206,\'72-TRIAL-William 250\',\'223-TRIAL-Gietz 85\',\'156-TRIAL-WGIETZ 116\',\'126-TRIAL- 57\','2002-06-07 00:00:00',\'126-TRIAL-\',8300.00,0.00,205,110);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `game_scores`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "game_scores" ("id","name","score") VALUES (1,\'James\',90),(2,\'James\',55),(3,\'James\',150),(4,\'James\',50),(5,\'Mary\',110),(6,\'Mary\',160),(7,\'Ron\',120),(8,\'Ron\',90),(9,\'Ron\',200);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `genres`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "genres" ("gen_id","gen_title") VALUES (1001,\'Action              \'),(1002,\'Adventure           \'),(1003,\'Animation           \'),(1004,\'Biography           \'),(1005,\'Comedy              \'),(1006,\'Crime               \'),(1007,\'Drama               \'),(1008,\'Horror              \'),(1009,\'Music               \'),(1010,\'Mystery             \'),(1011,\'Romance             \'),(1012,\'Thriller            \'),(1013,\'War                 \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `playing_position`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "playing_position" ("position_id","position_desc") VALUES (\'GK\',\'Goalkeepers\'),(\'DF\',\'Defenders\'),(\'MF\',\'Midfielders\'),(\'FD\',\'Defenders\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `grade`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "grade" ("city") VALUES (\'New York\'),(\'New York\'),(\'Moncow\'),(\'California\'),(\'London\'),(\'Paris\'),(\'Berlin\'),(\'London\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `grades`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "grades" ("id","grade_1","grade_2","grade_3") VALUES (1,75,25,80),(2,85,85,60),(3,70,75,65);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `item_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "item_mast" ("pro_id","pro_name","pro_price","pro_com") VALUES (101,\'Mother Board\',3200.00,15),(102,\'Key 

Board\',450.00,16),(103,\'ZIP 

drive\',250.00,14),(104,\'Speaker\',550.00,16),(105,\'Monitor\',5000.00,11),(106,\'DVD 

drive\',900.00,12),(107,\'CD 

drive\',800.00,12),(108,\'Printer\',2600.00,13),(109,\'Refill cartridge\',350.00,13),(110,\'Mouse\',250.00,12);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `job_history`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "job_history" ("employee_id","start_date","end_date","job_id","department_id") VALUES (201,'2004-02-17 00:00:00','2007-12-19 00:00:00',\'MK_REP\',20),(200,'2002-07-01 00:00:00','2006-12-31 00:00:00',\'AC_ACCOUNT\',90),(200,'1995-09-17 00:00:00','2001-06-17 00:00:00',\'AD_ASST\',90),(176,'2007-01-01 00:00:00','2007-12-31 00:00:00',\'SA_MAN\',80),(176,'2006-03-24 00:00:00','2006-12-31 00:00:00',\'SA_REP\',80),(122,'2007-01-01 00:00:00','2007-12-31 00:00:00',\'ST_CLERK\',50),(114,'2006-03-24 00:00:00','2007-12-31 00:00:00',\'ST_CLERK\',50),(102,'2001-01-13 00:00:00','2006-07-24 00:00:00',\'IT_PROG\',60),(101,'2001-10-28 00:00:00','2005-03-15 00:00:00',\'AC_MGR\',110),(101,'1997-09-21 00:00:00','2001-10-27 00:00:00',\'AC_ACCOUNT\',110);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `jobs`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "jobs" ("job_id","job_title","min_salary","max_salary") VALUES (\'AD_PRES\',\'President\',20000,40000),(\'AD_VP\',\'Administration Vice President\',15000,30000),(\'AD_ASST\',\'Administration Assistant\',3000,6000),(\'FI_MGR\',\'Finance Manager\',8200,16000),(\'FI_ACCOUNT\',\'Accountant\',4200,9000),(\'AC_MGR\',\'Accounting Manager\',8200,16000),(\'AC_ACCOUNT\',\'Public Accountant\',4200,9000),(\'SA_MAN\',\'Sales Manager\',10000,20000),(\'SA_REP\',\'Sales Representative\',6000,12000),(\'PU_MAN\',\'Purchasing Manager\',8000,15000),(\'PU_CLERK\',\'Purchasing Clerk\',2500,5500),(\'ST_MAN\',\'Stock Manager\',5500,8500),(\'ST_CLERK\',\'Stock Clerk\',2000,5000),(\'SH_CLERK\',\'Shipping Clerk\',2500,5500),(\'IT_PROG\',\'Programmer\',4000,10000),(\'MK_MAN\',\'Marketing Manager\',9000,15000),(\'MK_REP\',\'Marketing Representative\',4000,9000),(\'HR_REP\',\'Human Resources Representative\',4000,9000),(\'PR_REP\',\'Public Relations Representative\',4500,10500);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `locations`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "locations" ("location_id","street_address","postal_code","city","state_province","country_id") VALUES (1000,\'1297 Via Cola di Rie\',\'989\',\'Roma\',\'\',\'IT\'),(1100,\'93091 Calle della Testa\',\'10934\',\'Venice\',\'\',\'IT\'),(1200,\'2017 Shinjuku-ku\',\'1689\',\'Tokyo\',\'Tokyo Prefecture\',\'JP\'),(1300,\'9450 Kamiya-cho\',\'6823\',\'Hiroshima\',\'\',\'JP\'),(1400,\'2014 Jabberwocky Rd\',\'26192\',\'Southlake\',\'Texas\',\'US\'),(1500,\'2011 Interiors Blvd\',\'99236\',\'South San Francisco\',\'California\',\'US\'),(1600,\'2007 Zagora St\',\'50090\',\'South Brunswick\',\'New Jersey\',\'US\'),(1700,\'2004 Charade Rd\',\'98199\',\'Seattle\',\'Washington\',\'US\'),(1800,\'147 Spadina Ave\',\'M5V 2L7\',\'Toronto\',\'Ontario\',\'CA\'),(1900,\'6092 Boxwood St\',\'YSW 9T2\',\'Whitehorse\',\'Yukon\',\'CA\'),(2000,\'40-5-12 Laogianggen\',\'190518\',\'Beijing\',\'\',\'CN\'),(2100,\'1298 Vileparle (E)\',\'490231\',\'Bombay\',\'Maharashtra\',\'IN\'),(2200,\'12-98 Victoria Street\',\'2901\',\'Sydney\',\'New South Wales\',\'AU\'),(2300,\'198 Clementi North\',\'540198\',\'Singapore\',\'\',\'SG\'),(2400,\'8204 Arthur St\',\'\',\'London\',\'\',\'UK\'),(2500,\'"Magdalen Centre\',\' The Oxford \',\'OX9 9ZB\',\'Oxford\',\'Ox\'),(2600,\'9702 Chester Road\',\'9629850293\',\'Stretford\',\'Manchester\',\'UK\'),(2700,\'Schwanthalerstr. 7031\',\'80925\',\'Munich\',\'Bavaria\',\'DE\'),(2800,\'Rua Frei Caneca 1360\',\'01307-002\',\'Sao Paulo\',\'Sao Paulo\',\'BR\'),(2900,\'20 Rue des Corps-Saints\',\'1730\',\'Geneva\',\'Geneve\',\'CH\'),(3000,\'Murtenstrasse 921\',\'3095\',\'Bern\',\'BE\',\'CH\'),(3100,\'Pieter Breughelstraat 837\',\'3029SK\',\'Utrecht\',\'Utrecht\',\'NL\'),(3200,\'Mariano Escobedo 9991\',\'11932\',\'Mexico City\',\'"Distrito Federal\',\'"\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `player_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_mast" ("player_id","team_id","jersey_no","player_name","posi_to_play","dt_of_bir","age","playing_club") VALUES (160001,1201,1,\'Etrit Berisha \',\'GK\','1989-03-10 00:00:00',27,\'Lazio\'),(160008,1201,2,\'Andi Lila \',\'DF\','1986-02-12 00:00:00',30,\'Giannina\'),(160016,1201,3,\'Ermir Lenjani \',\'MF\','1989-08-05 00:00:00',26,\'Nantes\'),(160007,1201,4,\'Elseid Hysaj \',\'DF\','1994-02-20 00:00:00',22,\'Napoli\'),(160013,1201,5,\'Lorik Cana \',\'MF\','1983-07-27 00:00:00',32,\'Nantes\'),(160010,1201,6,\'Frederic Veseli \',\'DF\','1992-11-20 00:00:00',23,\'Lugano\'),(160004,1201,7,\'Ansi Agolli \',\'DF\','1982-10-11 00:00:00',33,\'Qarabag\'),(160012,1201,8,\'Migjen Basha \',\'MF\','1987-01-05 00:00:00',29,\'Como\'),(160017,1201,9,\'Ledian Memushaj \',\'MF\','1986-12-17 00:00:00',29,\'Pescara\'),(160023,1201,10,\'Armando Sadiku \',\'FD\','1991-05-27 00:00:00',25,\'Vaduz\'),(160022,1201,11,\'Shkelzen Gashi \',\'FD\','1988-07-15 00:00:00',27,\'Colorado\'),(160003,1201,12,\'Orges Shehi \',\'GK\','1977-09-25 00:00:00',38,\'Skenderbeu\'),(160015,1201,13,\'Burim Kukeli \',\'MF\','1984-01-16 00:00:00',32,\'Zurich\'),(160019,1201,14,\'Taulant Xhaka \',\'MF\','1991-03-28 00:00:00',25,\'Basel\'),(160009,1201,15,\'Mergim Mavraj \',\'DF\','1986-06-09 00:00:00',30,\'Koln\'),(160021,1201,16,\'Sokol Cikalleshi \',\'FD\','1990-07-27 00:00:00',25,\'Istanbul Basaksehir\'),(160006,1201,17,\'Naser Aliji \',\'DF\','1993-12-27 00:00:00',22,\'Basel\'),(160005,1201,18,\'Arlind Ajeti \',\'DF\','1993-09-25 00:00:00',22,\'Frosinone\'),(160020,1201,19,\'Bekim Balaj \',\'FD\','1991-01-11 00:00:00',25,\'Rijeka\'),(160014,1201,20,\'Ergys Kace \',\'MF\','1993-07-08 00:00:00',22,\'PAOK\'),(160018,1201,21,\'Odise Roshi \',\'MF\','1991-05-22 00:00:00',25,\'Rijeka\'),(160011,1201,22,\'Amir Abrashi \',\'MF\','1990-03-27 00:00:00',26,\'Freiburg\'),(160002,1201,23,\'Alban Hoxha \',\'GK\','1987-11-23 00:00:00',28,\'Partizani\'),(160024,1202,1,\'Robert Almer \',\'GK\','1984-03-20 00:00:00',32,\'Austria Wien\'),(160036,1202,2,\'Gyorgy Garics \',\'MF\','1984-03-08 00:00:00',32,\'Darmstadt\'),(160027,1202,3,\'Aleksandar Dragovic \',\'DF\','1991-03-06 00:00:00',25,\'Dynamo Kyiv\'),(160029,1202,4,\'Martin Hinteregger \',\'DF\','1992-09-07 00:00:00',23,\'Monchengladbach\'),(160028,1202,5,\'Christian Fuchs \',\'DF\','1986-04-07 00:00:00',30,\'Leicester\'),(160038,1202,6,\'Stefan Ilsanker \',\'MF\','1989-05-18 00:00:00',27,\'Leipzig\'),(160043,1202,7,\'Marko Arnautovic \',\'FD\','1989-04-19 00:00:00',27,\'Stoke\'),(160034,1202,8,\'David Alaba \',\'MF\','1992-06-24 00:00:00',23,\'Bayern\'),(160046,1202,9,\'Rubin Okotie \',\'FD\','1987-06-06 00:00:00',29,\'1860 Munchen\'),(160040,1202,10,\'Zlatko Junuzovic \',\'MF\','1987-09-26 00:00:00',28,\'Bremen\'),(160037,1202,11,\'Martin Harnik \',\'MF\','1987-06-10 00:00:00',29,\'Stuttgart\'),(160025,1202,12,\'Heinz Lindner \',\'GK\','1990-07-17 00:00:00',25,\'Frankfurt\'),(160032,1202,13,\'Markus Suttner \',\'DF\','1987-04-16 00:00:00',29,\'Ingolstadt\'),(160035,1202,14,\'Julian Baumgartlinger \',\'MF\','1988-01-02 00:00:00',28,\'Mainz\'),(160031,1202,15,\'Sebastian Prodl \',\'DF\','1987-06-21 00:00:00',28,\'Watford\'),(160033,1202,16,\'Kevin Wimmer \',\'DF\','1992-11-15 00:00:00',23,\'Tottenham\'),(160030,1202,17,\'Florian Klein \',\'DF\','1986-11-17 00:00:00',29,\'Stuttgart\'),(160042,1202,18,\'Alessandro Schopf \',\'MF\','1994-02-07 00:00:00',22,\'Schalke\'),(160044,1202,19,\'Lukas Hinterseer \',\'FD\','1991-03-28 00:00:00',25,\'Ingolstadt\'),(160041,1202,20,\'Marcel Sabitzer \',\'MF\','1994-03-17 00:00:00',22,\'Leipzig\'),(160045,1202,21,\'Marc Janko \',\'FD\','1983-06-25 00:00:00',32,\'Basel\'),(160039,1202,22,\'Jakob Jantscher \',\'MF\','1989-01-08 00:00:00',27,\'Luzern\'),(160026,1202,23,\'Ramazan Ozcan \',\'GK\','1984-06-28 00:00:00',31,\'Ingolstadt\'),(160047,1203,1,\'Thibaut Courtois \',\'GK\','1992-05-11 00:00:00',24,\'Chelsea\'),(160050,1203,2,\'Toby Alderweireld \',\'DF\','1989-03-02 00:00:00',27,\'Tottenham\'),(160056,1203,3,\'Thomas Vermaelen \',\'DF\','1985-11-14 00:00:00',30,\'Barcelona\'),(160063,1203,4,\'Radja Nainggolan \',\'MF\','1988-05-04 00:00:00',28,\'Roma\'),(160057,1203,5,\'Jan Vertonghen \',\'DF\','1987-04-24 00:00:00',29,\'Tottenham\'),(160064,1203,6,\'237-TRIAL-Axel Witsel  171\',\'MF\','1989-01-12 00:00:00',27,\'169-TRIAL-Zenit 261\'),(160059,1203,7,\'296-TRIAL-Kevin De Bruyne  122\',\'MF\','1991-06-28 00:00:00',24,\'217-TRIAL-Man. City 212\'),(160061,1203,8,\'117-TRIAL-Marouane Fellaini  96\',\'MF\','1987-11-22 00:00:00',28,\'185-TRIAL-Man. United 41\'),(160067,1203,9,\'123-TRIAL-Romelu Lukaku  129\',\'FD\','1993-05-13 00:00:00',23,\'229-TRIAL-Everton 65\'),(160062,1203,10,\'259-TRIAL-Eden Hazard  232\',\'MF\','1991-01-07 00:00:00',25,\'96-TRIAL-Chelsea 155\'),(160058,1203,11,\'53-TRIAL-Yannick Carrasco  162\',\'MF\','1993-09-04 00:00:00',22,\'284-TRIAL-Atletico 34\'),(160049,1203,12,\'54-TRIAL-Simon Mignolet  172\',\'GK\','1988-08-06 00:00:00',27,\'157-TRIAL-Liverpool 269\'),(160048,1203,13,\'32-TRIAL-Jean-Francois Gillet  263\',\'GK\','1979-05-31 00:00:00',37,\'207-TRIAL-Mechelen 83\'),(160068,1203,14,\'11-TRIAL-Dries Mertens  235\',\'FD\','1987-05-06 00:00:00',29,\'167-TRIAL-Napoli 48\'),(160052,1203,15,\'175-TRIAL-Jason Denayer  38\',\'DF\','1995-06-28 00:00:00',20,\'123-TRIAL-Galatasaray 242\'),(160055,1203,16,\'54-TRIAL-Thomas Meunier  211\',\'DF\','1991-09-12 00:00:00',24,\'241-TRIAL-Club Brugge 75\'),(160069,1203,17,\'159-TRIAL-Divock Origi  125\',\'FD\','1995-04-18 00:00:00',21,\'221-TRIAL-Liverpool 170\'),(160053,1203,18,\'126-TRIAL-Christian Kabasele  134\',\'DF\','1991-02-24 00:00:00',25,\'205-TRIAL-Genk 283\'),(160060,1203,19,\'150-TRIAL-Mousa Dembele  298\',\'MF\','1987-07-16 00:00:00',28,\'79-TRIAL-Tottenham 201\'),(160066,1203,20,\'193-TRIAL-Christian Benteke  134\',\'FD\','1990-12-03 00:00:00',25,\'137-TRIAL-Liverpool 134\'),(160054,1203,21,\'156-TRIAL-Jordan Lukaku  193\',\'DF\','1994-07-25 00:00:00',21,\'276-TRIAL-Oostende 205\'),(160065,1203,22,\'62-TRIAL-Michy Batshuayi  48\',\'FD\','1993-10-02 00:00:00',22,\'281-TRIAL-Marseille 0\'),(160051,1203,23,\'13-TRIAL-Laurent Ciman  141\',\'DF\','1985-08-05 00:00:00',30,\'55-TRIAL-Montreal 255\'),(160072,1204,1,\'242-TRIAL-Ivan Vargic  62\',\'GK\','1987-03-15 00:00:00',29,\'11-TRIAL-Rijeka 277\'),(160079,1204,2,\'24-TRIAL-Sime Vrsaljko  278\',\'DF\','1992-01-10 00:00:00',24,\'252-TRIAL-Sassuolo 143\'),(160077,1204,3,\'96-TRIAL-Ivan Strinic  73\',\'DF\','1987-07-17 00:00:00',28,\'140-TRIAL-Napoli 13\'),(160085,1204,4,\'275-TRIAL-Ivan PeriSic  272\',\'MF\','1989-02-02 00:00:00',27,\'218-TRIAL-Internazionale 10\'),(160073,1204,5,\'117-TRIAL-Vedran Corluka  232\',\'DF\','1986-02-05 00:00:00',30,\'212-TRIAL-Lokomotiv Moskva 95\'),(160074,1204,6,\'269-TRIAL-Tin Jedvaj  131\',\'DF\','1995-11-28 00:00:00',20,\'240-TRIAL-Leverkusen 88\'),(160086,1204,7,\'185-TRIAL-Ivan Rakitic  190\',\'MF\','1988-03-10 00:00:00',28,\'297-TRIAL-Barcelona 189\'),(160083,1204,8,\'190-TRIAL-Mateo Kovacic  145\',\'MF\','1994-05-06 00:00:00',22,\'153-TRIAL-Real Madrid 114\'),(160090,1204,9,\'51-TRIAL-Andrej Kramaric  40\',\'FD\','1991-06-19 00:00:00',24,\'144-TRIAL-Hoffenheim 158\'),(160084,1204,10,\'35-TRIAL-Luka Modric  59\',\'MF\','1985-09-09 00:00:00',30,\'92-TRIAL-Real Madrid 105\'),(160076,1204,11,\'64-TRIAL-Darijo Srna  181\',\'DF\','1982-05-01 00:00:00',34,\'3-TRIAL-Shakhtar Donetsk 229\'),(160070,1204,12,\'75-TRIAL-Lovre Kalinic  208\',\'GK\','1990-04-03 00:00:00',26,\'192-TRIAL-Hajduk Split 297\'),(160075,1204,13,\'149-TRIAL-Gordon Schildenfeld  156\',\'DF\','1985-03-18 00:00:00',31,\'61-TRIAL-Dinamo Zagreb 127\'),(160081,1204,14,\'167-TRIAL-Marcelo Brozovic  141\',\'MF\','1992-11-16 00:00:00',23,\'29-TRIAL-Internazionale 40\'),(160087,1204,15,\'213-TRIAL-Marko Rog  74\',\'MF\','1995-07-19 00:00:00',20,\'201-TRIAL-Dinamo Zagreb 77\'),(160089,1204,16,\'115-TRIAL-Nikola Kalinic  283\',\'FD\','1988-01-05 00:00:00',28,\'113-TRIAL-Fiorentina 292\'),(160091,1204,17,\'24-TRIAL-Mario Mandzukic  201\',\'FD\','1986-05-21 00:00:00',30,\'292-TRIAL-Juventus 159\'),(160082,1204,18,\'270-TRIAL-Ante Coric  28\',\'MF\','1997-04-14 00:00:00',19,\'127-TRIAL-Dinamo Zagreb 184\'),(160080,1204,19,\'175-TRIAL-Milan Badelj  186\',\'MF\','1989-02-25 00:00:00',27,\'198-TRIAL-Fiorentina 70\'),(160092,1204,20,\'287-TRIAL-Marko Pjaca  147\',\'FD\','1995-05-06 00:00:00',21,\'204-TRIAL-Dinamo Zagreb 203\'),(160078,1204,21,\'221-TRIAL-Domagoj Vida  163\',\'DF\','1989-04-29 00:00:00',27,\'6-TRIAL-Dynamo Kyiv 263\'),(160088,1204,22,\'10-TRIAL-Duje Cop  271\',\'FD\','1990-02-01 00:00:00',26,\'189-TRIAL-Malaga 240\'),(160071,1204,23,\'164-TRIAL-Danijel SubaSic  42\',\'GK\','1984-10-27 00:00:00',31,\'119-TRIAL-Monaco 213\'),(160093,1205,1,\'91-TRIAL-Petr Cech  104\',\'GK\','1982-05-20 00:00:00',34,\'18-TRIAL-Arsenal 232\'),(160098,1205,2,\'150-TRIAL-Pavel Kaderabek  5\',\'DF\','1992-04-25 00:00:00',24,\'175-TRIAL-Hoffenheim 39\'),(160099,1205,3,\'3-TRIAL-Michal Kadlec  22\',\'DF\','1984-12-13 00:00:00',31,\'98-TRIAL-Fenerbahce 147\'),(160096,1205,4,\'84-TRIAL-Theodor Gebre Selassie  148\',\'DF\','1986-12-24 00:00:00',29,\'271-TRIAL-Bremen 164\'),(160097,1205,5,\'113-TRIAL-Roman Hubnik  275\',\'DF\','1984-06-06 00:00:00',32,\'245-TRIAL-Plzen 212\'),(160101,1205,6,\'146-TRIAL-TomasSivok  78\',\'DF\','1983-09-15 00:00:00',32,\'269-TRIAL-Bursaspor 262\'),(160114,1205,7,\'119-TRIAL-TomasNecid  185\',\'FD\','1989-08-13 00:00:00',26,\'89-TRIAL-Bursaspor 44\'),(160100,1205,8,\'165-TRIAL-David Limbersky  240\',\'DF\','1983-10-06 00:00:00',32,\'145-TRIAL-Plzen 8\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_mast" ("player_id","team_id","jersey_no","player_name","posi_to_play","dt_of_bir","age","playing_club") VALUES (160104,1205,9,\'118-TRIAL-Borek Dockal  270\',\'MF\','1988-09-30 00:00:00',27,\'1-TRIAL-Sparta Praha 123\'),(160110,1205,10,\'132-TRIAL-TomasRosicky  172\',\'MF\','1980-10-04 00:00:00',35,\'152-TRIAL-Arsenal 187\'),(160109,1205,11,\'70-TRIAL-Daniel Pudil  63\',\'MF\','1985-09-27 00:00:00',30,\'201-TRIAL-Sheff. Wednesday 3\'),(160115,1205,12,\'23-TRIAL-Milan Skoda  227\',\'FD\','1986-01-16 00:00:00',30,\'200-TRIAL-Slavia Praha 269\'),(160108,1205,13,\'215-TRIAL-Jaroslav PlaSil  165\',\'MF\','1982-01-05 00:00:00',34,\'28-TRIAL-Bordeaux 243\'),(160105,1205,14,\'147-TRIAL-Daniel Kolar  288\',\'MF\','1985-10-27 00:00:00',30,\'243-TRIAL-Plzen 37\'),(160107,1205,15,\'209-TRIAL-David Pavelka  63\',\'MF\','1991-05-18 00:00:00',25,\'249-TRIAL-Kasimpasa 181\'),(160095,1205,16,\'88-TRIAL-TomasVaclik  242\',\'GK\','1989-03-29 00:00:00',27,\'8-TRIAL-Basel 260\'),(160102,1205,17,\'221-TRIAL-Marek Suchy  258\',\'DF\','1988-03-29 00:00:00',28,\'254-TRIAL-Basel 188\'),(160112,1205,18,\'46-TRIAL-Josef Sural  90\',\'MF\','1990-05-30 00:00:00',26,\'149-TRIAL-Sparta Praha 243\'),(160106,1205,19,\'130-TRIAL-Ladislav Krejci  120\',\'MF\','1992-07-05 00:00:00',23,\'148-TRIAL-Sparta Praha 67\'),(160111,1205,20,\'36-TRIAL-Jiri Skalak  83\',\'MF\','1992-03-12 00:00:00',24,\'35-TRIAL-Brighton 126\'),(160113,1205,21,\'185-TRIAL-David Lafata  138\',\'FD\','1981-09-18 00:00:00',34,\'253-TRIAL-Sparta Praha 129\'),(160103,1205,22,\'124-TRIAL-Vladimir Darida  148\',\'MF\','1990-08-08 00:00:00',25,\'123-TRIAL-Hertha 59\'),(160094,1205,23,\'157-TRIAL-TomasKoubek  166\',\'GK\','1992-08-26 00:00:00',23,\'144-TRIAL-Liberec 255\'),(160117,1206,1,\'218-TRIAL-Joe Hart  26\',\'GK\','1987-04-19 00:00:00',29,\'211-TRIAL-Man. City 25\'),(160125,1206,2,\'255-TRIAL-Kyle Walker  101\',\'DF\','1990-05-28 00:00:00',26,\'49-TRIAL-Tottenham 196\'),(160122,1206,3,\'284-TRIAL-Danny Rose  215\',\'DF\','1990-07-02 00:00:00',25,\'264-TRIAL-Tottenham 242\'),(160131,1206,4,\'275-TRIAL-James Milner  213\',\'MF\','1986-01-04 00:00:00',30,\'242-TRIAL-Liverpool 296\'),(160120,1206,5,\'48-TRIAL-Gary Cahill  172\',\'DF\','1985-12-19 00:00:00',30,\'26-TRIAL-Chelsea 206\'),(160123,1206,6,\'73-TRIAL-Chris Smalling  129\',\'DF\','1989-11-22 00:00:00',26,\'4-TRIAL-Man. United 105\'),(160132,1206,7,\'226-TRIAL-Raheem Sterling  112\',\'MF\','1994-12-08 00:00:00',21,\'175-TRIAL-Man. City 93\'),(160130,1206,8,\'65-TRIAL-Adam Lallana  136\',\'MF\','1988-05-10 00:00:00',28,\'36-TRIAL-Liverpool 41\'),(160134,1206,9,\'214-TRIAL-Harry Kane  294\',\'FD\','1993-07-28 00:00:00',22,\'156-TRIAL-Tottenham 52\'),(160136,1206,10,\'236-TRIAL-Wayne Rooney  238\',\'FD\','1985-10-24 00:00:00',30,\'82-TRIAL-Man. United 155\'),(160138,1206,11,\'15-TRIAL-Jamie Vardy  231\',\'FD\','1987-01-11 00:00:00',29,\'230-TRIAL-Leicester 141\'),(160121,1206,12,\'225-TRIAL-Nathaniel Clyne  211\',\'DF\','1991-04-05 00:00:00',25,\'237-TRIAL-Liverpool 286\'),(160116,1206,13,\'190-TRIAL-Fraser Forster  150\',\'GK\','1988-03-17 00:00:00',28,\'262-TRIAL-Southampton 34\'),(160129,1206,14,\'93-TRIAL-Jordan Henderson  153\',\'MF\','1990-06-17 00:00:00',26,\'116-TRIAL-Liverpool 252\'),(160137,1206,15,\'208-TRIAL-Daniel Sturridge  62\',\'FD\','1989-09-01 00:00:00',26,\'33-TRIAL-Liverpool 54\'),(160124,1206,16,\'103-TRIAL-John Stones  134\',\'DF\','1994-05-28 00:00:00',22,\'203-TRIAL-Everton 156\'),(160128,1206,17,\'148-TRIAL-Eric Dier  24\',\'MF\','1994-01-15 00:00:00',22,\'17-TRIAL-Tottenham 13\'),(160133,1206,18,\'109-TRIAL-Jack Wilshere  28\',\'MF\','1992-01-01 00:00:00',24,\'100-TRIAL-Arsenal 80\'),(160127,1206,19,\'18-TRIAL-Ross Barkley  58\',\'MF\','1993-12-05 00:00:00',22,\'50-TRIAL-Everton 155\'),(160126,1206,20,\'161-TRIAL-Dele Alli  264\',\'MF\','1996-04-11 00:00:00',20,\'203-TRIAL-Tottenham 76\'),(160119,1206,21,\'243-TRIAL-Ryan Bertrand  209\',\'DF\','1989-08-05 00:00:00',26,\'202-TRIAL-Southampton 261\'),(160135,1206,22,\'289-TRIAL-Marcus Rashford  48\',\'FD\','1997-10-31 00:00:00',18,\'82-TRIAL-Man. United 153\'),(160118,1206,23,\'74-TRIAL-Tom Heaton  120\',\'GK\','1986-04-15 00:00:00',30,\'2-TRIAL-Burnley 23\'),(160140,1207,1,\'231-TRIAL-Hugo Lloris  169\',\'GK\','1986-12-26 00:00:00',29,\'278-TRIAL-Tottenham 159\'),(160144,1207,2,\'108-TRIAL-Christophe Jallet  119\',\'DF\','1983-10-31 00:00:00',32,\'271-TRIAL-Lyon 3\'),(160143,1207,3,\'45-TRIAL-Patrice Evra  181\',\'DF\','1981-05-15 00:00:00',35,\'104-TRIAL-Juventus 92\'),(160147,1207,4,\'285-TRIAL-Adil Rami  113\',\'DF\','1985-12-27 00:00:00',30,\'98-TRIAL-Sevilla 189\'),(160152,1207,5,\'122-TRIAL-NGolo Kante  238\',\'MF\','1991-03-29 00:00:00',25,\'137-TRIAL-Leicester 110\'),(160150,1207,6,\'261-TRIAL-Yohan Cabaye  234\',\'MF\','1986-01-14 00:00:00',30,\'208-TRIAL-Crystal Palace 61\'),(160160,1207,7,\'59-TRIAL-Antoine Griezmann  193\',\'FD\','1991-03-21 00:00:00',25,\'15-TRIAL-Atletico 69\'),(160154,1207,8,\'37-TRIAL-Dimitri Payet  69\',\'MF\','1987-03-29 00:00:00',29,\'58-TRIAL-West Ham 0\'),(160159,1207,9,\'171-TRIAL-Olivier Giroud  164\',\'FD\','1986-09-30 00:00:00',29,\'117-TRIAL-Arsenal 15\'),(160158,1207,10,\'255-TRIAL-Andre-Pierre Gignac  15\',\'FD\','1985-12-05 00:00:00',30,\'30-TRIAL-Tigres 39\'),(160161,1207,11,\'212-TRIAL-Anthony Martial  188\',\'FD\','1995-12-05 00:00:00',20,\'182-TRIAL-Man. United 154\'),(160156,1207,12,\'185-TRIAL-Morgan Schneiderlin  10\',\'MF\','1989-11-08 00:00:00',26,\'184-TRIAL-Man. United 174\'),(160146,1207,13,\'280-TRIAL-Eliaquim Mangala  115\',\'DF\','1991-02-13 00:00:00',25,\'151-TRIAL-Man. City 241\'),(160153,1207,14,\'115-TRIAL-Blaise Matuidi  179\',\'MF\','1987-04-09 00:00:00',29,\'10-TRIAL-Paris 98\'),(160155,1207,15,\'273-TRIAL-Paul Pogba  188\',\'MF\','1993-03-15 00:00:00',23,\'277-TRIAL-Juventus 132\'),(160141,1207,16,\'256-TRIAL-Steve Mandanda  189\',\'GK\','1985-03-28 00:00:00',31,\'13-TRIAL-Marseille 108\'),(160142,1207,17,\'41-TRIAL-Lucas Digne  190\',\'DF\','1993-07-20 00:00:00',22,\'223-TRIAL-Roma 63\'),(160157,1207,18,\'28-TRIAL-Moussa Sissoko  284\',\'MF\','1989-08-16 00:00:00',26,\'178-TRIAL-Newcastle 0\'),(160148,1207,19,\'271-TRIAL-Bacary Sagna  85\',\'DF\','1983-02-14 00:00:00',33,\'74-TRIAL-Man. City 171\'),(160151,1207,20,\'233-TRIAL-Kingsley Coman  67\',\'MF\','1996-06-13 00:00:00',20,\'53-TRIAL-Bayern 195\'),(160145,1207,21,\'68-TRIAL-Laurent Koscielny  125\',\'DF\','1985-09-10 00:00:00',30,\'76-TRIAL-Arsenal 29\'),(160149,1207,22,\'150-TRIAL-Samuel Umtiti  198\',\'DF\','1993-11-14 00:00:00',22,\'9-TRIAL-Lyon 193\'),(160139,1207,23,\'186-TRIAL-Benoit Costil  80\',\'GK\','1987-07-03 00:00:00',28,\'216-TRIAL-Rennes 249\'),(160163,1208,1,\'267-TRIAL-Manuel Neuer  28\',\'GK\','1986-03-27 00:00:00',30,\'279-TRIAL-Bayern 64\'),(160170,1208,2,\'21-TRIAL-Shkodran Mustafi  5\',\'DF\','1992-04-17 00:00:00',24,\'126-TRIAL-Valencia 216\'),(160166,1208,3,\'16-TRIAL-Jonas Hector  126\',\'DF\','1990-05-27 00:00:00',26,\'166-TRIAL-Koln 287\'),(160167,1208,4,\'81-TRIAL-Benedikt Howedes  164\',\'DF\','1988-02-29 00:00:00',28,\'140-TRIAL-Schalke 286\'),(160168,1208,5,\'21-TRIAL-Mats Hummels  262\',\'DF\','1988-12-16 00:00:00',27,\'21-TRIAL-Dortmund 64\'),(160175,1208,6,\'209-TRIAL-Sami Khedira  15\',\'MF\','1987-04-04 00:00:00',29,\'202-TRIAL-Juventus 173\'),(160180,1208,7,\'24-TRIAL-Bastian Schweinsteiger  241\',\'MF\','1984-08-01 00:00:00',31,\'45-TRIAL-Man. United 262\'),(160177,1208,8,\'223-TRIAL-Mesut ozil  231\',\'MF\','1988-10-15 00:00:00',27,\'6-TRIAL-Arsenal 268\'),(160179,1208,9,\'18-TRIAL-Andre Schurrle  202\',\'MF\','1990-11-06 00:00:00',25,\'107-TRIAL-Wolfsburg 7\'),(160184,1208,10,\'81-TRIAL-Lukas Podolski  112\',\'FD\','1985-06-04 00:00:00',31,\'136-TRIAL-Galatasaray 230\'),(160173,1208,11,\'114-TRIAL-Julian Draxler  109\',\'MF\','1993-09-20 00:00:00',22,\'184-TRIAL-Wolfsburg 156\'),(160162,1208,12,\'290-TRIAL-Bernd Leno  293\',\'GK\','1992-03-04 00:00:00',24,\'296-TRIAL-Leverkusen 52\'),(160183,1208,13,\'154-TRIAL-Thomas Muller  145\',\'FD\','1989-09-13 00:00:00',26,\'8-TRIAL-Bayern 248\'),(160172,1208,14,\'291-TRIAL-Emre Can  212\',\'MF\','1994-01-12 00:00:00',22,\'31-TRIAL-Liverpool 114\'),(160181,1208,15,\'239-TRIAL-Julian Weigl  158\',\'MF\','1995-09-08 00:00:00',20,\'122-TRIAL-Dortmund 4\'),(160171,1208,16,\'95-TRIAL-Jonathan Tah  152\',\'DF\','1996-02-11 00:00:00',20,\'69-TRIAL-Leverkusen 279\'),(160165,1208,17,\'238-TRIAL-Jerome Boateng  23\',\'DF\','1988-09-03 00:00:00',27,\'18-TRIAL-Bayern 66\'),(160176,1208,18,\'259-TRIAL-Toni Kroos  98\',\'MF\','1990-01-04 00:00:00',26,\'286-TRIAL-Real Madrid 196\'),(160174,1208,19,\'262-TRIAL-Mario Gotze  233\',\'MF\','1992-06-03 00:00:00',24,\'158-TRIAL-Bayern 22\'),(160178,1208,20,\'246-TRIAL-Leroy Sane  92\',\'MF\','1996-01-11 00:00:00',20,\'137-TRIAL-Schalke 25\'),(160169,1208,21,\'147-TRIAL-Joshua Kimmich  58\',\'DF\','1995-02-08 00:00:00',21,\'2-TRIAL-Bayern 207\'),(160164,1208,22,\'198-TRIAL-Marc-Andre ter Stegen  230\',\'GK\','1992-04-30 00:00:00',24,\'192-TRIAL-Barcelona 200\'),(160182,1208,23,\'78-TRIAL-Mario Gomez  99\',\'FD\','1985-07-10 00:00:00',30,\'52-TRIAL-Besiktas 48\'),(160187,1209,1,\'82-TRIAL-Gabor Kiraly  240\',\'GK\','1976-04-01 00:00:00',40,\'115-TRIAL-Haladas 75\'),(160194,1209,2,\'62-TRIAL-Adam Lang  267\',\'DF\','1993-01-17 00:00:00',23,\'136-TRIAL-Videoton 97\'),(160193,1209,3,\'218-TRIAL-Mihaly Korhut  97\',\'DF\','1988-12-01 00:00:00',27,\'28-TRIAL-Debrecen 51\'),(160192,1209,4,\'116-TRIAL-Tamas Kadar  230\',\'DF\','1990-03-14 00:00:00',26,\'249-TRIAL-Lech 125\'),(160189,1209,5,\'58-TRIAL-Attila Fiola  229\',\'DF\','1990-02-17 00:00:00',26,\'220-TRIAL-Puskas Akademia 140\'),(160196,1209,6,\'160-TRIAL-Akos Elek  147\',\'MF\','1988-07-21 00:00:00',27,\'262-TRIAL-Diosgyor 155\'),(160202,1209,7,\'275-TRIAL-Balazs Dzsudzsak  192\',\'FD\','1986-12-23 00:00:00',29,\'161-TRIAL-Bursaspor 254\'),(160199,1209,8,\'198-TRIAL-Adam Nagy  46\',\'MF\','1995-06-17 00:00:00',21,\'14-TRIAL-Ferencvaros 246\'),(160207,1209,9,\'188-TRIAL-Adam Szalai  69\',\'FD\','1987-12-09 00:00:00',28,\'238-TRIAL-Hannover 63\'),(160203,1209,10,\'75-TRIAL-Zoltan Gera  215\',\'FD\','1979-04-22 00:00:00',37,\'121-TRIAL-Ferencvaros 175\'),(160204,1209,11,\'15-TRIAL-Krisztian Nemeth  128\',\'FD\','1989-01-05 00:00:00',27,\'34-TRIAL-Al-Gharafa 270\'),(160185,1209,12,\'5-TRIAL-Denes Dibusz  164\',\'GK\','1990-11-16 00:00:00',25,\'57-TRIAL-Ferencvaros 162\'),(160201,1209,13,\'61-TRIAL-Daniel Bode  224\',\'FD\','1986-10-24 00:00:00',29,\'249-TRIAL-Ferencvaros 69\'),(160198,1209,14,\'30-TRIAL-Gergo Lovrencsics  123\',\'MF\','1988-09-01 00:00:00',27,\'150-TRIAL-Lech 233\'),(160197,1209,15,\'125-TRIAL-Laszlo Kleinheisler  110\',\'MF\','1994-04-08 00:00:00',22,\'237-TRIAL-Bremen 136\'),(160195,1209,16,\'37-TRIAL-adam Pinter  78\',\'DF\','1988-06-12 00:00:00',28,\'93-TRIAL-Ferencvaros 136\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_mast" ("player_id","team_id","jersey_no","player_name","posi_to_play","dt_of_bir","age","playing_club") VALUES (160205,1209,17,\'114-TRIAL-Nemanja Nikolic  264\',\'FD\','1987-12-31 00:00:00',28,\'91-TRIAL-Legia 149\'),(160200,1209,18,\'235-TRIAL-Zoltan Stieber  105\',\'MF\','1988-10-16 00:00:00',27,\'137-TRIAL-Nurnberg 104\'),(160206,1209,19,\'137-TRIAL-Tamas Priskin  223\',\'FD\','1986-09-27 00:00:00',29,\'164-TRIAL-Slovan Bratislava 70\'),(160190,1209,20,\'108-TRIAL-Richard Guzmics  268\',\'DF\','1987-04-16 00:00:00',29,\'81-TRIAL-Wisla 185\'),(160188,1209,21,\'152-TRIAL-Barnabas Bese  73\',\'DF\','1994-05-06 00:00:00',22,\'152-TRIAL-MTK 94\'),(160186,1209,22,\'276-TRIAL-Peter Gulacsi  126\',\'GK\','1990-05-06 00:00:00',26,\'196-TRIAL-Leipzig 72\'),(160191,1209,23,\'249-TRIAL-Roland Juhasz  240\',\'DF\','1983-07-01 00:00:00',32,\'74-TRIAL-Videoton 219\'),(160208,1210,1,\'43-TRIAL-Hannes Halldorsson  211\',\'GK\','1984-04-27 00:00:00',32,\'141-TRIAL-Bodo/Glimt 189\'),(160216,1210,2,\'19-TRIAL-Birkir Saevarsson  165\',\'DF\','1984-11-11 00:00:00',31,\'205-TRIAL-Hammarby 85\'),(160212,1210,3,\'16-TRIAL-Haukur Heidar Hauksson  250\',\'DF\','1991-09-01 00:00:00',24,\'115-TRIAL-AIK 209\'),(160213,1210,4,\'264-TRIAL-Hjortur Hermannsson  166\',\'DF\','1995-02-08 00:00:00',21,\'93-TRIAL-Goteborg 174\'),(160214,1210,5,\'9-TRIAL-Sverrir Ingason  0\',\'DF\','1993-08-05 00:00:00',22,\'195-TRIAL-Lokeren 273\'),(160217,1210,6,\'289-TRIAL-Ragnar Sigurdsson  261\',\'DF\','1986-06-19 00:00:00',29,\'272-TRIAL-Krasnodar 268\'),(160229,1210,7,\'58-TRIAL-Johann Gudmundsson  231\',\'FD\','1990-10-27 00:00:00',25,\'168-TRIAL-Charlton 226\'),(160221,1210,8,\'10-TRIAL-Birkir Bjarnason  222\',\'MF\','1988-05-27 00:00:00',28,\'174-TRIAL-Basel 79\'),(160230,1210,9,\'10-TRIAL-Kolbeinn Sigthorsson  152\',\'FD\','1990-03-14 00:00:00',26,\'282-TRIAL-Nantes 191\'),(160224,1210,10,\'195-TRIAL-Gylfi Sigurdsson  64\',\'MF\','1989-09-08 00:00:00',26,\'274-TRIAL-Swansea 164\'),(160227,1210,11,\'202-TRIAL-Alfred Finnbogason  55\',\'FD\','1989-02-01 00:00:00',27,\'260-TRIAL-Augsburg 174\'),(160210,1210,12,\'72-TRIAL-Ogmundur Kristinsson  121\',\'GK\','1989-06-19 00:00:00',26,\'22-TRIAL-Hammarby 47\'),(160209,1210,13,\'277-TRIAL-Ingvar Jonsson  189\',\'GK\','1989-10-18 00:00:00',26,\'105-TRIAL-Sandefjord 195\'),(160220,1210,14,\'294-TRIAL-Kari Arnason  150\',\'MF\','1982-10-13 00:00:00',33,\'143-TRIAL-Malmo 154\'),(160226,1210,15,\'81-TRIAL-Jon Dadi Bodvarsson  112\',\'FD\','1992-05-25 00:00:00',24,\'272-TRIAL-Kaiserslautern 39\'),(160225,1210,16,\'228-TRIAL-Runar Mar Sigurjonsson  212\',\'MF\','1990-06-18 00:00:00',26,\'162-TRIAL-Sundsvall 267\'),(160222,1210,17,\'108-TRIAL-Aron Gunnarsson  215\',\'MF\','1989-04-22 00:00:00',27,\'108-TRIAL-Cardiff 123\'),(160211,1210,18,\'59-TRIAL-Elmar Bjarnason  34\',\'DF\','1987-03-04 00:00:00',29,\'104-TRIAL-AGF 86\'),(160215,1210,19,\'119-TRIAL-Hordur Magnusson  58\',\'DF\','1993-02-11 00:00:00',23,\'245-TRIAL-Cesena 106\'),(160223,1210,20,\'66-TRIAL-Emil Hallfredsson  100\',\'MF\','1984-06-29 00:00:00',31,\'267-TRIAL-Udinese 292\'),(160219,1210,21,\'187-TRIAL-Arnor Ingvi Traustason  32\',\'DF\','1993-04-30 00:00:00',23,\'256-TRIAL-Norrkoping 74\'),(160228,1210,22,\'147-TRIAL-Eidur Gudjohnsen  221\',\'FD\','1978-09-15 00:00:00',37,\'283-TRIAL-Molde 122\'),(160218,1210,23,\'231-TRIAL-Ari Skulason  276\',\'DF\','1987-05-14 00:00:00',29,\'83-TRIAL-OB 248\'),(160231,1211,1,\'123-TRIAL-Gianluigi Buffon  282\',\'GK\','1978-01-28 00:00:00',38,\'118-TRIAL-Juventus 76\'),(160238,1211,2,\'220-TRIAL-Mattia De Sciglio  211\',\'DF\','1992-10-20 00:00:00',23,\'82-TRIAL-Milan 156\'),(160236,1211,3,\'290-TRIAL-Giorgio Chiellini  225\',\'DF\','1984-08-14 00:00:00',31,\'124-TRIAL-Juventus 186\'),(160237,1211,4,\'177-TRIAL-Matteo Darmian  69\',\'DF\','1989-12-02 00:00:00',26,\'243-TRIAL-Man. United 34\'),(160239,1211,5,\'77-TRIAL-Angelo Ogbonna  168\',\'DF\','1988-05-23 00:00:00',28,\'168-TRIAL-West Ham 91\'),(160241,1211,6,\'96-TRIAL-Antonio Candreva  283\',\'MF\','1987-02-28 00:00:00',29,\'28-TRIAL-Lazio 227\'),(160253,1211,7,\'26-TRIAL-Simone Zaza  271\',\'FD\','1991-06-25 00:00:00',24,\'197-TRIAL-Juventus 212\'),(160243,1211,8,\'103-TRIAL-Alessandro Florenzi  227\',\'MF\','1991-03-11 00:00:00',25,\'8-TRIAL-Roma 145\'),(160252,1211,9,\'208-TRIAL-Graziano Pelle 285\',\'FD\','1985-07-15 00:00:00',30,\'238-TRIAL-Southampton 237\'),(160245,1211,10,\'43-TRIAL-Thiago Motta  13\',\'MF\','1982-08-28 00:00:00',33,\'1-TRIAL-Paris 150\'),(160250,1211,11,\'228-TRIAL-Ciro Immobile  11\',\'FD\','1990-02-20 00:00:00',26,\'250-TRIAL-Torino 249\'),(160233,1211,12,\'92-TRIAL-Salvatore Sirigu  254\',\'GK\','1987-01-12 00:00:00',29,\'269-TRIAL-Paris 81\'),(160232,1211,13,\'165-TRIAL-Federico Marchetti  167\',\'GK\','1983-02-07 00:00:00',33,\'13-TRIAL-Lazio 293\'),(160247,1211,14,\'34-TRIAL-Stefano Sturaro  272\',\'MF\','1993-03-09 00:00:00',23,\'272-TRIAL-Juventus 30\'),(160234,1211,15,\'1-TRIAL-Andrea Barzagli  242\',\'DF\','1981-05-08 00:00:00',35,\'77-TRIAL-Juventus 77\'),(160242,1211,16,\'270-TRIAL-Daniele De Rossi  102\',\'MF\','1983-07-24 00:00:00',32,\'264-TRIAL-Roma 181\'),(160248,1211,17,\'90-TRIAL-Eder  123\',\'FD\','1986-11-15 00:00:00',29,\'137-TRIAL-Internazionale 223\'),(160246,1211,18,\'79-TRIAL-Marco Parolo  95\',\'MF\','1985-01-25 00:00:00',31,\'69-TRIAL-Lazio 227\'),(160235,1211,19,\'42-TRIAL-Leonardo Bonucci  110\',\'DF\','1987-05-01 00:00:00',29,\'282-TRIAL-Juventus 258\'),(160251,1211,20,\'126-TRIAL-Lorenzo Insigne  187\',\'FD\','1991-06-04 00:00:00',25,\'170-TRIAL-Napoli 128\'),(160240,1211,21,\'251-TRIAL-Federico Bernardeschi  158\',\'MF\','1994-02-16 00:00:00',22,\'13-TRIAL-Fiorentina 260\'),(160249,1211,22,\'283-TRIAL-Stephan El Shaarawy  286\',\'FD\','1992-10-27 00:00:00',23,\'42-TRIAL-Roma 210\'),(160244,1211,23,\'272-TRIAL-Emanuele Giaccherini  228\',\'MF\','1985-05-05 00:00:00',31,\'134-TRIAL-Bologna 141\'),(160256,1212,1,\'18-TRIAL-Michael McGovern  203\',\'GK\','1984-07-12 00:00:00',31,\'167-TRIAL-Hamilton 265\'),(160264,1212,2,\'138-TRIAL-Conor McLaughlin  81\',\'DF\','1991-07-26 00:00:00',24,\'257-TRIAL-Fleetwood 250\'),(160269,1212,3,\'114-TRIAL-Shane Ferguson  298\',\'MF\','1991-07-12 00:00:00',24,\'258-TRIAL-Millwall 261\'),(160262,1212,4,\'263-TRIAL-Gareth McAuley  56\',\'DF\','1979-12-05 00:00:00',36,\'107-TRIAL-West Brom 178\'),(160259,1212,5,\'289-TRIAL-Jonny Evans  235\',\'DF\','1988-01-03 00:00:00',28,\'65-TRIAL-West Brom 275\'),(160257,1212,6,\'86-TRIAL-Chris Baird  186\',\'DF\','1982-02-25 00:00:00',34,\'33-TRIAL-Fulham 260\'),(160275,1212,7,\'130-TRIAL-Niall McGinn  248\',\'FD\','1987-07-20 00:00:00',28,\'228-TRIAL-Aberdeen 92\'),(160267,1212,8,\'133-TRIAL-Steven Davis  140\',\'MF\','1985-01-01 00:00:00',31,\'166-TRIAL-Southampton 235\'),(160273,1212,9,\'10-TRIAL-Will Grigg  199\',\'FD\','1991-07-03 00:00:00',24,\'137-TRIAL-Wigan 292\'),(160274,1212,10,\'182-TRIAL-Kyle Lafferty  128\',\'FD\','1987-09-16 00:00:00',28,\'252-TRIAL-Birmingham 269\'),(160276,1212,11,\'44-TRIAL-Conor Washington  194\',\'FD\','1992-05-18 00:00:00',24,\'8-TRIAL-QPR 252\'),(160254,1212,12,\'247-TRIAL-Roy Carroll 32\',\'GK\','1977-09-30 00:00:00',38,\'235-TRIAL-Notts County 8\'),(160268,1212,13,\'264-TRIAL-Corry Evans  197\',\'MF\','1990-07-30 00:00:00',25,\'143-TRIAL-Blackburn 49\'),(160266,1212,14,\'115-TRIAL-Stuart Dallas  141\',\'MF\','1991-04-19 00:00:00',25,\'189-TRIAL-Leeds 200\'),(160263,1212,15,\'12-TRIAL-Luke McCullough  48\',\'DF\','1994-02-15 00:00:00',22,\'223-TRIAL-Doncaster 51\'),(160271,1212,16,\'174-TRIAL-Oliver Norwood  133\',\'MF\','1991-04-12 00:00:00',25,\'191-TRIAL-Reading 200\'),(160265,1212,17,\'54-TRIAL-Paddy McNair  90\',\'DF\','1995-04-27 00:00:00',21,\'197-TRIAL-Man. United 119\'),(160261,1212,18,\'80-TRIAL-Aaron Hughes  78\',\'DF\','1979-11-08 00:00:00',36,\'31-TRIAL-Melbourne City 144\'),(160272,1212,19,\'40-TRIAL-Jamie Ward  287\',\'MF\','1986-05-12 00:00:00',30,\'299-TRIAL-Nottm Forest 25\'),(160258,1212,20,\'83-TRIAL-Craig Cathcart  138\',\'DF\','1989-02-06 00:00:00',27,\'292-TRIAL-Watford 193\'),(160270,1212,21,\'52-TRIAL-Josh Magennis  111\',\'MF\','1990-08-15 00:00:00',25,\'60-TRIAL-Kilmarnock 234\'),(160260,1212,22,\'40-TRIAL-Lee Hodson  297\',\'DF\','1991-10-02 00:00:00',24,\'85-TRIAL-MK Dons 229\'),(160255,1212,23,\'40-TRIAL-Alan Mannus  205\',\'GK\','1982-05-19 00:00:00',34,\'291-TRIAL-St Johnstone 192\'),(160279,1213,1,\'10-TRIAL-Wojciech Szczesny  49\',\'GK\','1990-04-18 00:00:00',26,\'278-TRIAL-Roma 279\'),(160283,1213,2,\'71-TRIAL-Michal Pazdan  277\',\'DF\','1987-09-21 00:00:00',28,\'73-TRIAL-Legia 93\'),(160282,1213,3,\'120-TRIAL-Artur Jedrzejczyk  197\',\'DF\','1987-11-04 00:00:00',28,\'26-TRIAL-Legia 76\'),(160280,1213,4,\'290-TRIAL-Thiago Cionek  282\',\'DF\','1986-04-21 00:00:00',30,\'78-TRIAL-Palermo 59\'),(160293,1213,5,\'18-TRIAL-Krzysztof Maczynski  89\',\'MF\','1987-05-23 00:00:00',29,\'159-TRIAL-Wisla 149\'),(160289,1213,6,\'24-TRIAL-Tomasz Jodlowiec  72\',\'MF\','1985-09-08 00:00:00',30,\'180-TRIAL-Legia 8\'),(160298,1213,7,\'67-TRIAL-Arkadiusz Milik  8\',\'FD\','1994-02-28 00:00:00',22,\'177-TRIAL-Ajax 203\'),(160292,1213,8,\'70-TRIAL-Karol Linetty  207\',\'MF\','1995-02-02 00:00:00',21,\'96-TRIAL-Lech 74\'),(160297,1213,9,\'222-TRIAL-Robert Lewandowski  11\',\'FD\','1988-08-21 00:00:00',27,\'119-TRIAL-Bayern 261\'),(160291,1213,10,\'256-TRIAL-Grzegorz Krychowiak  290\',\'MF\','1990-01-29 00:00:00',26,\'263-TRIAL-Sevilla 183\'),(160288,1213,11,\'216-TRIAL-Kamil Grosicki  132\',\'MF\','1988-06-08 00:00:00',28,\'252-TRIAL-Rennes 41\'),(160277,1213,12,\'154-TRIAL-Artur Boruc  213\',\'GK\','1980-02-20 00:00:00',36,\'162-TRIAL-Bournemouth 196\'),(160299,1213,13,\'160-TRIAL-Mariusz Stepinski  215\',\'FD\','1995-05-12 00:00:00',21,\'104-TRIAL-Ruch 199\'),(160286,1213,14,\'36-TRIAL-Jakub Wawrzyniak  180\',\'DF\','1983-07-07 00:00:00',32,\'198-TRIAL-Lechia 32\'),(160281,1213,15,\'287-TRIAL-Kamil Glik  184\',\'DF\','1988-02-03 00:00:00',28,\'40-TRIAL-Torino 217\'),(160287,1213,16,\'206-TRIAL-Jakub Blaszczykowski  270\',\'MF\','1985-12-14 00:00:00',30,\'241-TRIAL-Fiorentina 282\'),(160294,1213,17,\'49-TRIAL-Slawomir Peszko  223\',\'MF\','1985-02-19 00:00:00',31,\'258-TRIAL-Lechia 105\'),(160285,1213,18,\'221-TRIAL-Bartosz Salamon  295\',\'DF\','1991-05-01 00:00:00',25,\'96-TRIAL-Cagliari 16\'),(160296,1213,19,\'78-TRIAL-Piotr Zielinski  278\',\'MF\','1994-05-20 00:00:00',22,\'79-TRIAL-Empoli 158\'),(160284,1213,20,\'277-TRIAL-Lukasz Piszczek  150\',\'DF\','1985-06-03 00:00:00',31,\'207-TRIAL-Dortmund 29\'),(160290,1213,21,\'81-TRIAL-Bartosz Kapustka  295\',\'MF\','1996-12-23 00:00:00',19,\'278-TRIAL- 76\'),(160278,1213,22,\'153-TRIAL-Lukasz Fabianski  199\',\'GK\','1985-04-18 00:00:00',31,\'84-TRIAL-Swansea 265\'),(160295,1213,23,\'93-TRIAL-Filip Starzynski  108\',\'MF\','1991-05-27 00:00:00',25,\'172-TRIAL-Zaglebie 143\'),(160302,1214,1,\'229-TRIAL-Rui Patricio  14\',\'GK\','1988-02-15 00:00:00',28,\'268-TRIAL-Sporting CP 255\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_mast" ("player_id","team_id","jersey_no","player_name","posi_to_play","dt_of_bir","age","playing_club") VALUES (160303,1214,2,\'91-TRIAL-Bruno Alves  273\',\'DF\','1981-11-27 00:00:00',34,\'222-TRIAL-Fenerbahce 148\'),(160307,1214,3,\'251-TRIAL-Pepe  186\',\'DF\','1983-02-26 00:00:00',33,\'44-TRIAL-Real Madrid 246\'),(160306,1214,4,\'77-TRIAL-Jose Fonte  117\',\'DF\','1983-12-22 00:00:00',32,\'229-TRIAL-Southampton 216\'),(160308,1214,5,\'174-TRIAL-Raphael Guerreiro  191\',\'DF\','1993-12-22 00:00:00',22,\'169-TRIAL-Lorient 112\'),(160309,1214,6,\'46-TRIAL-Ricardo Carvalho  93\',\'DF\','1978-05-18 00:00:00',38,\'91-TRIAL-Monaco 215\'),(160322,1214,7,\'249-TRIAL-Cristiano Ronaldo  157\',\'FD\','1985-02-05 00:00:00',31,\'240-TRIAL-Real Madrid 252\'),(160314,1214,8,\'236-TRIAL-Joao Moutinho  151\',\'MF\','1986-09-08 00:00:00',29,\'187-TRIAL-Monaco 26\'),(160319,1214,9,\'262-TRIAL-Eder  155\',\'FD\','1987-12-22 00:00:00',28,\'83-TRIAL-LOSC 294\'),(160313,1214,10,\'180-TRIAL-Joao Mario  197\',\'MF\','1993-01-19 00:00:00',23,\'65-TRIAL-Sporting CP 65\'),(160317,1214,11,\'113-TRIAL-Vieirinha  261\',\'MF\','1986-01-24 00:00:00',30,\'278-TRIAL-Wolfsburg 78\'),(160301,1214,12,\'78-TRIAL-Anthony Lopes  40\',\'GK\','1990-10-01 00:00:00',25,\'111-TRIAL-Lyon 147\'),(160312,1214,13,\'45-TRIAL-Danilo  170\',\'MF\','1991-09-09 00:00:00',24,\'275-TRIAL-Porto 289\'),(160318,1214,14,\'150-TRIAL-William Carvalho  149\',\'MF\','1992-04-07 00:00:00',24,\'33-TRIAL-Sporting CP 65\'),(160311,1214,15,\'14-TRIAL-Andre Gomes  182\',\'MF\','1993-07-30 00:00:00',22,\'7-TRIAL-Valencia 132\'),(160316,1214,16,\'196-TRIAL-Renato Sanches  167\',\'MF\','1997-08-18 00:00:00',18,\'22-TRIAL-Benfica 82\'),(160320,1214,17,\'10-TRIAL-Nani  241\',\'FD\','1986-11-17 00:00:00',29,\'31-TRIAL-Fenerbahce 87\'),(160315,1214,18,\'105-TRIAL-Rafa Silva  179\',\'MF\','1993-05-17 00:00:00',23,\'21-TRIAL-Braga 238\'),(160305,1214,19,\'151-TRIAL-Eliseu  247\',\'DF\','1983-10-01 00:00:00',32,\'208-TRIAL-Benfica 46\'),(160321,1214,20,\'76-TRIAL-Ricardo Quaresma  259\',\'FD\','1983-09-26 00:00:00',32,\'189-TRIAL-Besiktas 122\'),(160304,1214,21,\'66-TRIAL-Cedric  86\',\'DF\','1991-08-31 00:00:00',24,\'155-TRIAL-Southampton 228\'),(160300,1214,22,\'214-TRIAL-Eduardo  60\',\'GK\','1982-09-19 00:00:00',33,\'153-TRIAL-Dinamo Zagreb 77\'),(160310,1214,23,\'148-TRIAL-Adrien Silva  203\',\'MF\','1989-03-15 00:00:00',27,\'61-TRIAL-Sporting CP 231\'),(160325,1215,1,\'282-TRIAL-Keiren Westwood  155\',\'GK\','1984-10-23 00:00:00',31,\'97-TRIAL-Sheff. Wednesday 206\'),(160328,1215,2,\'52-TRIAL-Seamus Coleman  221\',\'DF\','1988-10-11 00:00:00',27,\'196-TRIAL-Everton 181\'),(160327,1215,3,\'221-TRIAL-Ciaran Clark  155\',\'DF\','1989-09-26 00:00:00',26,\'47-TRIAL-Aston Villa 124\'),(160331,1215,4,\'18-TRIAL-John OShea  135\',\'DF\','1981-04-30 00:00:00',35,\'276-TRIAL-Sunderland 274\'),(160330,1215,5,\'159-TRIAL-Richard Keogh  98\',\'DF\','1986-08-11 00:00:00',29,\'74-TRIAL-Derby 253\'),(160341,1215,6,\'22-TRIAL-Glenn Whelan  135\',\'MF\','1984-01-13 00:00:00',32,\'143-TRIAL-Stoke 88\'),(160338,1215,7,\'53-TRIAL-Aiden McGeady  32\',\'MF\','1986-04-04 00:00:00',30,\'247-TRIAL-Sheff. Wednesday 180\'),(160336,1215,8,\'126-TRIAL-James McCarthy  178\',\'MF\','1990-11-12 00:00:00',25,\'150-TRIAL-Everton 101\'),(160343,1215,9,\'61-TRIAL-Shane Long  99\',\'FD\','1987-01-22 00:00:00',29,\'155-TRIAL-Southampton 263\'),(160342,1215,10,\'16-TRIAL-Robbie Keane  73\',\'FD\','1980-07-08 00:00:00',35,\'61-TRIAL-LA Galaxy 145\'),(160337,1215,11,\'173-TRIAL-James McClean  74\',\'MF\','1989-04-22 00:00:00',27,\'50-TRIAL-West Brom 53\'),(160329,1215,12,\'281-TRIAL-Shane Duffy  87\',\'DF\','1992-01-01 00:00:00',24,\'299-TRIAL-Blackburn 110\'),(160334,1215,13,\'43-TRIAL-Jeff Hendrick  65\',\'MF\','1992-01-31 00:00:00',24,\'272-TRIAL-Derby 129\'),(160345,1215,14,\'81-TRIAL-Jon Walters  12\',\'FD\','1983-09-20 00:00:00',32,\'276-TRIAL-Stoke 181\'),(160326,1215,15,\'147-TRIAL-Cyrus Christie  190\',\'DF\','1992-09-30 00:00:00',23,\'171-TRIAL-Derby 105\'),(160323,1215,16,\'272-TRIAL-Shay Given  32\',\'GK\','1976-04-20 00:00:00',40,\'89-TRIAL-Stoke 20\'),(160332,1215,17,\'65-TRIAL-Stephen Ward  131\',\'DF\','1985-08-20 00:00:00',30,\'58-TRIAL-Burnley 193\'),(160339,1215,18,\'106-TRIAL-David Meyler  178\',\'MF\','1989-05-29 00:00:00',27,\'148-TRIAL-Hull 106\'),(160333,1215,19,\'171-TRIAL-Robbie Brady  166\',\'MF\','1992-01-14 00:00:00',24,\'96-TRIAL-Norwich 197\'),(160335,1215,20,\'120-TRIAL-Wes Hoolahan  294\',\'MF\','1982-05-20 00:00:00',34,\'229-TRIAL-Norwich 88\'),(160344,1215,21,\'109-TRIAL-Daryl Murphy  284\',\'FD\','1983-03-15 00:00:00',33,\'269-TRIAL-Ipswich 178\'),(160340,1215,22,\'17-TRIAL-Stephen Quinn  115\',\'MF\','1986-04-01 00:00:00',30,\'126-TRIAL-Reading 84\'),(160324,1215,23,\'168-TRIAL-Darren Randolph  206\',\'GK\','1987-05-12 00:00:00',29,\'128-TRIAL-West Ham 97\'),(160347,1216,1,\'218-TRIAL-Costel Pantilimon  90\',\'GK\','1987-02-01 00:00:00',29,\'199-TRIAL-Watford 85\'),(160353,1216,2,\'86-TRIAL-Alexandru Matel  299\',\'DF\','1989-10-17 00:00:00',26,\'120-TRIAL-Dinamo Zagreb 10\'),(160355,1216,3,\'271-TRIAL-Razvan Rat  213\',\'DF\','1981-05-26 00:00:00',35,\'115-TRIAL-Rayo Vallecano 85\'),(160354,1216,4,\'18-TRIAL-Cosmin Moti  280\',\'DF\','1984-12-03 00:00:00',31,\'131-TRIAL-Ludogorets 67\'),(160358,1216,5,\'287-TRIAL-Ovidiu Hoban  244\',\'MF\','1982-12-27 00:00:00',33,\'86-TRIAL-H. Beer-Sheva 107\'),(160349,1216,6,\'160-TRIAL-Vlad Chiriches  127\',\'DF\','1989-11-14 00:00:00',26,\'174-TRIAL-Napoli 31\'),(160357,1216,7,\'252-TRIAL-Alexandru Chipciu  271\',\'MF\','1989-05-18 00:00:00',27,\'68-TRIAL-Steaua 193\'),(160359,1216,8,\'85-TRIAL-Mihai Pintilii  37\',\'MF\','1984-11-09 00:00:00',31,\'111-TRIAL-Steaua 204\'),(160365,1216,9,\'77-TRIAL-Denis Alibec  106\',\'FD\','1991-01-05 00:00:00',25,\'268-TRIAL-Astra 222\'),(160363,1216,10,\'213-TRIAL-Nicolae Stanciu  200\',\'MF\','1993-05-07 00:00:00',23,\'242-TRIAL-Steaua 137\'),(160364,1216,11,\'38-TRIAL-Gabriel Torje  88\',\'MF\','1989-11-22 00:00:00',26,\'155-TRIAL-Osmanlispor 89\'),(160348,1216,12,\'147-TRIAL-Ciprian Tatarusanu  181\',\'GK\','1986-02-09 00:00:00',30,\'193-TRIAL-Fiorentina 84\'),(160367,1216,13,\'187-TRIAL-Claudiu Keeru  261\',\'FD\','1986-12-02 00:00:00',29,\'93-TRIAL-Ludogorets 117\'),(160366,1216,14,\'201-TRIAL-Florin Andone  82\',\'FD\','1993-04-11 00:00:00',23,\'47-TRIAL-Cordoba 65\'),(160351,1216,15,\'253-TRIAL-Valerica Gaman  204\',\'DF\','1989-02-25 00:00:00',27,\'84-TRIAL-Astra 195\'),(160350,1216,16,\'25-TRIAL-Steliano Filip  221\',\'DF\','1994-05-15 00:00:00',22,\'64-TRIAL-Dinamo Bucuresti 181\'),(160362,1216,17,\'72-TRIAL-Lucian Sanmartean  6\',\'MF\','1980-03-13 00:00:00',36,\'56-TRIAL-Al-Ittihad 43\'),(160361,1216,18,\'93-TRIAL-Andrei Prepelita  80\',\'MF\','1985-12-08 00:00:00',30,\'180-TRIAL-Ludogorets 168\'),(160368,1216,19,\'111-TRIAL-Bogdan Stancu  213\',\'FD\','1987-06-28 00:00:00',28,\'268-TRIAL-Genclerbirligi 251\'),(160360,1216,20,\'216-TRIAL-Adrian Popa  79\',\'MF\','1988-07-24 00:00:00',27,\'268-TRIAL-Steaua 240\'),(160352,1216,21,\'31-TRIAL-Dragos Grigore  33\',\'DF\','1986-09-07 00:00:00',29,\'79-TRIAL-Al-Sailiya 263\'),(160356,1216,22,\'259-TRIAL-Cristian Sapunaru  253\',\'DF\','1984-04-05 00:00:00',32,\'36-TRIAL-Pandurii 295\'),(160346,1216,23,\'65-TRIAL-Silviu Lung  174\',\'GK\','1989-06-04 00:00:00',27,\'220-TRIAL-Astra 135\'),(160369,1217,1,\'180-TRIAL-Igor Akinfeev  276\',\'GK\','1986-04-08 00:00:00',30,\'155-TRIAL-CSKA Moskva 25\'),(160378,1217,2,\'171-TRIAL-Roman Shishkin  208\',\'DF\','1987-01-27 00:00:00',29,\'59-TRIAL-Lokomotiv Moskva 156\'),(160379,1217,3,\'202-TRIAL-Igor Smolnikov  132\',\'DF\','1988-08-08 00:00:00',27,\'105-TRIAL-Zenit 240\'),(160374,1217,4,\'175-TRIAL-Sergei Ignashevich  262\',\'DF\','1979-07-14 00:00:00',36,\'85-TRIAL-CSKA Moskva 62\'),(160376,1217,5,\'80-TRIAL-Roman Neustadter  36\',\'DF\','1988-02-18 00:00:00',28,\'297-TRIAL-Schalke 2\'),(160372,1217,6,\'8-TRIAL-Aleksei Berezutski  180\',\'DF\','1982-06-20 00:00:00',33,\'240-TRIAL-CSKA Moskva 76\'),(160388,1217,7,\'58-TRIAL-Artur Yusupov  293\',\'MF\','1989-09-01 00:00:00',26,\'240-TRIAL-Zenit 146\'),(160380,1217,8,\'74-TRIAL-Denis Glushakov  173\',\'MF\','1987-01-27 00:00:00',29,\'197-TRIAL-Spartak Moskva 180\'),(160390,1217,9,\'235-TRIAL-Aleksandr Kokorin  272\',\'FD\','1991-03-19 00:00:00',25,\'0-TRIAL-Zenit 107\'),(160391,1217,10,\'155-TRIAL-Fedor Smolov  266\',\'FD\','1990-02-09 00:00:00',26,\'241-TRIAL-Krasnodar 188\'),(160383,1217,11,\'181-TRIAL-Pavel Mamaev  68\',\'MF\','1988-09-17 00:00:00',27,\'115-TRIAL-Krasnodar 196\'),(160371,1217,12,\'25-TRIAL-Yuri Lodygin  109\',\'GK\','1990-05-26 00:00:00',26,\'112-TRIAL-Zenit 136\'),(160381,1217,13,\'55-TRIAL-Aleksandr Golovin  162\',\'MF\','1996-05-30 00:00:00',20,\'143-TRIAL-CSKA Moskva 142\'),(160373,1217,14,\'21-TRIAL-Vasili Berezutski  222\',\'DF\','1982-06-20 00:00:00',33,\'212-TRIAL-CSKA Moskva 248\'),(160386,1217,15,\'218-TRIAL-Roman Shirokov  68\',\'MF\','1981-07-06 00:00:00',34,\'17-TRIAL-CSKA Moskva 114\'),(160370,1217,16,\'250-TRIAL-Guilherme  90\',\'GK\','1985-12-12 00:00:00',30,\'35-TRIAL-Lokomotiv Moskva 159\'),(160385,1217,17,\'169-TRIAL-Oleg Shatov  295\',\'MF\','1990-07-29 00:00:00',25,\'203-TRIAL-Zenit 140\'),(160382,1217,18,\'79-TRIAL-Oleg Ivanov  199\',\'MF\','1986-08-04 00:00:00',29,\'5-TRIAL-Terek 191\'),(160384,1217,19,\'61-TRIAL-Aleksandr Samedov  281\',\'MF\','1984-07-19 00:00:00',31,\'52-TRIAL-Lokomotiv Moskva 53\'),(160387,1217,20,\'33-TRIAL-Dmitri Torbinski  229\',\'MF\','1984-04-28 00:00:00',32,\'87-TRIAL-Krasnodar 142\'),(160377,1217,21,\'153-TRIAL-Georgi Schennikov  283\',\'DF\','1991-04-27 00:00:00',25,\'20-TRIAL-CSKA Moskva 214\'),(160389,1217,22,\'18-TRIAL-Artem Dzyuba  244\',\'FD\','1988-08-22 00:00:00',27,\'163-TRIAL-Zenit 29\'),(160375,1217,23,\'252-TRIAL-Dmitri Kombarov  264\',\'DF\','1987-01-22 00:00:00',29,\'269-TRIAL-Spartak Moskva 170\'),(160393,1218,1,\'5-TRIAL-Jan Mucha  47\',\'GK\','1982-12-05 00:00:00',33,\'94-TRIAL-Slovan Bratislava 187\'),(160398,1218,2,\'26-TRIAL-Peter Pekarik  276\',\'DF\','1986-10-30 00:00:00',29,\'23-TRIAL-Hertha 240\'),(160401,1218,3,\'179-TRIAL-Martin Skrtel  290\',\'DF\','1984-12-15 00:00:00',31,\'188-TRIAL-Liverpool 110\'),(160395,1218,4,\'171-TRIAL-Jan Durica  245\',\'DF\','1981-12-10 00:00:00',34,\'121-TRIAL-Lokomotiv Moskva 270\'),(160396,1218,5,\'83-TRIAL-Norbert Gyomber  189\',\'DF\','1992-07-03 00:00:00',23,\'255-TRIAL-Roma 178\'),(160404,1218,6,\'179-TRIAL-Jan Gregus 206\',\'MF\','1991-01-29 00:00:00',25,\'62-TRIAL-Jablonec 35\'),(160411,1218,7,\'87-TRIAL-Vladimir Weiss  196\',\'MF\','1989-11-30 00:00:00',26,\'233-TRIAL-Al-Gharafa 288\'),(160403,1218,8,\'35-TRIAL-Ondrej Duda  279\',\'MF\','1994-12-05 00:00:00',21,\'93-TRIAL-Legia 90\'),(160414,1218,9,\'62-TRIAL-Stanislav Sestak  65\',\'FD\','1982-12-16 00:00:00',33,\'201-TRIAL-Ferencvaros 205\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_mast" ("player_id","team_id","jersey_no","player_name","posi_to_play","dt_of_bir","age","playing_club") VALUES (160410,1218,10,\'107-TRIAL-Miroslav Stoch  267\',\'MF\','1989-10-19 00:00:00',26,\'269-TRIAL-Bursaspor 134\'),(160413,1218,11,\'271-TRIAL-Adam Nemec  257\',\'FD\','1985-09-02 00:00:00',30,\'98-TRIAL-Willem II 245\'),(160394,1218,12,\'97-TRIAL-Jan Novota  118\',\'GK\','1983-11-29 00:00:00',32,\'138-TRIAL-Rapid Wien 144\'),(160406,1218,13,\'172-TRIAL-Patrik HroSovsky  163\',\'MF\','1992-04-22 00:00:00',24,\'28-TRIAL-Plzen 164\'),(160400,1218,14,\'1-TRIAL-Milan Skriniar  23\',\'DF\','1995-02-11 00:00:00',21,\'290-TRIAL-Sampdoria 104\'),(160397,1218,15,\'101-TRIAL-TomasHubocan  227\',\'DF\','1985-09-17 00:00:00',30,\'97-TRIAL-Dinamo Moskva 292\'),(160399,1218,16,\'271-TRIAL-Kornel Salata  263\',\'DF\','1985-01-24 00:00:00',31,\'201-TRIAL-Slovan Bratislava 163\'),(160405,1218,17,\'221-TRIAL-Marek Hamsik  265\',\'MF\','1987-07-27 00:00:00',28,\'21-TRIAL-Napoli 45\'),(160402,1218,18,\'10-TRIAL-Dusan Svento  195\',\'DF\','1985-08-01 00:00:00',30,\'241-TRIAL-Koln 22\'),(160407,1218,19,\'12-TRIAL-Juraj Kucka  51\',\'MF\','1987-02-26 00:00:00',29,\'215-TRIAL-Milan 255\'),(160408,1218,20,\'93-TRIAL-Robert Mak  38\',\'MF\','1991-03-08 00:00:00',25,\'279-TRIAL-PAOK 82\'),(160412,1218,21,\'108-TRIAL-Michal Duris 54\',\'FD\','1988-06-01 00:00:00',28,\'222-TRIAL-Plzen 7\'),(160409,1218,22,\'245-TRIAL-Viktor Pecovsky  138\',\'MF\','1983-05-24 00:00:00',33,\'144-TRIAL-zilina 90\'),(160392,1218,23,\'139-TRIAL-MatusKozacik  54\',\'GK\','1983-12-27 00:00:00',32,\'4-TRIAL-Plzen 123\'),(160415,1219,1,\'25-TRIAL-Lker Casillas  278\',\'GK\','1981-05-20 00:00:00',35,\'124-TRIAL-Porto 181\'),(160418,1219,2,\'230-TRIAL-Cesar Azpilicueta  33\',\'DF\','1989-08-28 00:00:00',26,\'23-TRIAL-Chelsea 194\'),(160423,1219,3,\'30-TRIAL-Gerard Pique  246\',\'DF\','1987-02-02 00:00:00',29,\'187-TRIAL-Barcelona 45\'),(160419,1219,4,\'205-TRIAL-Marc Bartra  216\',\'DF\','1991-01-15 00:00:00',25,\'50-TRIAL-Barcelona 89\'),(160427,1219,5,\'38-TRIAL-Sergio Busquets  63\',\'MF\','1988-07-16 00:00:00',27,\'235-TRIAL-Barcelona 297\'),(160429,1219,6,\'109-TRIAL-Andres Iniesta  30\',\'MF\','1984-05-11 00:00:00',32,\'124-TRIAL-Barcelona 108\'),(160435,1219,7,\'37-TRIAL-Alvaro Morata  174\',\'FD\','1992-10-23 00:00:00',23,\'120-TRIAL-Juventus 72\'),(160430,1219,8,\'93-TRIAL-Koke  255\',\'MF\','1992-01-08 00:00:00',24,\'134-TRIAL-Atletico 261\'),(160434,1219,9,\'156-TRIAL-Lucas Vazquez  6\',\'FD\','1991-07-01 00:00:00',24,\'84-TRIAL-Real Madrid 175\'),(160428,1219,10,\'182-TRIAL-Cesc Fabregas  19\',\'MF\','1987-05-04 00:00:00',29,\'141-TRIAL-Chelsea 132\'),(160437,1219,11,\'84-TRIAL-Pedro Rodriguez  179\',\'FD\','1987-07-28 00:00:00',28,\'279-TRIAL-Chelsea 283\'),(160420,1219,12,\'267-TRIAL-Hector Bellerin  236\',\'DF\','1995-03-19 00:00:00',21,\'125-TRIAL-Arsenal 118\'),(160416,1219,13,\'137-TRIAL-David de Gea  28\',\'GK\','1990-11-07 00:00:00',25,\'19-TRIAL-Man. United 177\'),(160432,1219,14,\'237-TRIAL-Thiago Alcantara  291\',\'MF\','1991-04-11 00:00:00',25,\'156-TRIAL-Bayern 295\'),(160424,1219,15,\'160-TRIAL-Sergio Ramos  1\',\'DF\','1986-03-30 00:00:00',30,\'93-TRIAL-Real Madrid 132\'),(160422,1219,16,\'36-TRIAL-Juanfran  80\',\'DF\','1985-01-09 00:00:00',31,\'175-TRIAL-Atletico 207\'),(160425,1219,17,\'184-TRIAL-Mikel San Jose  174\',\'DF\','1989-05-30 00:00:00',27,\'19-TRIAL-Athletic 90\'),(160421,1219,18,\'76-TRIAL-Jordi Alba  41\',\'DF\','1989-03-21 00:00:00',27,\'51-TRIAL-Barcelona 229\'),(160426,1219,19,\'90-TRIAL-Bruno Soriano  174\',\'MF\','1984-06-12 00:00:00',32,\'272-TRIAL-Villarreal 291\'),(160433,1219,20,\'189-TRIAL-Aritz Aduriz  187\',\'FD\','1981-02-11 00:00:00',35,\'290-TRIAL-Athletic 239\'),(160431,1219,21,\'193-TRIAL-David Silva  53\',\'MF\','1986-01-08 00:00:00',30,\'263-TRIAL-Man. City 181\'),(160436,1219,22,\'103-TRIAL-Nolito  5\',\'FD\','1986-10-15 00:00:00',29,\'176-TRIAL-Celta 179\'),(160417,1219,23,\'195-TRIAL-Sergio Rico  139\',\'GK\','1993-09-01 00:00:00',22,\'68-TRIAL-Sevilla 198\'),(160439,1220,1,\'83-TRIAL-Andreas Isaksson  239\',\'GK\','1981-10-03 00:00:00',34,\'115-TRIAL-Kasimpasa 21\'),(160446,1220,2,\'293-TRIAL-Mikael Lustig  226\',\'DF\','1986-12-13 00:00:00',29,\'122-TRIAL-Celtic 138\'),(160444,1220,3,\'228-TRIAL-Erik Johansson  281\',\'DF\','1988-12-30 00:00:00',27,\'99-TRIAL-Kobenhavn 278\'),(160442,1220,4,\'191-TRIAL-Andreas Granqvist  123\',\'DF\','1985-04-16 00:00:00',31,\'243-TRIAL-Krasnodar 234\'),(160447,1220,5,\'143-TRIAL-Martin Olsson  149\',\'DF\','1988-05-17 00:00:00',28,\'2-TRIAL-Norwich 7\'),(160451,1220,6,\'102-TRIAL-Emil Forsberg  241\',\'MF\','1991-10-23 00:00:00',24,\'187-TRIAL-Leipzig 246\'),(160454,1220,7,\'291-TRIAL-Sebastian Larsson  37\',\'MF\','1985-06-06 00:00:00',31,\'113-TRIAL-Sunderland 0\'),(160449,1220,8,\'16-TRIAL-Albin Ekdal  190\',\'MF\','1989-07-28 00:00:00',26,\'262-TRIAL-Hamburg 235\'),(160453,1220,9,\'26-TRIAL-Kim Kallstrom  210\',\'MF\','1982-08-24 00:00:00',33,\'77-TRIAL-Grasshoppers 282\'),(160459,1220,10,\'160-TRIAL-Zlatan Ibrahimovic  189\',\'FD\','1981-10-03 00:00:00',34,\'5-TRIAL-Paris 74\'),(160457,1220,11,\'263-TRIAL-Marcus Berg  22\',\'FD\','1986-08-17 00:00:00',29,\'95-TRIAL-Panathinaikos 66\'),(160440,1220,12,\'160-TRIAL-Robin Olsen  38\',\'GK\','1990-01-08 00:00:00',26,\'288-TRIAL-Kobenhavn 11\'),(160443,1220,13,\'45-TRIAL-Pontus Jansson  167\',\'DF\','1991-02-13 00:00:00',25,\'25-TRIAL-Torino 167\'),(160445,1220,14,\'289-TRIAL-Victor Lindelof  42\',\'DF\','1994-07-17 00:00:00',21,\'163-TRIAL-Benfica 247\'),(160452,1220,15,\'202-TRIAL-Oscar Hiljemark  217\',\'MF\','1992-06-28 00:00:00',23,\'99-TRIAL-Palermo 123\'),(160456,1220,16,\'126-TRIAL-Pontus Wernbloom  3\',\'MF\','1986-06-25 00:00:00',29,\'48-TRIAL-CSKA Moskva 51\'),(160441,1220,17,\'70-TRIAL-Ludwig Augustinsson  236\',\'DF\','1994-04-21 00:00:00',22,\'158-TRIAL-Kobenhavn 167\'),(160455,1220,18,\'56-TRIAL-Oscar Lewicki  105\',\'MF\','1992-07-14 00:00:00',23,\'131-TRIAL-Malmo 262\'),(160460,1220,19,\'119-TRIAL-Emir Kujovic  175\',\'FD\','1988-06-22 00:00:00',27,\'56-TRIAL-Norrkoping 31\'),(160458,1220,20,\'295-TRIAL-John Guidetti  244\',\'FD\','1992-04-15 00:00:00',24,\'291-TRIAL-Celta 103\'),(160448,1220,21,\'88-TRIAL-Jimmy Durmaz  15\',\'MF\','1989-03-22 00:00:00',27,\'250-TRIAL-Olympiacos 19\'),(160450,1220,22,\'272-TRIAL-Erkan Zengin  291\',\'MF\','1985-08-05 00:00:00',30,\'283-TRIAL-Trabzonspor 233\'),(160438,1220,23,\'275-TRIAL-Patrik Carlgren  42\',\'GK\','1992-01-08 00:00:00',24,\'169-TRIAL-AIK 0\'),(160463,1221,1,\'254-TRIAL-Yann Sommer  78\',\'GK\','1988-12-17 00:00:00',27,\'285-TRIAL-Monchengladbach 157\'),(160467,1221,2,\'133-TRIAL-Stephan Lichtsteiner  293\',\'DF\','1984-01-16 00:00:00',32,\'290-TRIAL-Juventus 203\'),(160468,1221,3,\'250-TRIAL-Francois Moubandje  133\',\'DF\','1990-06-21 00:00:00',25,\'222-TRIAL-Toulouse 271\'),(160465,1221,4,\'111-TRIAL-Nico Elvedi  90\',\'DF\','1996-09-30 00:00:00',19,\'38-TRIAL-Monchengladbach 41\'),(160471,1221,5,\'158-TRIAL-Steve von Bergen  147\',\'DF\','1983-06-10 00:00:00',33,\'252-TRIAL-Young Boys 58\'),(160466,1221,6,\'179-TRIAL-Michael Lang  102\',\'DF\','1991-02-08 00:00:00',25,\'283-TRIAL-Basel 90\'),(160480,1221,7,\'102-TRIAL-Breel Embolo  268\',\'FD\','1997-02-14 00:00:00',19,\'222-TRIAL-Basel 95\'),(160475,1221,8,\'135-TRIAL-Fabian Frei  8\',\'MF\','1989-01-08 00:00:00',27,\'61-TRIAL-Mainz 242\'),(160482,1221,9,\'294-TRIAL-Haris Seferovic  299\',\'FD\','1992-02-22 00:00:00',24,\'88-TRIAL-Frankfurt 78\'),(160477,1221,10,\'142-TRIAL-Granit Xhaka  157\',\'MF\','1992-09-27 00:00:00',23,\'241-TRIAL-Monchengladbach 47\'),(160472,1221,11,\'269-TRIAL-Valon Behrami  145\',\'MF\','1985-04-19 00:00:00',31,\'180-TRIAL-Watford 113\'),(160462,1221,12,\'264-TRIAL-Marwin Hitz  210\',\'GK\','1987-09-18 00:00:00',28,\'261-TRIAL-Augsburg 185\'),(160469,1221,13,\'273-TRIAL-Ricardo Rodriguez  204\',\'DF\','1992-08-25 00:00:00',23,\'162-TRIAL-Wolfsburg 3\'),(160478,1221,14,\'202-TRIAL-Denis Zakaria  269\',\'MF\','1996-11-20 00:00:00',19,\'54-TRIAL-Young Boys 229\'),(160473,1221,15,\'52-TRIAL-Blerim Dzemaili  274\',\'MF\','1986-04-12 00:00:00',30,\'249-TRIAL-Genoa 130\'),(160474,1221,16,\'144-TRIAL-Gelson Fernandes  44\',\'MF\','1986-09-02 00:00:00',29,\'149-TRIAL-Rennes 18\'),(160483,1221,17,\'65-TRIAL-Shani Tarashaj  263\',\'FD\','1995-02-07 00:00:00',21,\'52-TRIAL-Grasshoppers 273\'),(160481,1221,18,\'170-TRIAL-Admir Mehmedi  31\',\'FD\','1991-03-16 00:00:00',25,\'147-TRIAL-Leverkusen 11\'),(160479,1221,19,\'169-TRIAL-Eren Derdiyok  298\',\'FD\','1988-06-12 00:00:00',28,\'298-TRIAL-Kasimpasa 203\'),(160464,1221,20,\'52-TRIAL-Johan Djourou  179\',\'DF\','1987-01-18 00:00:00',29,\'153-TRIAL-Hamburg 143\'),(160461,1221,21,\'122-TRIAL-Roman Burki  188\',\'GK\','1990-11-14 00:00:00',25,\'63-TRIAL-Dortmund 34\'),(160470,1221,22,\'250-TRIAL-Fabian Schar  222\',\'DF\','1991-12-20 00:00:00',24,\'40-TRIAL-Hoffenheim 11\'),(160476,1221,23,\'192-TRIAL-Xherdan Shaqiri  251\',\'MF\','1991-10-10 00:00:00',24,\'80-TRIAL-Stoke 177\'),(160486,1222,1,\'16-TRIAL-Volkan Babacan  176\',\'GK\','1988-08-11 00:00:00',27,\'278-TRIAL-Istanbul Basaksehir 120\'),(160492,1222,2,\'215-TRIAL-Semih Kaya  148\',\'DF\','1991-02-24 00:00:00',25,\'98-TRIAL-Galatasaray 79\'),(160490,1222,3,\'135-TRIAL-Hakan Balta  57\',\'DF\','1983-03-23 00:00:00',33,\'183-TRIAL-Galatasaray 162\'),(160487,1222,4,\'2-TRIAL-Ahmet Calik  62\',\'DF\','1994-02-26 00:00:00',22,\'220-TRIAL-Genclerbirligi 270\'),(160497,1222,5,\'122-TRIAL-Nuri Sahin  273\',\'MF\','1988-09-05 00:00:00',27,\'41-TRIAL-Dortmund 86\'),(160495,1222,6,\'88-TRIAL-Hakan Calhanoglu  17\',\'MF\','1994-02-08 00:00:00',22,\'217-TRIAL-Leverkusen 92\'),(160489,1222,7,\'198-TRIAL-Gokhan Gonul  167\',\'DF\','1985-01-04 00:00:00',31,\'49-TRIAL-Fenerbahce 65\'),(160501,1222,8,\'189-TRIAL-Selcuk Inan  32\',\'MF\','1985-02-10 00:00:00',31,\'119-TRIAL-Galatasaray 181\'),(160505,1222,9,\'203-TRIAL-Cenk Tosun  30\',\'FD\','1991-06-07 00:00:00',25,\'128-TRIAL-Besiktas 31\'),(160494,1222,10,\'189-TRIAL-Arda Turan  52\',\'MF\','1987-01-30 00:00:00',29,\'30-TRIAL-Barcelona 272\'),(160499,1222,11,\'64-TRIAL-Olcay Sahan  107\',\'MF\','1987-05-26 00:00:00',29,\'295-TRIAL-Besiktas 228\'),(160485,1222,12,\'281-TRIAL-Onur Kivrak  190\',\'GK\','1988-01-01 00:00:00',28,\'210-TRIAL-Trabzonspor 77\'),(160491,1222,13,\'171-TRIAL-Ismail Koybasi  36\',\'DF\','1989-07-10 00:00:00',26,\'159-TRIAL-Besiktas 243\'),(160498,1222,14,\'40-TRIAL-Oguzhan Ozyakup  33\',\'MF\','1992-09-23 00:00:00',23,\'37-TRIAL-Besiktas 193\'),(160496,1222,15,\'130-TRIAL-Mehmet Topal  174\',\'MF\','1986-03-03 00:00:00',30,\'93-TRIAL-Fenerbahce 282\'),(160500,1222,16,\'103-TRIAL-Ozan Tufan  61\',\'MF\','1995-03-23 00:00:00',21,\'190-TRIAL-Fenerbahce 162\'),(160504,1222,17,\'107-TRIAL-Burak Yilmaz  297\',\'FD\','1985-07-15 00:00:00',30,\'251-TRIAL-Beijing Guoan 123\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_mast" ("player_id","team_id","jersey_no","player_name","posi_to_play","dt_of_bir","age","playing_club") VALUES (160488,1222,18,\'190-TRIAL-Caner Erkin  217\',\'DF\','1988-10-04 00:00:00',27,\'40-TRIAL-Fenerbahce 203\'),(160503,1222,19,\'166-TRIAL-Yunus Malli  83\',\'MF\','1992-02-24 00:00:00',24,\'261-TRIAL-Mainz 159\'),(160502,1222,20,\'245-TRIAL-Volkan Sen  286\',\'MF\','1987-07-07 00:00:00',28,\'251-TRIAL-Fenerbahce 65\'),(160506,1222,21,\'101-TRIAL-Emre Mor  40\',\'FD\','1997-07-24 00:00:00',18,\'109-TRIAL-Nordsj�lland 297\'),(160493,1222,22,\'283-TRIAL-Sener Ozbayrakli  150\',\'DF\','1990-01-23 00:00:00',26,\'58-TRIAL-Fenerbahce 21\'),(160484,1222,23,\'175-TRIAL-Harun Tekin  281\',\'GK\','1989-06-17 00:00:00',27,\'87-TRIAL-Bursaspor 271\'),(160507,1223,1,\'87-TRIAL-Denys Boyko  99\',\'GK\','1988-01-29 00:00:00',28,\'204-TRIAL-Besiktas 79\'),(160510,1223,2,\'38-TRIAL-Bohdan Butko  200\',\'DF\','1991-01-13 00:00:00',25,\'230-TRIAL-Amkar 61\'),(160512,1223,3,\'118-TRIAL-Yevhen Khacheridi  205\',\'DF\','1987-07-28 00:00:00',28,\'40-TRIAL-Dynamo Kyiv 228\'),(160524,1223,4,\'159-TRIAL-Anatoliy Tymoshchuk  222\',\'MF\','1979-03-30 00:00:00',37,\'298-TRIAL-Kairat 160\'),(160513,1223,5,\'98-TRIAL-Olexandr Kucher  35\',\'DF\','1982-10-22 00:00:00',33,\'281-TRIAL-Shakhtar Donetsk 14\'),(160522,1223,6,\'67-TRIAL-Taras Stepanenko  193\',\'MF\','1989-08-08 00:00:00',26,\'96-TRIAL-Shakhtar Donetsk 261\'),(160525,1223,7,\'246-TRIAL-Andriy Yarmolenko  38\',\'MF\','1989-10-23 00:00:00',26,\'170-TRIAL-Dynamo Kyiv 138\'),(160529,1223,8,\'176-TRIAL-Roman Zozulya  255\',\'FD\','1989-11-17 00:00:00',26,\'17-TRIAL-Dnipro 171\'),(160519,1223,9,\'260-TRIAL-Viktor Kovalenko  52\',\'MF\','1996-02-14 00:00:00',20,\'258-TRIAL-Shakhtar Donetsk 133\'),(160518,1223,10,\'155-TRIAL-Yevhen Konoplyanka  265\',\'MF\','1989-09-29 00:00:00',26,\'126-TRIAL-Sevilla 1\'),(160528,1223,11,\'34-TRIAL-Yevhen Seleznyov  159\',\'FD\','1985-07-20 00:00:00',30,\'193-TRIAL-Shakhtar Donetsk 248\'),(160508,1223,12,\'273-TRIAL-Andriy Pyatov  192\',\'GK\','1984-06-28 00:00:00',31,\'293-TRIAL-Shakhtar Donetsk 33\'),(160515,1223,13,\'137-TRIAL-Vyacheslav Shevchuk  3\',\'DF\','1979-05-13 00:00:00',37,\'90-TRIAL-Shakhtar Donetsk 11\'),(160520,1223,14,\'194-TRIAL-Ruslan Rotan  68\',\'MF\','1981-10-29 00:00:00',34,\'227-TRIAL-Dnipro 298\'),(160527,1223,15,\'89-TRIAL-Pylyp Budkivskiy  248\',\'FD\','1992-03-10 00:00:00',24,\'279-TRIAL-Zorya 247\'),(160523,1223,16,\'49-TRIAL-Serhiy Sydorchuk  31\',\'MF\','1991-05-02 00:00:00',25,\'69-TRIAL-Dynamo Kyiv 275\'),(160511,1223,17,\'98-TRIAL-Artem Fedetskiy  249\',\'DF\','1985-04-26 00:00:00',31,\'236-TRIAL-Dnipro 299\'),(160521,1223,18,\'23-TRIAL-Serhiy Rybalka  243\',\'MF\','1990-04-01 00:00:00',26,\'231-TRIAL-Dynamo Kyiv 154\'),(160516,1223,19,\'61-TRIAL-Denys Garmash  234\',\'MF\','1990-04-19 00:00:00',26,\'85-TRIAL-Dynamo Kyiv 267\'),(160514,1223,20,\'16-TRIAL-Yaroslav Rakitskiy  293\',\'DF\','1989-08-03 00:00:00',26,\'204-TRIAL-Shakhtar Donetsk 166\'),(160526,1223,21,\'53-TRIAL-Olexandr Zinchenko  8\',\'MF\','1996-12-15 00:00:00',19,\'23-TRIAL-Ufa 84\'),(160517,1223,22,\'192-TRIAL-Olexandr Karavaev  167\',\'MF\','1992-06-02 00:00:00',24,\'148-TRIAL-Zorya 69\'),(160509,1223,23,\'130-TRIAL-Mykyta Shevchenko  211\',\'GK\','1993-01-26 00:00:00',23,\'14-TRIAL-Zorya 190\'),(160531,1224,1,\'27-TRIAL-Wayne Hennessey  43\',\'GK\','1987-01-24 00:00:00',29,\'104-TRIAL-Crystal Palace 99\'),(160536,1224,2,\'274-TRIAL-Chris Gunter  295\',\'DF\','1989-07-21 00:00:00',26,\'206-TRIAL-Reading 58\'),(160538,1224,3,\'189-TRIAL-Neil Taylor  258\',\'DF\','1989-02-07 00:00:00',27,\'209-TRIAL-Swansea 189\'),(160535,1224,4,\'47-TRIAL-Ben Davies  40\',\'DF\','1993-04-24 00:00:00',23,\'218-TRIAL-Tottenham 64\'),(160533,1224,5,\'275-TRIAL-James Chester  26\',\'DF\','1989-01-23 00:00:00',27,\'189-TRIAL-West Brom 103\'),(160539,1224,6,\'66-TRIAL-Ashley Williams  12\',\'DF\','1984-08-23 00:00:00',31,\'240-TRIAL-Swansea 270\'),(160540,1224,7,\'276-TRIAL-Joe Allen  195\',\'MF\','1990-03-14 00:00:00',26,\'141-TRIAL-Liverpool 286\'),(160542,1224,8,\'251-TRIAL-Andy King  109\',\'MF\','1988-10-29 00:00:00',27,\'87-TRIAL-Leicester 283\'),(160550,1224,9,\'85-TRIAL-Hal Robson-Kanu  290\',\'FD\','1989-05-21 00:00:00',27,\'50-TRIAL-Reading 86\'),(160544,1224,10,\'255-TRIAL-Aaron Ramsey  281\',\'MF\','1990-12-26 00:00:00',25,\'228-TRIAL-Arsenal 2\'),(160547,1224,11,\'277-TRIAL-Gareth Bale  262\',\'FD\','1989-07-16 00:00:00',26,\'24-TRIAL-Real Madrid 265\'),(160530,1224,12,\'272-TRIAL-Owain Fon Williams  232\',\'GK\','1987-03-17 00:00:00',29,\'285-TRIAL-Inverness 28\'),(160552,1224,13,\'46-TRIAL-George Williams  189\',\'FD\','1995-09-07 00:00:00',20,\'178-TRIAL-Fulham 83\'),(160541,1224,14,\'25-TRIAL-David Edwards  265\',\'MF\','1986-02-03 00:00:00',30,\'60-TRIAL-Wolves 245\'),(160537,1224,15,\'78-TRIAL-Jazz Richards  121\',\'DF\','1991-04-12 00:00:00',25,\'55-TRIAL-Fulham 120\'),(160543,1224,16,\'27-TRIAL-Joe Ledley  173\',\'MF\','1987-01-23 00:00:00',29,\'234-TRIAL-Crystal Palace 251\'),(160549,1224,17,\'175-TRIAL-David Cotterill  136\',\'FD\','1987-12-04 00:00:00',28,\'134-TRIAL-Birmingham 1\'),(160551,1224,18,\'37-TRIAL-Sam Vokes  10\',\'FD\','1989-10-21 00:00:00',26,\'274-TRIAL-Burnley 90\'),(160534,1224,19,\'256-TRIAL-James Collins  171\',\'DF\','1983-08-23 00:00:00',32,\'280-TRIAL-West Ham 135\'),(160546,1224,20,\'45-TRIAL-Jonathan Williams  182\',\'MF\','1993-10-09 00:00:00',22,\'25-TRIAL-Crystal Palace 118\'),(160532,1224,21,\'73-TRIAL-Danny Ward  212\',\'GK\','1993-06-22 00:00:00',22,\'137-TRIAL-Liverpool 198\'),(160545,1224,22,\'5-TRIAL-David Vaughan  262\',\'MF\','1983-02-18 00:00:00',33,\'217-TRIAL-Nottm Forest 292\'),(160548,1224,23,\'39-TRIAL-Simon Church  147\',\'FD\','1988-12-10 00:00:00',27,\'146-TRIAL-MK Dons 203\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `referee_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "referee_mast" ("referee_id","referee_name","country_id") VALUES (70001,\'Damir Skomina\',1225),(70002,\'Martin Atkinson\',1206),(70003,\'Felix Brych\',1208),(70004,\'Cuneyt Cakir\',1222),(70005,\'Mark Clattenburg\',1206),(70006,\'Jonas Eriksson\',1220),(70007,\'Viktor Kassai\',1209),(70008,\'Bjorn Kuipers\',1226),(70009,\'Szymon Marciniak\',1213),(70010,\'Milorad Mazic\',1227),(70011,\'Nicola Rizzoli\',1211),(70012,\'Carlos Velasco Carballo\',1219),(70013,\'William Collum\',1228),(70014,\'Ovidiu Hategan\',1216),(70015,\'Sergei Karasev\',1217),(70016,\'Pavel Kralovec\',1205),(70017,\'Svein Oddvar Moen\',1229),(70018,\'Clement Turpin\',1207);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `soccer_city`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "soccer_city" ("city_id","city","country_id") VALUES (10001,\'Paris\',1207),(10002,\'Saint-Denis\',1207),(10003,\'Bordeaux\',1207),(10004,\'Lens\',1207),(10005,\'Lille\',1207),(10006,\'Lyon\',1207),(10007,\'Marseille\',1207),(10008,\'Nice\',1207),(10009,\'Saint-Etienne\',1207),(10010,\'Toulouse\',1207);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `movie`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "movie" ("mov_id","mov_title","mov_year","mov_time","mov_lang","mov_dt_rel","mov_rel_country") VALUES (901,\'Vertigo                                           \',1958,128,\'English        \','1958-08-24 00:00:00',\'UK   \'),(902,\'The Innocents                                     \',1961,100,\'English        \','1962-02-19 00:00:00',\'SW   \'),(903,\'Lawrence of Arabia                                \',1962,216,\'English        \','1962-12-11 00:00:00',\'UK   \'),(904,\'The Deer Hunter                                   \',1978,183,\'English        \','1979-03-08 00:00:00',\'UK   \'),(905,\'Amadeus                                           \',1984,160,\'English        \','1985-01-07 00:00:00',\'UK   \'),(906,\'Blade Runner                                      \',1982,117,\'English        \','1982-09-09 00:00:00',\'UK   \'),(907,\'Eyes Wide Shut                                    \',1999,159,\'English        \',NULL,\'UK   \'),(908,\'The Usual Suspects                                \',1995,106,\'English        \','1995-08-25 00:00:00',\'UK   \'),(909,\'Chinatown                                         \',1974,130,\'English        \','1974-08-09 00:00:00',\'UK   \'),(910,\'Boogie Nights                                     \',1997,155,\'English        \','1998-02-16 00:00:00',\'UK   \'),(911,\'Annie Hall                                        \',1977,93,\'English        \','1977-04-20 00:00:00',\'USA  \'),(912,\'Princess Mononoke                                 \',1997,134,\'Japanese       \','2001-10-19 00:00:00',\'UK   \'),(913,\'The Shawshank Redemption                          \',1994,142,\'English        \','1995-02-17 00:00:00',\'UK   \'),(914,\'American Beauty                                   \',1999,122,\'English        \',NULL,\'UK   \'),(915,\'Titanic                                           \',1997,194,\'English        \','1998-01-23 00:00:00',\'UK   \'),(916,\'Good Will Hunting                                 \',1997,126,\'English        \','1998-06-03 00:00:00',\'UK   \'),(917,\'Deliverance                                       \',1972,109,\'English        \','1982-10-05 00:00:00',\'UK   \'),(918,\'Trainspotting                                     \',1996,94,\'English        \','1996-02-23 00:00:00',\'UK   \'),(919,\'The Prestige                                      \',2006,130,\'English        \','2006-11-10 00:00:00',\'UK   \'),(920,\'Donnie Darko                                      \',2001,113,\'English        \',NULL,\'UK   \'),(921,\'Slumdog Millionaire                               \',2008,120,\'English        \','2009-01-09 00:00:00',\'UK   \'),(922,\'Aliens                                            \',1986,137,\'English        \','1986-08-29 00:00:00',\'UK   \'),(923,\'Beyond the Sea                                    \',2004,118,\'English        \','2004-11-26 00:00:00',\'UK   \'),(924,\'Avatar                                            \',2009,162,\'English        \','2009-12-17 00:00:00',\'UK   \'),(926,\'Seven Samurai                                     \',1954,207,\'Japanese       \','1954-04-26 00:00:00',\'JP   \'),(927,\'Spirited Away                                     \',2001,125,\'Japanese       \','2003-09-12 00:00:00',\'UK   \'),(928,\'Back to the Future                                \',1985,116,\'English        \','1985-12-04 00:00:00',\'UK   \'),(925,\'Braveheart                                        \',1995,178,\'English        \','1995-09-08 00:00:00',\'UK   \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `movie_cast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "movie_cast" ("act_id","mov_id","role") VALUES (101,901,\'John Scottie Ferguson         \'),(102,902,\'Miss Giddens                  \'),(103,903,\'T.E. Lawrence                 \'),(104,904,\'Michael                       \'),(105,905,\'Antonio Salieri               \'),(106,906,\'Rick Deckard                  \'),(107,907,\'Alice Harford                 \'),(108,908,\'McManus                       \'),(110,910,\'Eddie Adams                   \'),(111,911,\'Alvy Singer                   \'),(112,912,\'San                           \'),(113,913,\'Andy Dufresne                 \'),(114,914,\'Lester Burnham                \'),(115,915,\'Rose DeWitt Bukater           \'),(116,916,\'Sean Maguire                  \'),(117,917,\'Ed                            \'),(118,918,\'Renton                        \'),(120,920,\'Elizabeth Darko               \'),(121,921,\'Older Jamal                   \'),(122,922,\'Ripley                        \'),(114,923,\'Bobby Darin                   \'),(109,909,\'J.J. Gittes                   \'),(119,919,\'Alfred Borden                 \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `movie_direction`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "movie_direction" ("dir_id","mov_id") VALUES (201,901),(202,902),(203,903),(204,904),(205,905),(206,906),(207,907),(208,908),(209,909),(210,910),(211,911),(212,912),(213,913),(214,914),(215,915),(216,916),(217,917),(218,918),(219,919),(220,920),(218,921),(215,922),(221,923);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `movie_genres`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "movie_genres" ("mov_id","gen_id") VALUES (922,1001),(917,1002),(903,1002),(912,1003),(911,1005),(908,1006),(913,1006),(926,1007),(928,1007),(918,1007),(921,1007),(902,1008),(923,1009),(907,1010),(927,1010),(901,1010),(914,1011),(906,1012),(904,1013);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `new`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "new" ("city") VALUES (\'New York\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `new123`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "new123" ("Customer","city","Salesman","commission") VALUES (\'Nick Rimando\',\'New York\',\'James Hoog\',0.15),(\'Graham Zusi\',\'California\',\'Nail Knite\',0.13),(\'Brad Guzan\',\'London\',\'Pit Alex\',0.11),(\'Fabian Johns\',\'Paris\',\'Mc Lyon\',0.14),(\'Brad Davis\',\'New York\',\'James Hoog\',0.15),(\'Geoff Camero\',\'Berlin\',\'Lauson Hen\',0.12),(\'Julian Green\',\'London\',\'Nail Knite\',0.13),(\'Jozy Altidor\',\'Moncow\',\'Paul Adam\',0.13);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `new_table`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "new_table" ("salesman_id","name","city","commission") VALUES (5002,\'Nail Knite\',\'Paris\',0.13),(5005,\'Pit Alex\',\'London\',0.11),(5006,\'Mc Lyon\',\'Paris\',0.14),(5003,\'Lauson Hense\',NULL,0.12),(5007,\'Paul Adam\',\'Rome\',0.13);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `newtab`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "newtab" ("ord_no","purch_amt","ord_date","customer_id","salesman_id") VALUES (70009,270.65,'2012-09-10 00:00:00',3001,5005),(70002,65.26,'2012-10-05 00:00:00',3002,5001),(70004,110.50,'2012-08-17 00:00:00',3009,5003),(70005,2400.60,'2012-07-27 00:00:00',3007,5001),(70008,5760.00,'2012-09-10 00:00:00',3002,5001),(70010,1983.43,'2012-10-10 00:00:00',3004,5006),(70003,2480.40,'2012-10-10 00:00:00',3009,5003),(70011,75.29,'2012-08-17 00:00:00',3003,5007),(70013,3045.60,'2012-04-25 00:00:00',3002,5001),(70001,150.50,'2012-10-05 00:00:00',3005,5002),(70007,948.50,'2012-09-10 00:00:00',3005,5002),(70012,250.45,'2012-06-27 00:00:00',3008,5002);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `nobel_win`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "nobel_win" ("year","subject","winner","country","category") VALUES (1970,\'Physics                  \',\'Hannes Alfven                                \',\'Sweden                   \',\'Scientist                \'),(1970,\'Physics                  \',\'Louis Neel                                   \',\'France                   \',\'Scientist                \'),(1970,\'Chemistry                \',\'Luis Federico Leloir                         \',\'France                   \',\'Scientist                \'),(1970,\'Physiology               \',\'Julius Axelrod                               \',\'USA                      \',\'Scientist                \'),(1970,\'Physiology               \',\'Ulf von Euler                                \',\'Sweden                   \',\'Scientist                \'),(1970,\'Physiology               \',\'Bernard Katz                                 \',\'Germany                  \',\'Scientist                \'),(1970,\'Literature               \',\'Aleksandr Solzhenitsyn                       \',\'Russia                   \',\'Linguist                 \'),(1970,\'Economics                \',\'Paul Samuelson                               \',\'USA                      \',\'Economist                \'),(1971,\'Physics                  \',\'Dennis Gabor                                 \',\'Hungary                  \',\'Scientist                \'),(1971,\'Chemistry                \',\'Gerhard Herzberg                             \',\'Germany                  \',\'Scientist                \'),(1971,\'Peace                    \',\'Willy Brandt                                 \',\'Germany                  \',\'Chancellor               \'),(1971,\'Literature               \',\'Pablo Neruda                                 \',\'Chile                    \',\'Linguist                 \'),(1971,\'Economics                \',\'Simon Kuznets                                \',\'Russia                   \',\'Economist                \'),(1978,\'Peace                    \',\'Anwar al-Sadat                               \',\'Egypt                    \',\'President                \'),(1978,\'Peace                    \',\'Menachem Begin                               \',\'Israel                   \',\'Prime Minister           \'),(1994,\'Peace                    \',\'Yitzhak Rabin                                \',\'Israel                   \',\'Prime Minister           \'),(1987,\'Physics                  \',\'Johannes Georg Bednorz                       \',\'Germany                  \',\'Scientist                \'),(1987,\'Chemistry                \',\'Donald J. Cram                               \',\'USA                      \',\'Scientist                \'),(1987,\'Chemistry                \',\'Jean-Marie Lehn                              \',\'France                   \',\'Scientist                \'),(1987,\'Physiology               \',\'Susumu Tonegawa                              \',\'Japan                    \',\'Scientist                \'),(1987,\'Literature               \',\'Joseph Brodsky                               \',\'Russia                   \',\'Linguist                 \'),(1987,\'Economics                \',\'Robert Solow                                 \',\'USA                      \',\'Economist                \'),(1994,\'Literature               \',\'Kenzaburo Oe                                 \',\'Japan                    \',\'Linguist                 \'),(1994,\'Economics                \',\'Reinhard Selten                              \',\'Germany                  \',\'Economist                \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `nuevo`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "nuevo" ("city") VALUES (\'New York\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `customer`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "customer" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3007,\'Brad Davis\',\'New York\',200,5001),(3005,\'Graham Zusi\',\'California\',200,5002),(3008,\'Julian Green\',\'London\',300,5002),(3004,\'Fabian Johnson\',\'Paris\',300,5006),(3009,\'Geoff Cameron\',\'Berlin\',100,5003),(3003,\'Jozy Altidor\',\'Moscow\',200,5007),(3001,\'Brad Guzan\',\'London\',NULL,5005);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `orozco`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "orozco" ("city") VALUES (\'New York\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `soccer_venue`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "soccer_venue" ("venue_id","venue_name","city_id","aud_capacity") VALUES (20001,\'Stade de Bordeaux\',10003,42115),(20002,\'Stade Bollaert-Delelis\',10004,38223),(20003,\'Stade Pierre Mauroy\',10005,49822),(20004,\'Stade de Lyon\',10006,58585),(20005,\'Stade VElodrome\',10007,64354),(20006,\'Stade de Nice\',10008,35624),(20007,\'Parc des Princes\',10001,47294),(20008,\'Stade de France\',10002,80100),(20009,\'Stade Geoffroy Guichard\',10009,42000),(20010,\'Stadium de Toulouse\',10010,33150);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `match_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "match_mast" ("match_no","play_stage","play_date","results","decided_by","goal_score","venue_id","referee_id","audence","plr_of_match","stop1_sec","stop2_sec") VALUES (1,\'G\','2016-06-11 00:00:00',\'WIN  \',\'N\',\'2-1  \',20008,70007,75113,160154,131,242),(2,\'G\','2016-06-11 00:00:00',\'WIN  \',\'N\',\'0-1  \',20002,70012,33805,160476,61,182),(3,\'G\','2016-06-11 00:00:00',\'WIN  \',\'N\',\'2-1  \',20001,70017,37831,160540,64,268),(4,\'G\','2016-06-12 00:00:00',\'DRAW \',\'N\',\'1-1  \',20005,70011,62343,160128,0,185),(5,\'G\','2016-06-12 00:00:00',\'WIN  \',\'N\',\'0-1  \',20007,70006,43842,160084,125,325),(6,\'G\','2016-06-12 00:00:00',\'WIN  \',\'N\',\'1-0  \',20006,70014,33742,160291,2,246),(7,\'G\','2016-06-13 00:00:00',\'WIN  \',\'N\',\'2-0  \',20003,70002,43035,160176,89,188),(8,\'G\','2016-06-13 00:00:00',\'WIN  \',\'N\',\'1-0  \',20010,70009,29400,160429,360,182),(9,\'G\','2016-06-13 00:00:00',\'DRAW \',\'N\',\'1-1  \',20008,70010,73419,160335,67,194),(10,\'G\','2016-06-14 00:00:00',\'WIN  \',\'N\',\'0-2  \',20004,70005,55408,160244,63,189),(11,\'G\','2016-06-14 00:00:00',\'WIN  \',\'N\',\'0-2  \',20001,70018,34424,160197,61,305),(12,\'G\','2016-06-15 00:00:00',\'DRAW \',\'N\',\'1-1  \',20009,70004,38742,160320,15,284),(13,\'G\','2016-06-15 00:00:00',\'WIN  \',\'N\',\'1-2  \',20003,70001,38989,160405,62,189),(14,\'G\','2016-06-15 00:00:00',\'DRAW \',\'N\',\'1-1  \',20007,70015,43576,160477,74,206),(15,\'G\','2016-06-16 00:00:00',\'WIN  \',\'N\',\'2-0  \',20005,70013,63670,160154,71,374),(16,\'G\','2016-06-16 00:00:00',\'WIN  \',\'N\',\'2-1  \',20002,70003,34033,160540,62,212),(17,\'G\','2016-06-16 00:00:00',\'WIN  \',\'N\',\'0-2  \',20004,70016,51043,160262,7,411),(18,\'G\','2016-06-17 00:00:00',\'DRAW \',\'N\',\'0-0  \',20008,70008,73648,160165,6,208),(19,\'G\','2016-06-17 00:00:00',\'WIN  \',\'N\',\'1-0  \',20010,70007,29600,160248,2,264),(20,\'G\','2016-06-17 00:00:00',\'DRAW \',\'N\',\'2-2  \',20009,70005,38376,160086,71,280),(21,\'G\','2016-06-18 00:00:00',\'WIN  \',\'N\',\'3-0  \',20006,70010,33409,160429,84,120),(22,\'G\','2016-06-18 00:00:00',\'WIN  \',\'N\',\'3-0  \',20001,70004,39493,160064,11,180),(23,\'G\','2016-06-18 00:00:00',\'DRAW \',\'N\',\'1-1  \',20005,70015,60842,160230,61,280),(24,\'G\','2016-06-19 00:00:00',\'DRAW \',\'N\',\'0-0  \',20007,70011,44291,160314,3,200),(25,\'G\','2016-06-20 00:00:00',\'WIN  \',\'N\',\'0-1  \',20004,70016,49752,160005,125,328),(26,\'G\','2016-06-20 00:00:00',\'DRAW \',\'N\',\'0-0  \',20003,70001,45616,160463,60,122),(27,\'G\','2016-06-21 00:00:00',\'WIN  \',\'N\',\'0-3  \',20010,70006,28840,160544,62,119),(28,\'G\','2016-06-21 00:00:00',\'DRAW \',\'N\',\'0-0  \',20009,70012,39051,160392,62,301),(29,\'G\','2016-06-21 00:00:00',\'WIN  \',\'N\',\'0-1  \',20005,70017,58874,160520,29,244),(30,\'G\','2016-06-21 00:00:00',\'WIN  \',\'N\',\'0-1  \',20007,70018,44125,160177,21,195),(31,\'G\','2016-06-22 00:00:00',\'WIN  \',\'N\',\'0-2  \',20002,70013,32836,160504,60,300),(32,\'G\','2016-06-22 00:00:00',\'WIN  \',\'N\',\'2-1  \',20001,70008,37245,160085,70,282),(33,\'G\','2016-06-22 00:00:00',\'WIN  \',\'N\',\'2-1  \',20008,70009,68714,160220,7,244),(34,\'G\','2016-06-22 00:00:00',\'DRAW \',\'N\',\'3-3  \',20004,70002,55514,160322,70,185),(35,\'G\','2016-06-23 00:00:00',\'WIN  \',\'N\',\'0-1  \',20003,70014,44268,160333,79,221),(36,\'G\','2016-06-23 00:00:00',\'WIN  \',\'N\',\'0-1  \',20006,70003,34011,160062,63,195),(37,\'R\','2016-06-25 00:00:00',\'WIN  \',\'P\',\'1-1  \',20009,70005,38842,160476,126,243),(38,\'R\','2016-06-25 00:00:00',\'WIN  \',\'N\',\'1-0  \',20007,70002,44342,160547,5,245),(39,\'R\','2016-06-26 00:00:00',\'WIN  \',\'N\',\'0-1  \',20002,70012,33523,160316,61,198),(40,\'R\','2016-06-26 00:00:00',\'WIN  \',\'N\',\'2-1  \',20004,70011,56279,160160,238,203),(41,\'R\','2016-06-26 00:00:00',\'WIN  \',\'N\',\'3-0  \',20003,70009,44312,160173,62,124),(42,\'R\','2016-06-27 00:00:00',\'WIN  \',\'N\',\'0-4  \',20010,70010,28921,160062,3,133),(43,\'R\','2016-06-27 00:00:00',\'WIN  \',\'N\',\'2-0  \',20008,70004,76165,160235,63,243),(44,\'R\','2016-06-28 00:00:00',\'WIN  \',\'N\',\'1-2  \',20006,70001,33901,160217,5,199),(45,\'Q\','2016-07-01 00:00:00',\'WIN  \',\'P\',\'1-1  \',20005,70003,62940,160316,58,181),(46,\'Q\','2016-07-02 00:00:00',\'WIN  \',\'N\',\'3-1  \',20003,70001,45936,160550,14,182),(47,\'Q\','2016-07-03 00:00:00',\'WIN  \',\'P\',\'1-1  \',20001,70007,38764,160163,63,181),(48,\'Q\','2016-07-04 00:00:00',\'WIN  \',\'N\',\'5-2  \',20008,70008,76833,160159,16,125),(49,\'S\','2016-07-07 00:00:00',\'WIN  \',\'N\',\'2-0  \',20004,70006,55679,160322,2,181),(50,\'S\','2016-07-08 00:00:00',\'WIN  \',\'N\',\'2-0  \',20005,70011,64078,160160,126,275),(51,\'F\','2016-07-11 00:00:00',\'WIN  \',\'N\',\'1-0  \',20008,70005,75868,160307,161,181);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `player_in_out`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_in_out" ("match_no","team_id","player_id","in_out","time_in_out","play_schedule","play_half") VALUES (1,1207,160151,\'I\',66,\'NT\',2),(1,1207,160160,\'O\',66,\'NT\',2),(1,1207,160161,\'I\',77,\'NT\',2),(1,1207,160161,\'O\',77,\'NT\',2),(1,1207,160157,\'I\',2,\'ST\',2),(1,1207,160154,\'O\',2,\'ST\',2),(1,1216,160365,\'I\',61,\'NT\',2),(1,1216,160366,\'O\',61,\'NT\',2),(1,1216,160357,\'I\',72,\'NT\',2),(1,1216,160363,\'O\',72,\'NT\',2),(1,1216,160364,\'I\',82,\'NT\',2),(1,1216,160360,\'O\',82,\'NT\',2),(2,1201,160014,\'I\',62,\'NT\',2),(2,1201,160019,\'O\',62,\'NT\',2),(2,1201,160021,\'I\',74,\'NT\',2),(2,1201,160018,\'O\',74,\'NT\',2),(2,1201,160022,\'I\',82,\'NT\',2),(2,1201,160023,\'O\',82,\'NT\',2),(2,1221,160480,\'I\',62,\'NT\',2),(2,1221,160481,\'O\',62,\'NT\',2),(2,1221,160475,\'I\',76,\'NT\',2),(2,1221,160473,\'O\',76,\'NT\',2),(2,1221,160474,\'I\',88,\'NT\',2),(2,1221,160476,\'O\',88,\'NT\',2),(3,1218,160413,\'I\',59,\'NT\',2),(3,1218,160412,\'O\',59,\'NT\',2),(3,1218,160403,\'I\',60,\'NT\',2),(3,1218,160406,\'O\',60,\'NT\',2),(3,1218,160410,\'I\',83,\'NT\',2),(3,1218,160411,\'O\',83,\'NT\',2),(3,1224,160543,\'I\',69,\'NT\',2),(3,1224,160541,\'O\',69,\'NT\',2),(3,1224,160550,\'I\',71,\'NT\',2),(3,1224,160546,\'O\',71,\'NT\',2),(3,1224,160537,\'I\',88,\'NT\',2),(3,1224,160544,\'O\',88,\'NT\',2),(4,1206,160133,\'I\',78,\'NT\',2),(4,1206,160136,\'O\',78,\'NT\',2),(4,1206,160131,\'I\',87,\'NT\',2),(4,1206,160132,\'O\',87,\'NT\',2),(4,1217,160386,\'I\',77,\'NT\',2),(4,1217,160381,\'O\',77,\'NT\',2),(4,1217,160380,\'I\',80,\'NT\',2),(4,1217,160376,\'O\',80,\'NT\',2),(4,1217,160383,\'I\',85,\'NT\',2),(4,1217,160391,\'O\',85,\'NT\',2),(5,1204,160090,\'I\',87,\'NT\',2),(5,1204,160085,\'O\',87,\'NT\',2),(5,1204,160075,\'I\',90,\'NT\',2),(5,1204,160086,\'O\',90,\'NT\',2),(5,1204,160092,\'I\',3,\'ST\',2),(5,1204,160091,\'2\',3,\'23\',2),(5,1222,160502,\'1\',46,\'94\',2),(5,1222,160498,\'1\',46,\'75\',2),(5,1222,160504,\'2\',65,\'80\',2),(5,1222,160494,\'1\',65,\'20\',2),(5,1222,160506,\'1\',69,\'34\',2),(5,1222,160505,\'7\',69,\'69\',2),(6,1212,160266,\'1\',46,\'13\',2),(6,1212,160265,\'2\',46,\'17\',2),(6,1212,160276,\'1\',66,\'10\',2),(6,1212,160269,\'6\',66,\'14\',2),(6,1212,160272,\'8\',76,\'13\',2),(6,1212,160257,\'1\',76,\'24\',2),(6,1213,160289,\'2\',78,\'43\',2),(6,1213,160293,\'1\',78,\'15\',2),(6,1213,160288,\'1\',80,\'12\',2),(6,1213,160287,\'2\',80,\'24\',2),(6,1213,160294,\'2\',88,\'25\',2),(6,1213,160290,\'1\',88,\'29\',2),(7,1208,160179,\'1\',78,\'12\',2),(7,1208,160173,\'2\',78,\'27\',2),(7,1208,160180,\'8\',90,\'17\',2),(7,1208,160174,\'2\',90,\'20\',2),(7,1223,160528,\'2\',66,\'0-\',2),(7,1223,160529,\'9\',66,\'11\',2),(7,1223,160526,\'6\',73,\'17\',2),(7,1223,160519,\'1\',73,\'23\',2),(8,1205,160113,\'2\',75,\'13\',2),(8,1205,160114,\'1\',75,\'18\',2),(8,1205,160112,\'2\',86,\'97\',2),(8,1205,160096,\'2\',86,\'10\',2),(8,1205,160107,\'1\',88,\'66\',2),(8,1205,160110,\'3\',88,\'19\',2),(8,1219,160433,\'1\',62,\'13\',2),(8,1219,160435,\'8\',62,\'17\',2),(8,1219,160432,\'2\',70,\'17\',2),(8,1219,160428,\'2\',70,\'18\',2),(8,1219,160437,\'2\',82,\'10\',2),(8,1219,160436,\'1\',82,\'15\',2),(9,1215,160337,\'1\',64,\'26\',2),(9,1215,160345,\'2\',64,\'28\',2),(9,1215,160342,\'2\',78,\'26\',2),(9,1215,160335,\'1\',78,\'28\',2),(9,1215,160338,\'1\',85,\'23\',2),(9,1215,160336,\'5\',85,\'11\',2),(9,1220,160444,\'2\',45,\'13\',1),(9,1220,160446,\'2\',45,\'20\',1),(9,1220,160458,\'1\',59,\'89\',2),(9,1220,160457,\'1\',59,\'21\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_in_out" ("match_no","team_id","player_id","in_out","time_in_out","play_schedule","play_half") VALUES (9,1220,160449,\'1\',86,\'19\',2),(9,1220,160455,\'2\',86,\'43\',2),(10,1203,160068,\'2\',62,\'27\',2),(10,1203,160063,\'2\',62,\'69\',2),(10,1203,160069,\'2\',73,\'29\',2),(10,1203,160067,\'2\',73,\'70\',2),(10,1203,160058,\'2\',76,\'29\',2),(10,1203,160051,\'2\',76,\'16\',2),(10,1211,160238,\'1\',58,\'16\',2),(10,1211,160237,\'7\',58,\'24\',2),(10,1211,160250,\'1\',75,\'18\',2),(10,1211,160248,\'1\',75,\'23\',2),(10,1211,160245,\'2\',78,\'12\',2),(10,1211,160242,\'6\',78,\'10\',2),(11,1202,160041,\'5\',59,\'18\',2),(11,1202,160040,\'4\',59,\'14\',2),(11,1202,160046,\'1\',65,\'15\',2),(11,1202,160045,\'1\',65,\'21\',2),(11,1202,160042,\'1\',77,\'27\',2),(11,1202,160037,\'1\',77,\'15\',2),(11,1209,160206,\'2\',69,\'12\',2),(11,1209,160207,\'2\',69,\'16\',2),(11,1209,160200,\'2\',79,\'11\',2),(11,1209,160197,\'1\',79,\'12\',2),(11,1209,160195,\'1\',89,\'71\',2),(11,1209,160204,\'2\',89,\'26\',2),(12,1210,160227,\'6\',81,\'10\',2),(12,1210,160230,\'1\',81,\'27\',2),(12,1210,160211,\'1\',90,\'18\',2),(12,1210,160229,\'2\',90,\'34\',2),(12,1214,160316,\'1\',71,\'10\',2),(12,1214,160314,\'1\',71,\'15\',2),(12,1214,160321,\'2\',76,\'17\',2),(12,1214,160313,\'3\',76,\'11\',2),(12,1214,160319,\'9\',84,\'23\',2),(12,1214,160311,\'1\',84,\'26\',2),(13,1217,160380,\'2\',46,\'19\',2),(13,1217,160383,\'7\',46,\'21\',2),(13,1217,160376,\'9\',46,\'23\',2),(13,1217,160381,\'1\',46,\'9-\',2),(13,1217,160386,\'2\',75,\'16\',2),(13,1217,160390,\'8\',75,\'28\',2),(13,1218,160413,\'1\',67,\'18\',2),(13,1218,160403,\'2\',67,\'87\',2),(13,1218,160402,\'1\',72,\'64\',2),(13,1218,160411,\'7\',72,\'15\',2),(13,1218,160412,\'1\',80,\'20\',2),(13,1218,160408,\'2\',80,\'24\',2),(14,1216,160358,\'1\',46,\'29\',2),(14,1216,160359,\'1\',46,\'4-\',2),(14,1216,160350,\'1\',62,\'28\',2),(14,1216,160355,\'1\',62,\'25\',2),(14,1216,160366,\'2\',84,\'17\',2),(14,1216,160368,\'0\',84,\'53\',2),(14,1221,160480,\'1\',63,\'20\',2),(14,1221,160482,\'1\',63,\'60\',2),(14,1221,160466,\'5\',83,\'14\',2),(14,1221,160473,\'2\',83,\'59\',2),(14,1221,160483,\'2\',90,\'16\',2),(14,1221,160476,\'2\',90,\'12\',2),(15,1201,160018,\'6\',71,\'28\',2),(15,1201,160008,\'1\',71,\'27\',2),(15,1201,160019,\'8\',74,\'69\',2),(15,1201,160015,\'1\',74,\'66\',2),(15,1201,160010,\'9\',85,\'21\',2),(15,1201,160005,\'2\',85,\'25\',2),(15,1207,160155,\'1\',46,\'26\',2),(15,1207,160161,\'1\',46,\'37\',2),(15,1207,160160,\'1\',68,\'25\',2),(15,1207,160151,\'2\',68,\'29\',2),(15,1207,160158,\'2\',77,\'96\',2),(15,1207,160159,\'2\',77,\'10\',2),(16,1206,160138,\'2\',46,\'82\',2),(16,1206,160137,\'1\',46,\'29\',2),(16,1206,160134,\'1\',46,\'10\',2),(16,1206,160132,\'5\',46,\'11\',2),(16,1206,160135,\'9\',73,\'10\',2),(16,1206,160130,\'6\',73,\'24\',2),(16,1224,160541,\'1\',67,\'22\',2),(16,1224,160543,\'1\',67,\'22\',2),(16,1224,160546,\'4\',72,\'12\',2),(16,1224,160550,\'1\',72,\'24\',2),(17,1212,160275,\'2\',69,\'17\',2),(17,1212,160272,\'1\',69,\'13\',2),(17,1212,160270,\'2\',84,\'22\',2),(17,1212,160276,\'1\',84,\'11\',2),(17,1212,160265,\'1\',87,\'29\',2),(17,1212,160268,\'8\',87,\'24\',2),(17,1223,160529,\'5\',71,\'27\',2),(17,1223,160528,\'7\',71,\'14\',2),(17,1223,160516,\'4\',76,\'34\',2),(17,1223,160523,\'3\',76,\'19\',2),(17,1223,160526,\'6\',83,\'13\',2),(17,1223,160519,\'1\',83,\'14\',2),(18,1208,160179,\'7\',66,\'18\',2),(18,1208,160174,\'9\',66,\'23\',2),(18,1208,160182,\'2\',71,\'10\',2),(18,1208,160173,\'2\',71,\'38\',2),(18,1213,160289,\'1\',76,\'14\',2),(18,1213,160293,\'1\',76,\'59\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_in_out" ("match_no","team_id","player_id","in_out","time_in_out","play_schedule","play_half") VALUES (18,1213,160290,\'2\',80,\'18\',2),(18,1213,160287,\'2\',80,\'26\',2),(18,1213,160294,\'1\',87,\'23\',2),(18,1213,160288,\'1\',87,\'22\',2),(19,1211,160253,\'4\',60,\'53\',2),(19,1211,160252,\'9\',60,\'8-\',2),(19,1211,160245,\'1\',74,\'16\',2),(19,1211,160242,\'2\',74,\'22\',2),(19,1211,160247,\'1\',85,\'23\',2),(19,1211,160243,\'1\',85,\'5-\',2),(19,1220,160455,\'7\',79,\'24\',2),(19,1220,160448,\'8\',79,\'16\',2),(19,1220,160449,\'1\',79,\'48\',2),(19,1220,160451,\'3\',79,\'21\',2),(19,1220,160457,\'2\',85,\'6-\',2),(19,1220,160458,\'1\',85,\'12\',2),(20,1204,160083,\'2\',62,\'33\',2),(20,1204,160084,\'7\',62,\'14\',2),(20,1204,160075,\'7\',90,\'11\',2),(20,1204,160086,\'2\',90,\'13\',2),(20,1204,160079,\'2\',2,\'91\',2),(20,1204,160077,\'1\',2,\'19\',2),(20,1205,160115,\'1\',67,\'13\',2),(20,1205,160112,\'1\',67,\'16\',2),(20,1205,160113,\'1\',67,\'28\',2),(20,1205,160111,\'1\',67,\'50\',2),(20,1205,160114,\'2\',86,\'27\',2),(20,1205,160108,\'1\',86,\'21\',2),(21,1219,160426,\'1\',64,\'16\',2),(21,1219,160431,\'4\',64,\'16\',2),(21,1219,160430,\'3\',71,\'12\',2),(21,1219,160428,\'1\',71,\'18\',2),(21,1219,160418,\'9\',81,\'86\',2),(21,1219,160421,\'4\',81,\'22\',2),(21,1222,160497,\'8\',46,\'25\',2),(21,1222,160495,\'7\',46,\'27\',2),(21,1222,160499,\'1\',62,\'96\',2),(21,1222,160498,\'1\',62,\'14\',2),(21,1222,160503,\'1\',70,\'24\',2),(21,1222,160501,\'2\',70,\'29\',2),(22,1203,160063,\'2\',57,\'14\',2),(22,1203,160060,\'1\',57,\'23\',2),(22,1203,160068,\'2\',64,\'22\',2),(22,1203,160058,\'9\',64,\'21\',2),(22,1203,160066,\'4\',83,\'13\',2),(22,1203,160067,\'2\',83,\'17\',2),(22,1215,160337,\'1\',62,\'44\',2),(22,1215,160336,\'2\',62,\'36\',2),(22,1215,160338,\'4\',71,\'16\',2),(22,1215,160335,\'3\',71,\'18\',2),(22,1215,160342,\'1\',79,\'28\',2),(22,1215,160343,\'1\',79,\'19\',2),(23,1209,160201,\'1\',66,\'29\',2),(23,1209,160205,\'2\',66,\'26\',2),(23,1209,160206,\'4\',66,\'1-\',2),(23,1209,160200,\'4\',66,\'26\',2),(23,1209,160207,\'2\',84,\'16\',2),(23,1209,160191,\'2\',84,\'25\',2),(23,1210,160223,\'1\',65,\'18\',2),(23,1210,160222,\'2\',65,\'29\',2),(23,1210,160227,\'2\',69,\'37\',2),(23,1210,160226,\'2\',69,\'19\',2),(23,1210,160228,\'1\',84,\'11\',2),(23,1210,160230,\'2\',84,\'69\',2),(24,1202,160042,\'1\',65,\'12\',2),(24,1202,160034,\'2\',65,\'19\',2),(24,1202,160044,\'1\',85,\'29\',2),(24,1202,160041,\'1\',85,\'18\',2),(24,1202,160033,\'9\',87,\'93\',2),(24,1202,160038,\'1\',87,\'19\',2),(24,1214,160313,\'9\',71,\'49\',2),(24,1214,160321,\'1\',71,\'69\',2),(24,1214,160319,\'1\',83,\'23\',2),(24,1214,160311,\'5\',83,\'20\',2),(24,1214,160315,\'2\',89,\'23\',2),(24,1214,160320,\'2\',89,\'68\',2),(25,1201,160020,\'2\',59,\'47\',2),(25,1201,160023,\'2\',59,\'23\',2),(25,1201,160018,\'1\',77,\'56\',2),(25,1201,160016,\'5\',77,\'21\',2),(25,1201,160013,\'2\',83,\'91\',2),(25,1201,160012,\'1\',83,\'25\',2),(25,1216,160362,\'2\',46,\'24\',2),(25,1216,160361,\'1\',46,\'10\',2),(25,1216,160364,\'2\',57,\'22\',2),(25,1216,160365,\'1\',57,\'22\',2),(25,1216,160366,\'2\',68,\'26\',2),(25,1216,160360,\'2\',68,\'18\',2),(26,1207,160154,\'2\',63,\'21\',2),(26,1207,160151,\'8\',63,\'25\',2),(26,1207,160153,\'2\',77,\'17\',2),(26,1207,160160,\'2\',77,\'17\',2),(26,1221,160482,\'8\',74,\'25\',2),(26,1221,160480,\'2\',74,\'13\',2),(26,1221,160474,\'7\',79,\'11\',2),(26,1221,160476,\'1\',79,\'13\',2),(26,1221,160466,\'3\',86,\'15\',2),(26,1221,160481,\'2\',86,\'28\',2),(27,1217,160372,\'8\',46,\'77\',2),(27,1217,160373,\'7\',46,\'24\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_in_out" ("match_no","team_id","player_id","in_out","time_in_out","play_schedule","play_half") VALUES (27,1217,160381,\'4\',52,\'26\',2),(27,1217,160386,\'2\',52,\'29\',2),(27,1217,160384,\'2\',70,\'20\',2),(27,1217,160391,\'5\',70,\'9-\',2),(27,1224,160541,\'2\',74,\'26\',2),(27,1224,160540,\'2\',74,\'35\',2),(27,1224,160542,\'2\',76,\'5-\',2),(27,1224,160543,\'2\',76,\'24\',2),(27,1224,160548,\'2\',83,\'15\',2),(27,1224,160547,\'5\',83,\'15\',2),(28,1206,160136,\'8\',56,\'8-\',2),(28,1206,160133,\'4\',56,\'29\',2),(28,1206,160126,\'5\',61,\'13\',2),(28,1206,160130,\'1\',61,\'21\',2),(28,1206,160134,\'8\',76,\'51\',2),(28,1206,160137,\'3\',76,\'10\',2),(28,1218,160402,\'2\',57,\'26\',2),(28,1218,160403,\'6\',57,\'24\',2),(28,1218,160396,\'1\',67,\'99\',2),(28,1218,160409,\'1\',67,\'15\',2),(28,1218,160400,\'1\',78,\'12\',2),(28,1218,160411,\'1\',78,\'16\',2),(29,1213,160287,\'9\',46,\'74\',2),(29,1213,160296,\'2\',46,\'78\',2),(29,1213,160288,\'1\',71,\'75\',2),(29,1213,160290,\'1\',71,\'24\',2),(29,1213,160295,\'2\',3,\'15\',2),(29,1213,160298,\'1\',3,\'28\',2),(29,1223,160519,\'2\',73,\'19\',2),(29,1223,160526,\'2\',73,\'11\',2),(29,1223,160524,\'1\',1,\'20\',2),(29,1223,160529,\'1\',1,\'90\',2),(30,1208,160179,\'8\',55,\'18\',2),(30,1208,160174,\'1\',55,\'15\',2),(30,1208,160180,\'3\',69,\'28\',2),(30,1208,160175,\'1\',69,\'10\',2),(30,1208,160167,\'2\',76,\'5-\',2),(30,1208,160165,\'9\',76,\'54\',2),(30,1212,160274,\'2\',59,\'20\',2),(30,1212,160276,\'2\',59,\'13\',2),(30,1212,160270,\'1\',70,\'11\',2),(30,1212,160272,\'1\',70,\'9-\',2),(30,1212,160275,\'3\',84,\'34\',2),(30,1212,160268,\'2\',84,\'26\',2),(31,1205,160115,\'3\',57,\'16\',2),(31,1205,160107,\'1\',57,\'44\',2),(31,1205,160112,\'8\',71,\'19\',2),(31,1205,160104,\'1\',71,\'16\',2),(31,1205,160105,\'1\',90,\'14\',2),(31,1205,160108,\'2\',90,\'11\',2),(31,1222,160498,\'8\',61,\'22\',2),(31,1222,160502,\'2\',61,\'23\',2),(31,1222,160499,\'2\',69,\'25\',2),(31,1222,160506,\'8\',69,\'24\',2),(31,1222,160505,\'4\',90,\'88\',2),(31,1222,160504,\'2\',90,\'15\',2),(32,1204,160083,\'9\',82,\'55\',2),(32,1204,160087,\'2\',82,\'25\',2),(32,1204,160088,\'2\',90,\'67\',2),(32,1204,160092,\'1\',90,\'62\',2),(32,1204,160090,\'1\',2,\'19\',2),(32,1204,160085,\'2\',2,\'86\',2),(32,1219,160426,\'7\',60,\'27\',2),(32,1219,160436,\'2\',60,\'16\',2),(32,1219,160433,\'1\',67,\'12\',2),(32,1219,160435,\'6\',67,\'20\',2),(32,1219,160432,\'2\',84,\'24\',2),(32,1219,160428,\'8\',84,\'52\',2),(33,1202,160042,\'2\',46,\'7-\',2),(33,1202,160045,\'2\',46,\'75\',2),(33,1202,160031,\'1\',46,\'14\',2),(33,1202,160038,\'3\',46,\'20\',2),(33,1202,160039,\'1\',78,\'23\',2),(33,1202,160041,\'9\',78,\'29\',2),(33,1210,160211,\'5\',71,\'32\',2),(33,1210,160226,\'2\',71,\'27\',2),(33,1210,160219,\'2\',80,\'11\',2),(33,1210,160230,\'1\',80,\'28\',2),(33,1210,160214,\'1\',86,\'25\',2),(33,1210,160229,\'1\',86,\'16\',2),(34,1209,160188,\'1\',46,\'15\',2),(34,1209,160203,\'2\',46,\'25\',2),(34,1209,160204,\'4\',71,\'22\',2),(34,1209,160207,\'1\',71,\'15\',2),(34,1209,160200,\'9\',83,\'11\',2),(34,1209,160198,\'5\',83,\'22\',2),(34,1214,160316,\'2\',46,\'48\',2),(34,1214,160314,\'2\',46,\'24\',2),(34,1214,160321,\'2\',61,\'27\',2),(34,1214,160311,\'2\',61,\'45\',2),(34,1214,160313,\'1\',81,\'52\',2),(34,1214,160320,\'2\',81,\'66\',2),(35,1211,160237,\'4\',60,\'23\',2),(35,1211,160240,\'1\',60,\'5-\',2),(35,1211,160251,\'9\',74,\'26\',2),(35,1211,160250,\'2\',74,\'0-\',2),(35,1211,160249,\'2\',81,\'22\',2),(35,1211,160238,\'5\',81,\'2-\',2),(35,1215,160338,\'2\',70,\'23\',2),(35,1215,160344,\'4\',70,\'3-\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_in_out" ("match_no","team_id","player_id","in_out","time_in_out","play_schedule","play_half") VALUES (35,1215,160335,\'1\',77,\'53\',2),(35,1215,160336,\'1\',77,\'20\',2),(35,1215,160340,\'1\',90,\'23\',2),(35,1215,160343,\'1\',90,\'13\',2),(36,1203,160068,\'2\',71,\'76\',2),(36,1203,160058,\'1\',71,\'29\',2),(36,1203,160066,\'2\',87,\'13\',2),(36,1203,160067,\'1\',87,\'29\',2),(36,1203,160069,\'5\',2,\'6-\',2),(36,1203,160062,\'8\',2,\'17\',2),(36,1220,160458,\'1\',63,\'88\',2),(36,1220,160457,\'2\',63,\'15\',2),(36,1220,160448,\'1\',70,\'38\',2),(36,1220,160454,\'9\',70,\'18\',2),(36,1220,160450,\'2\',82,\'14\',2),(36,1220,160451,\'1\',82,\'23\',2),(37,1213,160289,\'1\',101,\'13\',1),(37,1213,160293,\'1\',101,\'15\',1),(37,1213,160294,\'1\',104,\'15\',1),(37,1213,160288,\'6\',104,\'11\',1),(37,1221,160480,\'1\',58,\'15\',2),(37,1221,160473,\'1\',58,\'22\',2),(37,1221,160479,\'6\',70,\'24\',2),(37,1221,160481,\'2\',70,\'28\',2),(37,1221,160474,\'5\',77,\'26\',2),(37,1221,160472,\'4\',77,\'28\',2),(38,1212,160276,\'2\',69,\'29\',2),(38,1212,160272,\'6\',69,\'28\',2),(38,1212,160275,\'1\',79,\'6-\',2),(38,1212,160271,\'1\',79,\'13\',2),(38,1212,160270,\'1\',84,\'45\',2),(38,1212,160262,\'5\',84,\'19\',2),(38,1224,160550,\'8\',55,\'92\',2),(38,1224,160551,\'2\',55,\'25\',2),(38,1224,160546,\'5\',63,\'15\',2),(38,1224,160543,\'1\',63,\'22\',2),(39,1204,160092,\'2\',110,\'11\',2),(39,1204,160086,\'2\',110,\'41\',2),(39,1204,160090,\'1\',120,\'24\',2),(39,1204,160073,\'2\',120,\'10\',2),(39,1204,160089,\'1\',88,\'14\',2),(39,1204,160091,\'1\',88,\'7-\',2),(39,1214,160313,\'1\',117,\'24\',2),(39,1214,160310,\'8\',117,\'49\',2),(39,1214,160316,\'1\',50,\'93\',2),(39,1214,160311,\'2\',50,\'21\',2),(39,1214,160321,\'1\',87,\'33\',2),(39,1214,160313,\'7\',87,\'55\',2),(40,1207,160151,\'1\',46,\'52\',2),(40,1207,160152,\'1\',46,\'27\',2),(40,1207,160158,\'2\',73,\'1-\',2),(40,1207,160159,\'8\',73,\'80\',2),(40,1207,160157,\'2\',2,\'23\',2),(40,1207,160151,\'2\',2,\'18\',2),(40,1215,160345,\'1\',65,\'17\',2),(40,1215,160344,\'6\',65,\'28\',2),(40,1215,160331,\'2\',68,\'63\',2),(40,1215,160337,\'2\',68,\'12\',2),(40,1215,160335,\'2\',71,\'20\',2),(40,1215,160336,\'2\',71,\'17\',2),(41,1208,160167,\'8\',72,\'40\',2),(41,1208,160184,\'2\',72,\'23\',2),(41,1208,160165,\'1\',72,\'25\',2),(41,1208,160173,\'5\',72,\'25\',2),(41,1208,160180,\'1\',76,\'27\',2),(41,1208,160175,\'2\',76,\'23\',2),(41,1218,160404,\'2\',46,\'27\',2),(41,1218,160411,\'1\',46,\'88\',2),(41,1218,160414,\'1\',64,\'19\',2),(41,1218,160412,\'2\',64,\'24\',2),(41,1218,160399,\'6\',84,\'25\',2),(41,1218,160396,\'1\',84,\'72\',2),(42,1203,160058,\'1\',70,\'18\',2),(42,1203,160068,\'1\',70,\'29\',2),(42,1203,160065,\'8\',76,\'19\',2),(42,1203,160067,\'6\',76,\'17\',2),(42,1203,160061,\'2\',81,\'42\',2),(42,1203,160062,\'2\',81,\'18\',2),(42,1209,160196,\'2\',46,\'28\',2),(42,1209,160203,\'2\',46,\'27\',2),(42,1209,160205,\'1\',75,\'23\',2),(42,1209,160195,\'8\',75,\'24\',2),(42,1209,160201,\'5\',79,\'26\',2),(42,1209,160191,\'3\',79,\'11\',2),(43,1211,160245,\'9\',54,\'78\',2),(43,1211,160242,\'2\',54,\'11\',2),(43,1211,160251,\'4\',82,\'10\',2),(43,1211,160248,\'1\',82,\'39\',2),(43,1211,160237,\'1\',84,\'20\',2),(43,1211,160243,\'1\',84,\'98\',2),(43,1219,160433,\'2\',46,\'19\',2),(43,1219,160436,\'1\',46,\'13\',2),(43,1219,160434,\'7\',70,\'27\',2),(43,1219,160435,\'1\',70,\'28\',2),(43,1219,160437,\'2\',81,\'95\',2),(43,1219,160433,\'1\',81,\'37\',2),(44,1206,160133,\'2\',46,\'18\',2),(44,1206,160128,\'8\',46,\'16\',2),(44,1206,160138,\'1\',60,\'25\',2),(44,1206,160132,\'6\',60,\'30\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_in_out" ("match_no","team_id","player_id","in_out","time_in_out","play_schedule","play_half") VALUES (44,1206,160135,\'7\',87,\'21\',2),(44,1206,160136,\'1\',87,\'45\',2),(44,1210,160211,\'1\',76,\'8-\',2),(44,1210,160230,\'6\',76,\'28\',2),(44,1210,160219,\'4\',89,\'20\',2),(44,1210,160226,\'2\',89,\'16\',2),(45,1213,160289,\'2\',98,\'14\',1),(45,1213,160293,\'3\',98,\'11\',1),(45,1213,160290,\'1\',82,\'26\',2),(45,1213,160288,\'5\',82,\'24\',2),(45,1214,160313,\'7\',96,\'27\',1),(45,1214,160318,\'2\',96,\'22\',1),(45,1214,160314,\'6\',70,\'34\',2),(45,1214,160310,\'8\',70,\'26\',2),(45,1214,160321,\'2\',80,\'29\',2),(45,1214,160313,\'6\',80,\'15\',2),(46,1203,160061,\'2\',46,\'22\',2),(46,1203,160058,\'1\',46,\'12\',2),(46,1203,160068,\'8\',75,\'17\',2),(46,1203,160054,\'7\',75,\'87\',2),(46,1203,160065,\'1\',83,\'15\',2),(46,1203,160067,\'2\',83,\'15\',2),(46,1224,160542,\'5\',78,\'86\',2),(46,1224,160543,\'1\',78,\'21\',2),(46,1224,160551,\'1\',80,\'12\',2),(46,1224,160550,\'2\',80,\'15\',2),(46,1224,160534,\'7\',90,\'29\',2),(46,1224,160544,\'7\',90,\'24\',2),(47,1208,160180,\'2\',16,\'27\',1),(47,1208,160175,\'2\',16,\'23\',1),(47,1208,160173,\'2\',72,\'22\',2),(47,1208,160182,\'2\',72,\'13\',2),(47,1211,160253,\'2\',120,\'8-\',2),(47,1211,160236,\'2\',120,\'21\',2),(47,1211,160237,\'2\',86,\'22\',2),(47,1211,160243,\'2\',86,\'29\',2),(47,1211,160251,\'6\',108,\'15\',2),(47,1211,160248,\'8\',108,\'18\',2),(48,1207,160158,\'2\',60,\'46\',2),(48,1207,160159,\'2\',60,\'26\',2),(48,1207,160146,\'9\',72,\'11\',2),(48,1207,160145,\'2\',72,\'14\',2),(48,1207,160151,\'7\',80,\'16\',2),(48,1207,160154,\'1\',80,\'12\',2),(48,1210,160214,\'1\',46,\'13\',2),(48,1210,160227,\'2\',46,\'56\',2),(48,1210,160220,\'6\',46,\'8-\',2),(48,1210,160226,\'9\',46,\'12\',2),(48,1210,160228,\'9\',83,\'89\',2),(48,1210,160230,\'2\',83,\'10\',2),(49,1214,160311,\'1\',74,\'20\',2),(49,1214,160316,\'3\',74,\'11\',2),(49,1214,160314,\'1\',79,\'19\',2),(49,1214,160310,\'2\',79,\'36\',2),(49,1214,160321,\'1\',86,\'15\',2),(49,1214,160320,\'2\',86,\'21\',2),(49,1224,160551,\'1\',58,\'28\',2),(49,1224,160543,\'2\',58,\'25\',2),(49,1224,160548,\'6\',63,\'22\',2),(49,1224,160550,\'1\',63,\'72\',2),(49,1224,160546,\'2\',66,\'18\',2),(49,1224,160534,\'1\',66,\'65\',2),(50,1207,160152,\'1\',71,\'17\',2),(50,1207,160154,\'2\',71,\'22\',2),(50,1207,160158,\'2\',78,\'13\',2),(50,1207,160159,\'1\',78,\'27\',2),(50,1207,160150,\'2\',2,\'28\',2),(50,1207,160160,\'1\',2,\'95\',2),(50,1208,160170,\'2\',61,\'18\',2),(50,1208,160165,\'7\',61,\'29\',2),(50,1208,160174,\'2\',67,\'27\',2),(50,1208,160172,\'2\',67,\'90\',2),(50,1208,160178,\'2\',79,\'27\',2),(50,1208,160180,\'2\',79,\'2-\',2),(51,1207,160161,\'5\',110,\'13\',2),(51,1207,160154,\'2\',110,\'26\',2),(51,1207,160151,\'1\',58,\'25\',2),(51,1207,160154,\'2\',58,\'33\',2),(51,1207,160158,\'2\',78,\'61\',2),(51,1207,160159,\'2\',78,\'7-\',2),(51,1214,160321,\'2\',25,\'29\',1),(51,1214,160322,\'2\',25,\'21\',1),(51,1214,160314,\'2\',66,\'11\',2),(51,1214,160310,\'5\',66,\'49\',2),(51,1214,160319,\'1\',79,\'22\',2),(51,1214,160316,\'2\',79,\'14\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `goal_details`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "goal_details" ("goal_id","match_no","player_id","team_id","goal_time","goal_type","play_stage","goal_schedule","goal_half") VALUES (1,1,160159,1207,57,\'N\',\'G\',\'NT\',2),(2,1,160368,1216,65,\'P\',\'G\',\'NT\',2),(3,1,160154,1207,89,\'N\',\'G\',\'NT\',2),(4,2,160470,1221,5,\'N\',\'G\',\'NT\',1),(5,3,160547,1224,10,\'N\',\'G\',\'NT\',1),(6,3,160403,1218,61,\'N\',\'G\',\'NT\',2),(7,3,160550,1224,81,\'N\',\'G\',\'NT\',2),(8,4,160128,1206,73,\'N\',\'G\',\'NT\',2),(9,4,160373,1217,93,\'N\',\'G\',\'ST\',2),(10,5,160084,1204,41,\'N\',\'G\',\'NT\',1),(11,6,160298,1213,51,\'N\',\'G\',\'NT\',2),(12,7,160183,1208,19,\'N\',\'G\',\'NT\',1),(13,7,160180,1208,93,\'N\',\'G\',\'ST\',2),(14,8,160423,1219,87,\'N\',\'G\',\'NT\',2),(15,9,160335,1215,48,\'N\',\'G\',\'NT\',2),(16,9,160327,1215,71,\'O\',\'G\',\'NT\',2),(17,10,160244,1211,32,\'N\',\'G\',\'NT\',1),(18,10,160252,1211,93,\'N\',\'G\',\'ST\',2),(19,11,160207,1209,62,\'N\',\'G\',\'NT\',2),(20,11,160200,1209,87,\'N\',\'G\',\'NT\',2),(21,12,160320,1214,31,\'N\',\'G\',\'NT\',1),(22,12,160221,1210,50,\'N\',\'G\',\'NT\',2),(23,13,160411,1218,32,\'N\',\'G\',\'NT\',1),(24,13,160405,1218,45,\'N\',\'G\',\'NT\',1),(25,13,160380,1217,80,\'N\',\'G\',\'NT\',2),(26,14,160368,1216,18,\'P\',\'G\',\'NT\',1),(27,14,160481,1221,57,\'N\',\'G\',\'NT\',2),(28,15,160160,1207,90,\'N\',\'G\',\'NT\',2),(29,15,160154,1207,96,\'N\',\'G\',\'ST\',2),(30,16,160547,1224,42,\'N\',\'G\',\'NT\',1),(31,16,160138,1206,56,\'N\',\'G\',\'NT\',2),(32,16,160137,1206,93,\'N\',\'G\',\'ST\',2),(33,17,160262,1212,49,\'N\',\'G\',\'NT\',2),(34,17,160275,1212,96,\'N\',\'G\',\'ST\',2),(35,19,160248,1211,88,\'N\',\'G\',\'NT\',2),(36,20,160085,1204,37,\'N\',\'G\',\'NT\',1),(37,20,160086,1204,59,\'N\',\'G\',\'NT\',2),(38,20,160115,1205,76,\'N\',\'G\',\'NT\',2),(39,20,160114,1205,89,\'P\',\'G\',\'NT\',2),(40,21,160435,1219,34,\'N\',\'G\',\'NT\',1),(41,21,160436,1219,37,\'N\',\'G\',\'NT\',1),(42,21,160435,1219,48,\'N\',\'G\',\'NT\',2),(43,22,160067,1203,48,\'N\',\'G\',\'NT\',2),(44,22,160064,1203,61,\'N\',\'G\',\'NT\',2),(45,22,160067,1203,70,\'N\',\'G\',\'NT\',2),(46,23,160224,1210,40,\'P\',\'G\',\'NT\',1),(47,23,160216,1210,88,\'O\',\'G\',\'NT\',2),(48,25,160023,1201,43,\'N\',\'G\',\'NT\',1),(49,27,160544,1224,11,\'N\',\'G\',\'NT\',1),(50,27,160538,1224,20,\'N\',\'G\',\'NT\',1),(51,27,160547,1224,67,\'N\',\'G\',\'NT\',2),(52,29,160287,1213,54,\'9\',\'2\',\'10\',2),(53,30,160182,1208,30,\'2\',\'2\',\'15\',1),(54,31,160504,1222,10,\'2\',\'2\',\'35\',1),(55,31,160500,1222,65,\'2\',\'1\',\'24\',2),(56,32,160435,1219,7,\'1\',\'2\',\'92\',1),(57,32,160089,1204,45,\'2\',\'1\',\'20\',1),(58,32,160085,1204,87,\'1\',\'2\',\'22\',2),(59,33,160226,1210,18,\'7\',\'1\',\'24\',1),(60,33,160042,1202,60,\'1\',\'1\',\'11\',2),(61,33,160226,1210,94,\'2\',\'2\',\'17\',2),(62,34,160203,1209,19,\'8\',\'2\',\'5-\',1),(63,34,160320,1214,42,\'1\',\'2\',\'11\',1),(64,34,160202,1209,47,\'2\',\'9\',\'27\',2),(65,34,160322,1214,50,\'1\',\'1\',\'62\',2),(66,34,160202,1209,55,\'2\',\'2\',\'43\',2),(67,34,160322,1214,62,\'1\',\'2\',\'3-\',2),(68,35,160333,1215,85,\'0\',\'1\',\'29\',2),(69,36,160063,1203,84,\'3\',\'5\',\'17\',2),(70,37,160287,1213,39,\'1\',\'9\',\'12\',1),(71,37,160476,1221,82,\'1\',\'2\',\'11\',2),(72,38,160262,1212,75,\'1\',\'1\',\'11\',2),(73,39,160321,1214,117,\'2\',\'2\',\'16\',2),(74,40,160333,1215,2,\'6\',\'1\',\'26\',1),(75,40,160160,1207,58,\'2\',\'1\',\'23\',2),(76,40,160160,1207,61,\'6\',\'3\',\'14\',2),(77,41,160165,1208,8,\'1\',\'1\',\'21\',1),(78,41,160182,1208,43,\'9\',\'2\',\'36\',1),(79,41,160173,1208,63,\'2\',\'5\',\'23\',2),(80,42,160050,1203,10,\'1\',\'7\',\'21\',1),(81,42,160065,1203,78,\'2\',\'1\',\'20\',2),(82,42,160062,1203,80,\'2\',\'2\',\'27\',2),(83,42,160058,1203,90,\'2\',\'1\',\'52\',2),(84,43,160236,1211,33,\'1\',\'4\',\'16\',1),(85,43,160252,1211,91,\'3\',\'1\',\'25\',2),(86,44,160136,1206,4,\'2\',\'2\',\'19\',1),(87,44,160219,1210,6,\'1\',\'1\',\'59\',1),(88,44,160230,1210,18,\'8\',\'2\',\'27\',1),(89,45,160297,1213,2,\'1\',\'7\',\'95\',1),(90,45,160316,1214,33,\'3\',\'1\',\'85\',1),(91,46,160063,1203,13,\'1\',\'1\',\'26\',1),(92,46,160539,1224,31,\'2\',\'3\',\'38\',1),(93,46,160550,1224,55,\'2\',\'2\',\'27\',2),(94,46,160551,1224,86,\'2\',\'2\',\'23\',2),(95,47,160177,1208,65,\'1\',\'2\',\'24\',2),(96,47,160235,1211,78,\'2\',\'2\',\'22\',2),(97,48,160159,1207,12,\'2\',\'1\',\'6-\',1),(98,48,160155,1207,20,\'2\',\'6\',\'18\',1),(99,48,160154,1207,43,\'1\',\'2\',\'75\',1),(100,48,160160,1207,45,\'1\',\'1\',\'90\',1);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "goal_details" ("goal_id","match_no","player_id","team_id","goal_time","goal_type","play_stage","goal_schedule","goal_half") VALUES (101,48,160230,1210,56,\'6\',\'2\',\'14\',2),(102,48,160159,1207,59,\'2\',\'9\',\'14\',2),(103,48,160221,1210,84,\'1\',\'7\',\'38\',2),(104,49,160322,1214,50,\'2\',\'1\',\'15\',2),(105,49,160320,1214,53,\'2\',\'1\',\'28\',2),(106,50,160160,1207,47,\'2\',\'7\',\'35\',1),(107,50,160160,1207,72,\'2\',\'2\',\'13\',2),(108,51,160319,1214,109,\'7\',\'3\',\'24\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `match_captain`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "match_captain" ("match_no","team_id","player_captain") VALUES (1,1207,160140),(1,1216,160349),(2,1201,160013),(2,1221,160467),(3,1224,160539),(3,1218,160401),(4,1206,160136),(4,1217,160373),(5,1222,160494),(5,1204,160076),(6,1213,160297),(6,1212,160267),(7,1208,160163),(7,1223,160515),(8,1219,160424),(8,1205,160110),(9,1215,160331),(9,1220,160459),(10,1203,160062),(10,1211,160231),(11,1202,160028),(11,1209,160202),(12,1214,160322),(12,1210,160222),(13,1217,160373),(13,1218,160401),(14,1216,160349),(14,1221,160467),(15,1207,160140),(15,1201,160004),(16,1206,160136),(16,1224,160539),(17,1223,160515),(17,1212,160267),(18,1208,160163),(18,1213,160297),(19,1211,160231),(19,1220,160459),(20,1205,160110),(20,1204,160076),(21,1219,160424),(21,1222,160494),(22,1203,160062),(22,1215,160331),(23,1210,160222),(23,1209,160202),(24,1214,160322),(24,1202,160028),(25,1216,160349),(25,1201,160004),(26,1221,160467),(26,1207,160140),(27,1217,160386),(27,1224,160539),(28,1218,160401),(28,1206,160120),(29,1223,160520),(29,1213,160297),(30,1212,160267),(30,1208,160163),(31,1205,160093),(31,1222,160494),(32,1204,160076),(32,1219,160424),(33,1210,160222),(33,1202,160028),(34,1209,160202),(34,1214,160322),(35,1211,160235),(35,1215,160328),(36,1220,160459),(36,1203,160062),(37,1221,160467),(37,1213,160297),(38,1224,160539),(38,1212,160267),(39,1204,160076),(39,1214,160322),(40,1207,160140),(40,1215,160328),(41,1208,160163),(41,1218,160401),(42,1209,160202),(42,1203,160062),(43,1211,160231),(43,1219,160424),(44,1206,160136),(44,1210,160222),(45,1213,160297),(45,1214,160322),(46,1224,160539),(46,1203,160062),(47,1208,160163),(47,1211,160231),(48,1207,160140),(48,1210,160222),(49,1214,160322),(49,1224,160539),(50,1207,160140),(50,1208,160180);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "match_captain" ("match_no","team_id","player_captain") VALUES (51,1214,160322),(51,1207,160140);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `asst_referee_mast`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "asst_referee_mast" ("ass_ref_id","ass_ref_name","country_id") VALUES (80034,\'Tomas Mokrusch\',1205),(80038,\'Martin Wilczek\',1205),(80004,\'Simon Beck\',1206),(80006,\'Stephen Child\',1206),(80007,\'Jake Collin\',1206),(80014,\'Mike Mullarkey\',1206),(80026,\'Frederic Cano\',1207),(80028,\'Nicolas Danos\',1207),(80005,\'Mark Borsch\',1208),(80013,\'Stefan Lupp\',1208),(80016,\'Gyorgy Ring\',1209),(80020,\'Vencel Toth\',1209),(80033,\'Damien McGraith\',1215),(80008,\'Elenito Di Liberatore\',1211),(80019,\'Mauro Tonolini\',1211),(80021,\'Sander van Roekel\',1226),(80024,\'Erwin Zeinstra\',1226),(80025,\'Frank Andas\',1229),(80031,\'Kim Haglund\',1229),(80012,\'Tomasz Listkiewicz\',1213),(80018,\'Pawel Sokolnicki\',1213),(80029,\'Sebastian Gheorghe\',1216),(80036,\'Octavian Sovre\',1216),(80030,\'Nikolay Golubev\',1217),(80032,\'Tikhon Kalugin\',1217),(80037,\'Anton Averyanov\',1217),(80027,\'Frank Connor\',1228),(80010,\'Dalibor Durdevic\',1227),(80017,\'Milovan Ristic\',1227),(80035,\'Roman Slysko\',1218),(80001,\'Jure Praprotnik\',1225),(80002,\'Robert Vukan\',1225),(80003,\'Roberto Alonso Fernandez\',1219),(80023,\'Juan Yuste Jimenez\',1219),(80011,\'Mathias Klasenius\',1220),(80022,\'Daniel Warnmark\',1220),(80009,\'Bahattin Duran\',1222),(80015,\'Tarik Ongun\',1222);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `reviewer`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "reviewer" ("rev_id","rev_name") VALUES (9001,\'Righty Sock                   \'),(9002,\'Jack Malvern                  \'),(9003,\'Flagrant Baronessa            \'),(9004,\'Alec Shaw                     \'),(9005,NULL),(9006,\'Victor Woeltjen               \'),(9007,\'Simon Wright                  \'),(9008,\'Neal Wruck                    \'),(9009,\'Paul Monks                    \'),(9010,\'Mike Salvati                  \'),(9011,NULL),(9012,\'Wesley S. Walker              \'),(9013,\'Sasha Goldshtein              \'),(9014,\'Josh Cates                    \'),(9015,\'Krug Stillo                   \'),(9016,\'Scott LeBrun                  \'),(9017,\'Hannah Steele                 \'),(9018,\'Vincent Cadena                \'),(9019,\'Brandt Sponseller             \'),(9020,\'Richard Adams                 \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `penalty_gk`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "penalty_gk" ("match_no","team_id","player_gk") VALUES (37,1221,160463),(37,1213,160278),(45,1213,160278),(45,1214,160302),(47,1208,160163),(47,1211,160231);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `regions`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "regions" ("region_id","region_name") VALUES (1,\'Europe                   \'),(2,\'Americas                 \'),(3,\'Asia                     \'),(4,\'Middle East and Africa   \');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `related`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "related" ("city") VALUES (\'New York\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `rating`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "rating" ("mov_id","rev_id","rev_stars","num_o_ratings") VALUES (901,9001,8.40,263575),(902,9002,7.90,20207),(903,9003,8.30,202778),(906,9005,8.20,484746),(924,9006,7.30,NULL),(908,9007,8.60,779489),(909,9008,NULL,227235),(910,9009,3.00,195961),(911,9010,8.10,203875),(912,9011,8.40,NULL),(914,9013,7.00,862618),(915,9001,7.70,830095),(916,9014,4.00,642132),(925,9015,7.70,81328),(918,9016,NULL,580301),(920,9017,8.10,609451),(921,9018,8.00,667758),(922,9019,8.40,511613),(923,9020,6.70,13091);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `orders`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "orders" ("ord_no","purch_amt","ord_date","customer_id","salesman_id") VALUES (70009,270.65,'2012-09-10 00:00:00',3001,5005),(70002,65.26,'2012-10-05 00:00:00',3002,5001),(70004,110.50,'2012-08-17 00:00:00',3009,5003),(70005,2400.60,'2012-07-27 00:00:00',3007,5001),(70008,5760.00,'2012-09-10 00:00:00',3002,5001),(70010,1983.43,'2012-10-10 00:00:00',3004,5006),(70003,2480.40,'2012-10-10 00:00:00',3009,5003),(70011,75.29,'2012-08-17 00:00:00',3003,5007),(70013,3045.60,'2012-04-25 00:00:00',3002,5001),(70001,150.50,'2012-10-05 00:00:00',3005,5002),(70007,948.50,'2012-09-10 00:00:00',3005,5002),(70012,250.45,'2012-06-27 00:00:00',3008,5002);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `scores`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "scores" ("id","score") VALUES (1,100);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `penalty_shootout`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "penalty_shootout" ("kick_id","match_no","team_id","player_id","score_goal","kick_no") VALUES (1,37,1221,160467,\'Y\',1),(2,37,1213,160297,\'Y\',2),(3,37,1221,160477,\'N\',3),(4,37,1213,160298,\'Y\',4),(5,37,1221,160476,\'Y\',5),(6,37,1213,160281,\'Y\',6),(7,37,1221,160470,\'Y\',7),(8,37,1213,160287,\'Y\',8),(9,37,1221,160469,\'Y\',9),(10,37,1213,160291,\'Y\',10),(11,45,1214,160322,\'Y\',1),(12,45,1213,160297,\'Y\',2),(13,45,1214,160316,\'Y\',3),(14,45,1213,160298,\'Y\',4),(15,45,1214,160314,\'Y\',5),(16,45,1213,160281,\'Y\',6),(17,45,1214,160320,\'Y\',7),(18,45,1213,160287,\'N\',8),(19,45,1214,160321,\'Y\',9),(20,47,1211,160251,\'Y\',1),(21,47,1208,160176,\'Y\',2),(22,47,1211,160253,\'N\',3),(23,47,1208,160183,\'N\',4),(24,47,1211,160234,\'Y\',5),(25,47,1208,160177,\'N\',6),(26,47,1211,160252,\'N\',7),(27,47,1208,160173,\'Y\',8),(28,47,1211,160235,\'N\',9),(29,47,1208,160180,\'N\',10),(30,47,1211,160244,\'Y\',11),(31,47,1208,160168,\'Y\',12),(32,47,1211,160246,\'Y\',13),(33,47,1208,160169,\'Y\',14),(34,47,1211,160238,\'Y\',15),(35,47,1208,160165,\'Y\',16),(36,47,1211,160237,\'N\',17),(37,47,1208,160166,\'Y\',18);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `soccer_team`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "soccer_team" ("team_id","team_group","match_played","won","draw","lost","goal_for","goal_agnst","goal_diff","points","group_position") VALUES (1201,\'A\',3,1,0,2,1,3,-2,3,3),(1202,\'F\',3,0,1,2,1,4,-3,1,4),(1203,\'E\',3,2,0,1,4,2,2,6,2),(1204,\'D\',3,2,1,0,5,3,2,7,1),(1205,\'D\',3,0,1,2,2,5,-3,1,4),(1206,\'B\',3,1,2,0,3,2,1,5,2),(1207,\'A\',3,2,1,0,4,1,3,7,1),(1208,\'C\',3,2,1,0,3,0,3,7,1),(1209,\'F\',3,1,2,0,6,4,2,5,1),(1210,\'F\',3,1,2,0,4,3,1,5,2),(1211,\'E\',3,2,0,1,3,1,2,6,1),(1212,\'C\',3,1,0,2,2,2,0,3,3),(1213,\'C\',3,2,1,0,2,0,2,7,2),(1214,\'F\',3,0,3,0,4,4,0,3,3),(1215,\'E\',3,1,1,1,2,4,-2,4,3),(1216,\'A\',3,0,1,2,2,4,-2,1,4),(1217,\'B\',3,0,1,2,2,6,-4,1,4),(1218,\'B\',3,1,1,1,3,3,0,4,3),(1219,\'D\',3,2,0,1,5,2,3,6,2),(1220,\'E\',3,0,1,2,1,3,-2,1,4),(1221,\'A\',3,1,2,0,2,1,1,5,2),(1222,\'D\',3,1,0,2,2,4,-2,3,3),(1223,\'C\',3,0,0,3,0,5,-5,0,4),(1224,\'B\',3,2,0,1,6,3,3,6,1);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `player_booked`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_booked" ("match_no","team_id","player_id","booking_time","sent_off","play_schedule","play_half") VALUES (1,1216,160349,32,\' \',\'NT\',1),(1,1216,160355,45,\' \',\'NT\',1),(1,1207,160159,69,\'Y\',\'NT\',2),(1,1216,160360,78,\' \',\'NT\',2),(2,1221,160470,14,\' \',\'NT\',1),(2,1201,160013,23,\' \',\'NT\',1),(2,1201,160013,36,\' \',\'NT\',1),(2,1201,160014,63,\' \',\'NT\',2),(2,1221,160472,66,\' \',\'NT\',2),(2,1201,160015,89,\' \',\'NT\',2),(2,1201,160009,93,\' \',\'NT\',2),(3,1218,160401,2,\' \',\'ST\',2),(3,1218,160406,31,\' \',\'NT\',1),(3,1218,160408,78,\' \',\'NT\',2),(3,1218,160411,80,\' \',\'NT\',2),(3,1218,160407,83,\' \',\'NT\',2),(4,1206,160120,62,\' \',\'NT\',2),(4,1217,160377,72,\' \',\'NT\',2),(5,1222,160505,31,\' \',\'NT\',1),(5,1222,160490,48,\' \',\'NT\',2),(5,1204,160077,80,\' \',\'NT\',2),(5,1222,160502,91,\' \',\'NT\',2),(6,1213,160290,65,\' \',\'NT\',2),(6,1212,160258,69,\' \',\'NT\',2),(6,1213,160284,89,\' \',\'NT\',2),(7,1223,160518,68,\' \',\'NT\',2),(8,1205,160100,61,\' \',\'NT\',2),(9,1215,160336,43,\' \',\'NT\',1),(9,1220,160445,61,\' \',\'NT\',2),(9,1215,160341,77,\' \',\'NT\',2),(10,1211,160236,65,\' \',\'NT\',2),(10,1211,160248,75,\' \',\'NT\',2),(10,1211,160235,78,\' \',\'NT\',2),(10,1211,160245,84,\' \',\'NT\',2),(10,1203,160057,93,\' \',\'NT\',2),(11,1202,160027,33,\' \',\'NT\',1),(11,1202,160027,66,\'Y\',\'NT\',2),(11,1209,160204,80,\' \',\'NT\',2),(12,1210,160227,2,\' \',\'ST\',2),(12,1210,160221,55,\' \',\'NT\',2),(13,1218,160395,46,\' \',\'NT\',2),(14,1221,160480,2,\' \',\'ST\',2),(14,1216,160361,22,\' \',\'NT\',1),(14,1216,160357,24,\' \',\'NT\',1),(14,1216,160367,37,\' \',\'NT\',1),(14,1221,160477,50,\' \',\'NT\',2),(14,1216,160352,76,\' \',\'NT\',2),(15,1201,160015,55,\' \',\'NT\',2),(15,1201,160011,81,\' \',\'NT\',2),(15,1207,160152,88,\' \',\'NT\',2),(16,1224,160535,61,\' \',\'NT\',2),(17,1223,160528,40,\'0\',\'16\',1),(17,1212,160272,63,\'1\',\'14\',2),(17,1223,160523,67,\'1\',\'23\',2),(17,1212,160266,87,\'3\',\'75\',2),(17,1212,160259,90,\'2\',\'26\',2),(18,1208,160175,3,\'1\',\'18\',1),(18,1213,160294,3,\'1\',\'24\',2),(18,1208,160177,34,\'8\',\'19\',1),(18,1213,160293,45,\'2\',\'14\',1),(18,1213,160288,55,\'2\',\'43\',2),(18,1208,160165,67,\'1\',\'11\',2),(19,1211,160242,69,\'2\',\'24\',2),(19,1220,160447,89,\'1\',\'11\',2),(19,1211,160231,94,\'1\',\'80\',2),(20,1204,160080,14,\'2\',\'20\',1),(20,1205,160101,72,\'7\',\'18\',2),(20,1204,160081,74,\'6\',\'24\',2),(20,1204,160078,88,\'2\',\'19\',2),(21,1219,160424,2,\'2\',\'21\',1),(21,1222,160504,9,\'2\',\'15\',1),(21,1222,160500,41,\'2\',\'25\',1),(22,1215,160334,42,\'1\',\'24\',1),(22,1203,160056,49,\'2\',\'27\',2),(23,1209,160199,2,\'1\',\'16\',2),(23,1210,160229,42,\'2\',\'10\',1),(23,1210,160227,75,\'2\',\'17\',2),(23,1210,160216,77,\'2\',\'22\',2),(23,1209,160192,81,\'7\',\'76\',2),(23,1209,160197,83,\'2\',\'13\',2),(24,1202,160028,6,\'2\',\'51\',1),(24,1214,160321,31,\'1\',\'10\',1),(24,1214,160307,40,\'1\',\'84\',1),(24,1202,160037,47,\'1\',\'58\',2),(24,1202,160029,78,\'2\',\'38\',2),(24,1202,160042,86,\'1\',\'16\',2),(25,1201,160012,6,\'1\',\'17\',1),(25,1216,160353,54,\'8\',\'22\',2),(25,1201,160017,85,\'2\',\'29\',2),(25,1216,160356,85,\'2\',\'11\',2),(25,1216,160364,91,\'1\',\'14\',2),(25,1201,160007,95,\'1\',\'43\',2),(26,1207,160147,25,\'1\',\'12\',1),(26,1207,160145,83,\'9\',\'31\',2),(27,1224,160551,16,\'2\',\'14\',1),(27,1217,160383,64,\'1\',\'21\',2),(28,1218,160409,24,\'2\',\'26\',1),(28,1206,160119,52,\'1\',\'6-\',2),(29,1223,160520,25,\'7\',\'29\',1),(29,1223,160513,38,\'6\',\'19\',1);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_booked" ("match_no","team_id","player_id","booking_time","sent_off","play_schedule","play_half") VALUES (29,1213,160290,60,\'1\',\'18\',2),(31,1222,160491,35,\'2\',\'22\',1),(31,1205,160108,36,\'2\',\'12\',1),(31,1205,160107,39,\'5\',\'29\',1),(31,1222,160490,50,\'2\',\'20\',2),(31,1205,160112,87,\'8\',\'21\',2),(32,1204,160087,29,\'2\',\'53\',1),(32,1204,160079,70,\'2\',\'29\',2),(32,1204,160076,70,\'2\',\'10\',2),(32,1204,160085,88,\'1\',\'28\',2),(33,1210,160218,36,\'2\',\'20\',1),(33,1210,160230,51,\'3\',\'23\',2),(33,1202,160045,70,\'2\',\'27\',2),(33,1210,160220,78,\'9\',\'29\',2),(33,1210,160208,82,\'3\',\'21\',2),(34,1209,160190,13,\'5\',\'23\',1),(34,1209,160191,28,\'1\',\'24\',1),(34,1209,160203,34,\'2\',\'18\',1),(34,1209,160202,56,\'4\',\'89\',2),(35,1211,160233,39,\'1\',\'28\',1),(35,1215,160343,39,\'7\',\'26\',1),(35,1215,160332,73,\'1\',\'82\',2),(35,1211,160234,78,\'2\',\'13\',2),(35,1211,160253,87,\'4\',\'12\',2),(35,1211,160251,93,\'2\',\'13\',2),(36,1203,160064,1,\'6\',\'22\',1),(36,1203,160055,30,\'1\',\'81\',1),(36,1220,160451,72,\'2\',\'70\',2),(37,1221,160470,55,\'1\',\'26\',2),(37,1213,160282,58,\'2\',\'52\',2),(38,1212,160266,44,\'1\',\'28\',1),(38,1224,160538,58,\'2\',\'82\',2),(38,1212,160267,67,\'8\',\'13\',2),(38,1224,160544,92,\'6\',\'28\',2),(39,1214,160318,78,\'2\',\'19\',2),(40,1215,160328,25,\'5\',\'17\',1),(40,1207,160152,27,\'2\',\'16\',1),(40,1215,160334,41,\'2\',\'28\',1),(40,1207,160147,44,\'2\',\'4-\',1),(40,1215,160329,66,\'9\',\'69\',2),(40,1215,160343,72,\'1\',\'14\',2),(41,1218,160407,2,\'8\',\'29\',2),(41,1218,160401,13,\'2\',\'21\',1),(41,1208,160169,46,\'1\',\'20\',2),(41,1208,160168,67,\'2\',\'20\',2),(42,1209,160192,34,\'1\',\'27\',1),(42,1209,160194,47,\'1\',\'26\',2),(42,1209,160196,61,\'8\',\'29\',2),(42,1203,160056,67,\'1\',\'74\',2),(42,1203,160065,89,\'2\',\'16\',2),(42,1203,160061,91,\'2\',\'22\',2),(42,1209,160207,92,\'1\',\'1-\',2),(43,1219,160431,2,\'2\',\'67\',2),(43,1211,160238,24,\'2\',\'24\',1),(43,1219,160436,41,\'1\',\'49\',1),(43,1211,160252,54,\'1\',\'36\',2),(43,1211,160245,89,\'2\',\'22\',2),(43,1219,160427,89,\'9\',\'31\',2),(43,1219,160421,89,\'7\',\'10\',2),(44,1210,160208,38,\'6\',\'16\',1),(44,1206,160137,47,\'1\',\'17\',2),(44,1210,160222,65,\'1\',\'11\',2),(45,1214,160318,2,\'2\',\'79\',2),(45,1213,160282,42,\'1\',\'32\',1),(45,1213,160281,66,\'1\',\'26\',2),(45,1214,160310,70,\'2\',\'10\',2),(45,1213,160290,89,\'6\',\'23\',2),(46,1224,160535,5,\'2\',\'14\',1),(46,1224,160533,16,\'2\',\'15\',1),(46,1224,160536,24,\'2\',\'18\',1),(46,1203,160061,59,\'2\',\'18\',2),(46,1224,160544,75,\'2\',\'89\',2),(46,1203,160050,85,\'1\',\'28\',2),(47,1211,160247,56,\'1\',\'25\',2),(47,1211,160238,57,\'2\',\'27\',2),(47,1211,160246,59,\'2\',\'18\',2),(47,1208,160168,90,\'2\',\'29\',2),(47,1208,160180,112,\'2\',\'12\',2),(48,1210,160221,58,\'2\',\'13\',2),(48,1207,160149,75,\'2\',\'57\',2),(49,1224,160540,8,\'3\',\'26\',1),(49,1224,160533,62,\'1\',\'22\',2),(49,1214,160303,71,\'1\',\'3-\',2),(49,1214,160322,72,\'2\',\'11\',2),(49,1224,160547,88,\'2\',\'22\',2),(50,1208,160177,1,\'2\',\'63\',1),(50,1208,160172,36,\'1\',\'51\',1),(50,1207,160143,43,\'8\',\'20\',1),(50,1208,160180,45,\'2\',\'64\',1),(50,1208,160173,50,\'2\',\'12\',2),(50,1207,160152,75,\'6\',\'29\',2),(51,1214,160304,34,\'1\',\'19\',1),(51,1214,160313,62,\'2\',\'11\',2),(51,1207,160149,80,\'5\',\'51\',2),(51,1214,160308,95,\'1\',\'24\',1),(51,1207,160153,97,\'2\',\'0-\',1),(51,1214,160318,98,\'2\',\'50\',1),(51,1207,160145,107,\'2\',\'21\',2),(51,1207,160155,115,\'2\',\'19\',2),(51,1214,160306,119,\'2\',\'27\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "player_booked" ("match_no","team_id","player_id","booking_time","sent_off","play_schedule","play_half") VALUES (51,1214,160302,122,\'9\',\'37\',2);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `match_details`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "match_details" ("match_no","play_stage","team_id","win_lose","decided_by","goal_score","penalty_score","ass_ref","player_gk") VALUES (1,\'G\',1207,\'W\',\'N\',2,NULL,80016,160140),(1,\'G\',1216,\'L\',\'N\',1,NULL,80020,160348),(2,\'G\',1201,\'L\',\'N\',0,NULL,80003,160001),(2,\'G\',1221,\'W\',\'N\',1,NULL,80023,160463),(3,\'G\',1224,\'W\',\'N\',2,NULL,80031,160532),(3,\'G\',1218,\'L\',\'N\',1,NULL,80025,160392),(4,\'G\',1206,\'D\',\'N\',1,NULL,80008,160117),(4,\'G\',1217,\'D\',\'N\',1,NULL,80019,160369),(5,\'G\',1222,\'L\',\'N\',0,NULL,80011,160486),(5,\'G\',1204,\'W\',\'N\',1,NULL,80022,160071),(6,\'G\',1213,\'W\',\'N\',1,NULL,80036,160279),(6,\'G\',1212,\'L\',\'N\',0,NULL,80029,160256),(7,\'G\',1208,\'W\',\'N\',2,NULL,80014,160163),(7,\'G\',1223,\'L\',\'N\',0,NULL,80006,160508),(8,\'G\',1219,\'W\',\'N\',1,NULL,80018,160416),(8,\'G\',1205,\'L\',\'N\',0,NULL,80012,160093),(9,\'G\',1215,\'D\',\'N\',1,NULL,80017,160324),(9,\'G\',1220,\'D\',\'N\',1,NULL,80010,160439),(10,\'G\',1203,\'L\',\'N\',0,NULL,80004,160047),(10,\'G\',1211,\'W\',\'N\',2,NULL,80007,160231),(11,\'G\',1202,\'L\',\'N\',0,NULL,80026,160024),(11,\'G\',1209,\'W\',\'N\',2,NULL,80028,160187),(12,\'G\',1214,\'D\',\'N\',1,NULL,80009,160302),(12,\'G\',1210,\'D\',\'N\',1,NULL,80015,160208),(13,\'G\',1217,\'L\',\'N\',1,NULL,80001,160369),(13,\'G\',1218,\'W\',\'N\',2,NULL,80002,160392),(14,\'G\',1216,\'D\',\'N\',1,NULL,80030,160348),(14,\'G\',1221,\'D\',\'N\',1,NULL,80032,160463),(15,\'G\',1207,\'W\',\'N\',2,NULL,80033,160140),(15,\'G\',1201,\'L\',\'N\',0,NULL,80027,160001),(16,\'G\',1206,\'W\',\'N\',2,NULL,80005,160117),(16,\'G\',1224,\'L\',\'N\',1,NULL,80013,160531),(17,\'G\',1223,\'L\',\'N\',0,NULL,80035,160508),(17,\'G\',1212,\'W\',\'N\',2,NULL,80034,160256),(18,\'G\',1208,\'D\',\'N\',0,NULL,80021,160163),(18,\'G\',1213,\'D\',\'N\',0,NULL,80024,160278),(19,\'G\',1211,\'W\',\'N\',1,NULL,80016,160231),(19,\'G\',1220,\'L\',\'N\',0,NULL,80020,160439),(20,\'G\',1205,\'D\',\'N\',2,NULL,80004,160093),(20,\'G\',1204,\'D\',\'N\',2,NULL,80007,160071),(21,\'G\',1219,\'W\',\'N\',3,NULL,80017,160416),(21,\'G\',1222,\'L\',\'N\',0,NULL,80010,160486),(22,\'G\',1203,\'W\',\'N\',3,NULL,80009,160047),(22,\'G\',1215,\'L\',\'N\',0,NULL,80015,160324),(23,\'G\',1210,\'D\',\'N\',1,NULL,80030,160208),(23,\'G\',1209,\'D\',\'N\',1,NULL,80032,160187),(24,\'G\',1214,\'D\',\'N\',0,NULL,80008,160302),(24,\'G\',1202,\'D\',\'N\',0,NULL,80019,160024),(25,\'G\',1216,\'L\',\'N\',0,NULL,80035,160348),(25,\'G\',1201,\'W\',\'N\',1,NULL,80034,160001),(26,\'G\',1221,\'D\',\'N\',0,NULL,80001,160463),(26,\'2\',1207,\'2\',\'7\',0,NULL,80002,160140),(27,\'2\',1217,\'2\',\'2\',0,NULL,80011,160369),(27,\'5\',1224,\'6\',\'1\',3,NULL,80022,160531),(28,\'9\',1218,\'7\',\'2\',0,NULL,80003,160392),(28,\'1\',1206,\'2\',\'2\',0,NULL,80023,160117),(29,\'2\',1223,\'1\',\'6\',0,NULL,80031,160508),(29,\'7\',1213,\'4\',\'1\',1,NULL,80025,160278),(30,\'2\',1212,\'1\',\'1\',0,NULL,80026,160256),(30,\'2\',1208,\'2\',\'1\',1,NULL,80028,160163),(31,\'5\',1205,\'9\',\'1\',0,NULL,80033,160093),(31,\'7\',1222,\'5\',\'1\',2,NULL,80027,160486),(32,\'1\',1204,\'8\',\'1\',2,NULL,80021,160071),(32,\'1\',1219,\'7\',\'7\',1,NULL,80024,160416),(33,\'9\',1210,\'6\',\'1\',2,NULL,80018,160208),(33,\'3\',1202,\'1\',\'6\',1,NULL,80012,160024),(34,\'5\',1209,\'1\',\'1\',3,NULL,80014,160187),(34,\'2\',1214,\'1\',\'1\',3,NULL,80006,160302),(35,\'2\',1211,\'8\',\'3\',0,NULL,80036,160233),(35,\'8\',1215,\'2\',\'2\',1,NULL,80029,160324),(36,\'2\',1220,\'5\',\'2\',0,NULL,80005,160439),(36,\'2\',1203,\'1\',\'2\',1,NULL,80013,160047),(37,\'2\',1221,\'2\',\'3\',1,4,80004,160463),(37,\'1\',1213,\'2\',\'3\',1,5,80007,160278),(38,\'3\',1224,\'1\',\'2\',1,NULL,80014,160531),(38,\'1\',1212,\'1\',\'2\',0,NULL,80006,160256),(39,\'1\',1204,\'2\',\'2\',0,NULL,80003,160071),(39,\'1\',1214,\'2\',\'7\',1,NULL,80023,160302),(40,\'5\',1207,\'3\',\'1\',2,NULL,80008,160140),(40,\'1\',1215,\'1\',\'1\',1,NULL,80019,160324),(41,\'3\',1208,\'1\',\'2\',3,NULL,80018,160163),(41,\'2\',1218,\'2\',\'2\',0,NULL,80012,160392),(42,\'2\',1209,\'1\',\'5\',0,NULL,80017,160187),(42,\'1\',1203,\'2\',\'1\',4,NULL,80010,160047),(43,\'1\',1211,\'2\',\'7\',2,NULL,80009,160231),(43,\'1\',1219,\'2\',\'8\',0,NULL,80015,160416),(44,\'5\',1206,\'1\',\'1\',1,NULL,80001,160117),(44,\'9\',1210,\'9\',\'2\',2,NULL,80002,160208),(45,\'0\',1213,\'2\',\'1\',1,3,80005,160278),(45,\'6\',1214,\'5\',\'1\',1,5,80013,160302),(46,\'2\',1224,\'2\',\'2\',3,NULL,80001,160531),(46,\'1\',1203,\'7\',\'4\',1,NULL,80002,160047),(47,\'1\',1208,\'7\',\'8\',1,6,80016,160163),(47,\'1\',1211,\'1\',\'1\',1,5,80020,160231),(48,\'1\',1207,\'1\',\'1\',5,NULL,80021,160140),(48,\'2\',1210,\'2\',\'1\',2,NULL,80024,160208),(49,\'2\',1214,\'1\',\'2\',2,NULL,80011,160302),(49,\'1\',1224,\'1\',\'2\',0,NULL,80022,160531),(50,\'1\',1207,\'1\',\'1\',2,NULL,80008,160140),(50,\'2\',1208,\'1\',\'2\',1,NULL,80019,160163);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
if ( !pg_Exec( $dbconn, 'INSERT INTO "match_details" ("match_no","play_stage","team_id","win_lose","decided_by","goal_score","penalty_score","ass_ref","player_gk") VALUES (51,\'1\',1214,\'2\',\'2\',1,NULL,80004,160302),(51,\'3\',1207,\'2\',\'1\',0,NULL,80007,160140);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `statements`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "statements" ("name") VALUES (\'This is SQL Exercise, Practice and Solution\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `team_coaches`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "team_coaches" ("team_id","coach_id") VALUES (1201,5550),(1202,5551),(1203,5552),(1204,5553),(1205,5554),(1206,5555),(1207,5556),(1208,5557),(1209,5558),(1210,5559),(1210,5560),(1211,5561),(1212,5562),(1213,5563),(1214,5564),(1215,5565),(1216,5566),(1217,5567),(1218,5568),(1219,5569),(1220,5570),(1221,5571),(1222,5572),(1223,5573),(1224,5574);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `temp`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "temp" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3007,\'Brad Davis\',\'New York\',200,5001),(3003,\'Jozy Altidor\',\'Moncow\',200,5007),(3005,\'Graham Zusi\',\'California\',200,5002),(3008,\'Julian Green\',\'London\',300,5002),(3004,\'Fabian Johnson\',\'Paris\',300,5006),(3009,\'Geoff Cameron\',\'Berlin\',100,5003),(3001,\'Brad Guzan\',\'London\',NULL,5005);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `tempa`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "tempa" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3005,\'Graham Zusi\',\'California\',200,5002),(3001,\'Brad Guzan\',\'London\',100,5005),(3004,\'Fabian Johns\',\'Paris\',300,5006),(3007,\'Brad Davis\',\'New York\',200,5001),(3009,\'Geoff Camero\',\'Berlin\',100,5003),(3008,\'Julian Green\',\'London\',300,5002),(3003,\'Jozy Altidor\',\'Moncow\',200,5007);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `tempcustomer`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "tempcustomer" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3005,\'Graham Zusi\',\'California\',200,5002),(3001,\'Brad Guzan\',\'London\',100,5005),(3004,\'Fabian Johns\',\'Paris\',300,5006),(3007,\'Brad Davis\',\'New York\',200,5001),(3009,\'Geoff Camero\',\'Berlin\',100,5003),(3008,\'Julian Green\',\'London\',300,5002),(3003,\'Jozy Altidor\',\'Moncow\',200,5007);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `temphi`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "temphi" ("salesman_id","name","city","commission") VALUES (5001,\'James Hoog\',\'New York\',0.15),(5002,\'Nail Knite\',\'Paris\',0.13),(5005,\'Pit Alex\',\'London\',0.11),(5006,\'Mc Lyon\',\'Paris\',0.14),(5003,\'Lauson Hen\',\'San Jose\',0.12),(5007,\'Paul Adam\',\'Rome\',0.13);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `tempp`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "tempp" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3007,\'Brad Davis\',\'New York\',200,5001),(3003,\'Jozy Altidor\',\'Moncow\',200,5007),(3005,\'Graham Zusi\',\'California\',200,5002),(3008,\'Julian Green\',\'London\',300,5002),(3004,\'Fabian Johnson\',\'Paris\',300,5006),(3009,\'Geoff Cameron\',\'Berlin\',100,5003),(3001,\'Brad Guzan\',\'London\',NULL,5005);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `tempp11`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "tempp11" ("customer_id","cust_name","city","grade","salesman_id") VALUES (3002,\'Nick Rimando\',\'New York\',100,5001),(3007,\'Brad Davis\',\'New York\',200,5001),(3003,\'Jozy Altidor\',\'Moncow\',200,5007),(3005,\'Graham Zusi\',\'California\',200,5002),(3008,\'Julian Green\',\'London\',300,5002),(3004,\'Fabian Johnson\',\'Paris\',300,5006),(3009,\'Geoff Cameron\',\'Berlin\',100,5003),(3001,\'Brad Guzan\',\'London\',NULL,5005);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `tempsalesman`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "tempsalesman" ("salesman_id","name","city","commission") VALUES (5001,\'James Hoog\',\'New York\',0.15),(5002,\'Nail Knite\',\'Paris\',0.13),(5005,\'Pit Alex\',\'London\',0.11),(5006,\'Mc Lyon\',\'Paris\',0.14),(5003,\'Lauson Hen\',\'San Jose\',0.12),(5007,\'Paul Adam\',\'Rome\',0.13);' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `testtable`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "testtable" ("col1") VALUES (\'A001/DJ-402\\44_/100/2015\'),(\'A001_\\DJ-402\\44_/100/2015\'),(\'A001_DJ-402-2014-2015\'),(\'A002_DJ-401-2014-2015\'),(\'A001/DJ_401\'),(\'A001/DJ_402\\44\'),(\'A001/DJ_402\\44\\2015\'),(\'A001/DJ-402%45\\2015/200\'),(\'A001/DJ_402\\45\\2015%100\'),(\'A001/DJ_402%45\\2015/300\'),(\'A001/DJ-402\\44\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `vowl`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "vowl" ("cust_name","substring") VALUES (\'Nick Rimando\',\'o\'),(\'Graham Zusi\',\'i\'),(\'Brad Guzan\',\'n\'),(\'Fabian Johns\',\'s\'),(\'Brad Davis\',\'s\'),(\'Geoff Camero\',\'o\'),(\'Julian Green\',\'n\'),(\'Jozy Altidor\',\'r\');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
// Dumping data for table `zebras`.
if ( !pg_Exec( $dbconn, 'INSERT INTO "zebras" ("id","score","date") VALUES (1,100,'2015-12-15 00:00:00'),(3,230,'2015-12-15 00:00:00');' ))
{
	echo 'Error:'.pg_errormessage( $dbconn );
	pg_close($dbconn); exit;
}
?>

