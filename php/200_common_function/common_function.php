<?php
    // array
    // isset
    // define
    // empty
    // assert
    print_r (assert(2 > 1));
    echo "</br>";
    //file
    /*$var = file('http://example.com');
    var_dump($var);
    print_r($var);*/
    // implode
    $array = array("2","a1",2,"5","alkjfl");
    $text = implode("hello", $array);
   var_dump($text);
    //explode
    $expl = explode("hello",$text,4);
    print_r($expl);
    //count
    echo count("hello",1);
    echo "<br>";
    //ord
    echo ord("h");// 1character
    echo "<br>";
    //dir

    $d = dir($_SERVER['DOCUMENT_ROOT']."/Training");
    echo "Handle: " . $d->handle . "\n";
    echo "Path: " . $d->path . "\n";
    while (false !== ($entry = $d->read())) {
        echo $entry."\n";
    }
    $d->close();
    echo "<br>";
    // pos
    echo pos($array);
    echo current($array);
    print_r(each($array));
    echo end ($array);
    echo key($array);
    // exp (a math operator)
// log (a math operator)
    //list
    list($a,$b,$c) = $array;
    print_r($a.$b.$c);
    echo "<br>";
    //header
    /*header('Location: http://google.com.vn');*/
    //chr convert an ascii to character
    echo chr('110');
    //defined return 1 or 0
    //unset: can't destroy static variable
    function foo()
    {
        static $bar;
        $bar++;
        echo "Before unset: $bar, ";
        unset($bar);
        $bar = 23;
        echo "after unset: $bar\n";
    }

    foo();
    foo();
    foo();
    echo '<br>';
    //exec ??
    echo exec("junction");
    //in_array: return true or false for check string in array
    // it just evaluate the value (no include type of data)
    if (in_array(5,$array)){
        echo 'yes';
    } else {
        echo 'no';
    }
    // trim: remove characters at both sides of a sting
    echo trim("hello","h");
    //intval: get integer of a float
    echo intval("5.2");
    //split: like str_split()
    //mail
    mail("nguyenbach.neu@gmail.com","nguyen_rua","Chuc ban may man lan sau","From: God");
    //trigger_error: generate a user-level error
    //pack ??
    //eval
    $string = "cup";
    $time = "10a.m";
    $text = 'I drink a $string of coffee at $time';
    eval("\$text =\"$text\";");
    echo $text;
    //date_default_timezone_set
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    echo date('Y-m-d H:i:s',time());
    //serialize??
    //extract
    //settype convert type of variable
    //getopt ??
    //urlencode: convert strign to url type
    $username = "*Bach$";
    echo urlencode($username);
    // copy: output 1;
    //get_class: get class name of an object;
    // call_user_function and function($var) ???
    // call_user_function_array
    //basename
    $dr = "/training/php/curl.php";
    echo basename($dr);
    //glob
    print_r(glob('*.*'));

    // gettext: ??
    /*// Set language to German
    putenv('LC_ALL=de_DE');
    setlocale(LC_ALL, 'de_DE');

    // Specify location of translation tables
    bindtextdomain("myPHPApp", "./locale");

    // Choose domain
    textdomain("myPHPApp");

    // Translation is looking for in ./locale/de_DE/LC_MESSAGES/myPHPApp.mo now
    echo gettext("Welcome to My PHP Application");
    echo _("Have a nice day");*/

    // ini_get: get the value of option
    // stat get infor of a file
    // flush ??
    //chmod: change permisson of a file
    // array_map
    $var = array(2,5,9,14);
    function multiple($var){
        return ($var * 2);
    }
    print_r(array_map("multiple",$var));
    //method_exits: check a method of an object
    //ini_set: set value for option
    //range
    print_r(range(0,10));
    //mkdir creat directory
    mkdir('directory');
    //extension_loaded
    // error_log
    //realpath
    echo crypt($array[0],time());
    //preg_replace_callback
    //error_reporting
    //setcookie
    $boo = setcookie("user","bach",time()+3600000);
    if($boo){
        echo 'true';
    }
    //pow tinh so mu
    //ob_get_content;
    ob_start();
    echo "your name";
    $out = ob_get_contents();
    ob_end_clean();
    // get_object_vars: get information of vars in an object
    //number_format
    number_format ("10",2,",",".");
    //getenv
    var_dump(getenv($var));
    //compact
    $a = 10;
    var_dump(compact("a"));
    // strcmp: compare the value of 2 strings
    // filemtime: the last time file eddited
    // parse_url: create an array from url (8 elements)
    // array_unshift: insert new elements to array
    //get_current_user
    echo get_current_user();
    //
?>