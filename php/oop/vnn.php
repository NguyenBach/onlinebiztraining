<?php

    class vnn_crawler extends base_method  {
        function get_title($resource){
            $start = strpos($resource,'<h1 class="title">');
            $end = strrpos($resource,'</h1>');
            $result = strip_tags(substr($resource,$start,$end-$start));
            return $result;
        }

        function get_date($resource){
            $start = strpos($resource,'<span class="ArticleDate">');
            $end = strpos($resource,'<div id="ArticleContent"');
            $datetime= strip_tags(substr($resource,$start,$end-$start));
            $datetime = htmlentities($datetime, null, 'utf-8');
            $datetime = str_replace("&nbsp;", " ", $datetime);
            $datetime = trim($datetime);
            $datetime = explode(" ",$datetime);
            $result[0] = $datetime[0];
            $result[1] = $datetime[2];
            return $result;
        }

        function get_content($resource){
            $start = strpos($resource,'<div id="ArticleContent"');
            $end = strpos($resource,'<div class="m-b-10"><div class="m-t-10 bo-t m-b-10 clearfix"><ul class="l-s-0 t-c list-btn-detail">');
            $content = strip_tags(substr($resource,$start,$end-$start));
            $content = trim($content);
            $content = htmlentities($content, null, 'utf-8');
            $result = str_replace("&nbsp;", " ", $content);
            return $result;
        }

        function get_image($resource){

            $start = strpos($resource,'<div id="ArticleContent"');
            $end = strpos($resource,'<div class="m-b-10"><div class="m-t-10 bo-t m-b-10 clearfix"><ul class="l-s-0 t-c list-btn-detail">');
            $result = substr($resource,$start,$end-$start);
            return $result;
        }
    }


?>