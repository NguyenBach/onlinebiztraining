<?php

    class vne_crawler extends base_method  {
        function get_title($resource){

            $start = strpos($resource,'<h1');
            $end = strrpos($resource,'</h1>');
            $result = strip_tags(substr($resource,$start,$end-$start));
            return $result;
        }

        function get_date($resource){
            $start = strpos($resource,'<div class="block_timer left txt_666">');
            $end = strpos($resource,'<div class="block_share right">');
            $datetime= strip_tags(substr($resource,$start,$end-$start));
            $datetime = str_replace("|","",$datetime);
            $datetime = trim($datetime);
            $datetime = explode(" ",$datetime);
            $result[0] = $datetime[2];
            $result[1] = $datetime[4];
            return $result;
        }

        function get_content($resource){
            $start = strpos($resource,'<h3 class="short_intro txt_666">');
            $end = strpos($resource,'<div class="xemthem_new_ver width_common">');
            $content = strip_tags(substr($resource,$start,$end-$start));
            $content = trim($content);
            $content = htmlentities($content, null, 'utf-8');
            $result = str_replace("&nbsp;", " ", $content);
            return $result;
        }

        function get_image($resource){

            $start = strpos($resource,'<h3 class="short_intro txt_666">');
            $end = strpos($resource,'<div class="xemthem_new_ver width_common">');
            $result = substr($resource,$start,$end-$start);
            return $result;
        }
    }

?>