<?php

    class base_method {


        function fetch($url){
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);
            if (empty($result)){
                $result = "The link can't be executed";
            }
            return $result;
        }

        function parse_title($resource){

            $result = trim($resource);
            return $result;
        }

        function parse_date($resource){
            $date = explode("/", $resource[0]);
            $date = $date[2]."-".$date[1]."-".$date[0];
            $time = $resource[1].":00";
            $result = $date." ".$time;
            return $result;
        }

        function parse_content($resource){
            return $resource;
        }

        function parse_image($resource){

            preg_match_all('/<img[^>]+>/i',$resource, $imgs);
            if (empty($imgs[0])){
                return $result ="null";
            } else {
                foreach($imgs[0] as $img){
                    $url_pattern = '/((http|https)\:\/\/)[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/';
                    preg_match($url_pattern,$img,$src);
                    $src = explode("?",$src[0]);

                    $fp = fopen('news_img/'.basename($src[0]),'w+');
                    $ch = curl_init($src[0]);
                    curl_setopt($ch, CURLOPT_URL, $src[0]);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.1 Safari/537.11');
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    $data = curl_exec($ch);
                    curl_close($ch);
                    if(empty($data)){
                        if(empty($result[0])){
                            $result[0] = basename($src[0]);
                        } else {
                            $result[0] .= ", ".basename($src[0]);
                        }
                    } else{
                        if(empty($result[1])){
                            $result[1] = basename($src[0]);

                        } else {
                            $result[1] .= ", ".(string)basename($src[0]);

                        }
                    }
                }
                return $result;
            }
        }

        function save_data($data,$info){

            $title = $this->parse_title($this->get_title($data));
            $datetime = $this->parse_date($this->get_date($data));
            $content = $this->parse_content($this->get_content($data));
            $image = $this->parse_image($this->get_image($data))[1];
            $conn = new PDO("mysql:host=$info[0];dbname=$info[1]",$info[2],$info[3]);
            $conn->setAttribute(PDO::ATTR_ERRMODE, 2);

            $query =$conn->prepare( "INSERT INTO info(title,Datetime,Content,Image) VALUES (:title,:datetime,:content,:image)");
            $query->bindValue(':title', $title, PDO::PARAM_STR);
            $query->bindValue(':datetime', $datetime);
            $query->bindValue(':content', $content, PDO::PARAM_STR);
            $query->bindValue(':image', $image);
            $result = $query->execute();
            $conn = null;
            return $result;

        }

       /* function show_result($limit,$info){
            $conn = new PDO("mysql:host=$info[0];dbname=$info[1]",$info[2],$info[3]);
            $conn->setAttribute(PDO::ATTR_ERRMODE, 2);

            $query =$conn->prepare( "SELECT * FROM info ORDER BY No DESC LIMIT $limit");
            $query->execute();
            $result = $query->setFetchMode(PDO::FETCH_ASSOC);
            $result = $query->fetchAll();

            return $result;
        }*/
    }
?>