<html>
    <head></head>
    <body>
        <label>SEARCH</label>
        <input type="text" name="search" onkeyup="sug(this.value)">
        <label style="display: inline">Suggest</label> <div id = "suggest"></div>


    </body>
    <script>

        function sug(char){
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200){
                    var table = "<table border='1'> <tr> <td>STT</td><td> Artist</td></tr>";
                    var data = this.responseText;
                    data = JSON.parse(data);

                    for(i in data){
                        stt = parseInt(i) + 1;
                        table += "<tr> <td>" + stt + "</td><td>" + data[i]+"</td></tr>";
                    }
                    document.getElementById('suggest').innerHTML = table;
                }
            };
            request.open("GET","ajax_php.php?char=" + char,true);
            request.send();
        }



    </script>
</html>
