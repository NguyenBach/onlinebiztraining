<html>
    <head></head>
    <body>
        <div id="demo"></div>
        <button onclick="load_table()">CLICK</button>
        <script>
            function load_table() {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function () {
                    if ((this.readyState == 4) && (this.status == 200)) {
                        var data = JSON.parse(this.responseText);
                        var table = "<table border='1'> <tr> <td>STT</td> <td>Title</td> <td> Artist</td></tr>"

                        for (i = 0; i < data.CD.length; i++ ){
                            table += "<tr> <td>" + i +"</td>";
                            table += "<td>" + data.CD[i].TITLE + "</td>";
                            table += "<td>" + data.CD[i].ARTIST + "</td>";
                            table += "</tr>";
                        }
                        table += "</table>";
                        document.getElementById('demo').innerHTML = table;
                    }
                }
                request.open("GET", "xml_handle.php", true);
                request.send();
            }
        </script>
    </body>
</html>
