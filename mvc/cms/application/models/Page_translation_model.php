
3
4
5
6
7
8
9
10
11
12
13
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Page_translation_model extends MY_Model
{

    public function __construct()
    {
        $this->has_one['page'] = array('Page_model','id','page_id');
        parent::__construct();
    }
}