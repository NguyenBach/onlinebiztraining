<?php
class MY_Controller extends CI_Controller
{
    protected $data = array();
    function __construct()
    {
        parent::__construct();
        $this->data['page_title'] = 'CI App';
        $this->data['before_head'] = '';
        $this->data['before_body'] ='';
        $this->load->model('language_model');
        $languages = $this->language_model->get_all();
        $this->data['languages'] = $languages;
        if($languages !== FALSE)
        {
            foreach($languages as $language)
            {

                $this->langs[$language->slug] = array(
                    'id'=>$language->id,
                    'slug'=>$language->slug,
                    'name'=>$language->language_name,
                    'language_directory'=>$language->language_directory,
                    'language_code'=>$language->language_code,
                    'alternate_link'=>'/'.$language->slug,
                    'default'=>$language->default);

                if($language->default == '1')
                {
                    $_SESSION['default_lang'] = $language->slug;
                    $this->default_lang = $language->slug;
                    $this->langs[$language->slug]['alternate_link'] = '';
                }
            }
        }


        /*$lang_slug = $this->uri->segment(1);
        var_dump($lang_slug);
        die('');
        if(isset($lang_slug) && array_key_exists($lang_slug, $this->langs))
        {
            $set_language = $lang_slug;
        }
        else
        {
            $set_language = $default_language;
        }

        if(isset($set_language))
        {
            $this->load->library('session');
            $_SESSION['set_language'] = $set_language;
        }*/
        $this->data['langs'] = $this->langs;
    }
    protected function render($the_view = NULL, $template = 'master')
    {
        if($template == 'json' || $this->input->is_ajax_request())
        {
            header('Content-Type: application/json');
            echo json_encode($this->data);
        }
        else
        {
            $this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view($the_view,$this->data, TRUE);;
            $this->load->view('templates/'.$template.'_view', $this->data);
        }
    }
}

class Admin_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in())
        {
            //redirect them to the login page
            redirect('admin/user/login', 'refresh');
        }
        $current_user = $this->ion_auth->user()->row();
        $this->user_id = $current_user->id;
        $this->data['current_user'] = $current_user;
        $this->data['current_user_menu'] = '';
        if($this->ion_auth->in_group('admin'))
        {
            $this->data['current_user_menu'] = $this->load->view('templates/_part/user_menu_admin_view.php', NULL, TRUE);
        }
        $this->data['page_title'] = 'CI App - Dashboard';
    }

    protected function render($the_view = NULL, $template = 'admin_master')
    {
        parent::render($the_view, $template);
    }
}

class Public_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }
}
?>