<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php
$this->load->view('templates/_part/admin_master_header_view'); ?>

    <div class="container">
        <div class="main-content" style="padding-top:40px;">
            <?php echo $the_view_content; ?>
        </div>
    </div>

<?php $this->load->view('templates/_part/admin_master_footer_view');?>