<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<div class="container" style="margin-top:60px;">
    <div class="row">
        <div class="col-lg-12">
            <!-- Single button -->
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Add page <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <?php
                    foreach($langs as $slug => $language)
                    {
                        echo '<li>'.anchor('admin/pages/create/'.$slug,$language['name']).'</li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="margin-top: 10px">
            <?php
            echo '<table class="table table-hover table-bordered table-condensed">';
            echo '<tr>';
            echo '<td >ID</td>';
            echo '<td >Page title</td>';
            echo '<td >Translations</td>';
            echo '<td >Created at</td>';
            echo '<td >Last update</td>';
            echo '<td >Operations</td>';
            echo '</tr>';

            if(!empty($pages))
            {
                foreach($pages as $page_id => $page)
                {
                    echo '<tr>';
                    echo '<td>'.$page_id.'</td><td>'.$page['title'].'</td>';
                    echo '<td>'.$page['translations'].'</td>>';
                    echo '<td>'.$page['created_at'].'</td>';
                    echo '<td>'.$page['updated_at'].'</td>';
                    echo '<td>'.anchor('admin/pages/delete/all/'.$page_id,'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>','onclick="return confirm(\'Are you sure you want to delete?\')"').'</td>';
                    echo '</tr>';
                }
            }
            echo '</table>';

            ?>
        </div>
    </div>
</div>