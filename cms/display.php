
<html lang="en">

  <head>
    <title>SimpleCMS</title>
  </head>

  <body>

  </body>

</html>

<?php

    include_once('simpleCMS.php');
    $obj = new simpleCMS();
    $obj->host = 'localhost';
    $obj->username = 'root';
    $obj->password = '';
    $obj->table = 'test';
    $obj->connect();

    if ( $_POST )
        $obj->write($_POST);

    echo ( $_GET['admin'] == 1 ) ? $obj->display_admin() : $obj->display_public();

?>