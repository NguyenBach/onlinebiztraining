<?php
    class simpleCMS{
        var $host;
        var $username;
        var $password;
        var $table;
        var $conn;

        public function display_public(){
            return

               ' <form action="{$_SERVER[\'PHP_SELF\']}" method="post">
                  <label for="title">Title:</label>
                  <input name="title" id="title" type="text" maxlength="150" />
                  <label for="bodytext">Body Text:</label>
                  <textarea name="bodytext" id="bodytext"></textarea>
                  <input type="submit" value="Create This Entry!" />
                </form>';
        }

        public function display_admin(){
            $q =$conn->prepare( "SELECT * FROM testDB ORDER BY created DESC LIMIT 3");
            $r = $q->execute();

            if ( $r !== false && $r > 0 ) {
                while ( $a = $q->fetchALL() ) {
                    $title = stripslashes($a['title']);
                    $bodytext = stripslashes($a['bodytext']);

                    $entry_display .= "<h2>$title</h2> <p> $bodytext </p>";


                }
            } else {
                $entry_display = '<h2>This Page Is Under Construction</h2>
                                    <p>
                                      No entries have been made on this page. 
                                      Please check back soon, or click the
                                      link below to add an entry!
                                    </p>';

            }
            $entry_display .= " <p class='admin_link'>
                                <a href=".$_SERVER['PHP_SELF']."?admin=1>Add a New Entry</a>
                                </p>";
            return $entry_display;
        }

        public function write(){
            if ( $p['title'] )
                $title = $conn->quote($p['title']);
            if ( $p['bodytext'])
                $bodytext =  $conn->quote($p['bodytext']);
            if ( $title && $bodytext ) {
                $created = time();
                $sql =$conn->prepare( "INSERT INTO testDB VALUES('$title','$bodytext','$created')");
                return $sql->execute();
            } else {
                return false;
            }
        }

        public function connect(){

            $conn = new PDO("mysql:host=$this->host;dbname=$this->table",$this->username,$this->password) or die("Could not connect. ");
            $this->conn = $conn;
            $conn->setAttribute(PDO::ATTR_ERRMODE, 2);
            return $this->buildDB();
        }

        private function buildDB(){
            $sql = $this->conn->prepare( "CREATE TABLE IF NOT EXISTS testDB(
                      title VARCHAR(150),
                      bodytext TEXT,
                      created VARCHAR(100)
                    )");
            return $sql->execute();
        }
    }
?>