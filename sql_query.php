<html>
<head>
    <link rel="stylesheet" type="text/css" href="js/oop/css/bootstrap.min.css">
    <style>
        .table{
            position: absolute;
            top: 200px;
        }
    </style>
</head>
<body>
    <?php
    $severname = "localhost";
    $username = "root";
    $pasword = "";
    $db_name = "movie";
    $conn = new PDO("mysql:host=$severname;dbname=$db_name",$username,$pasword);

    $query = $conn->prepare("SELECT movie.mov_id, movie.mov_title, rating.rev_stars FROM movie LEFT JOIN rating ON movie.mov_id = rating.mov_id WHERE rating.rev_stars > 7");
    $query->execute();
    $results = $query->setFetchMode(PDO::FETCH_ASSOC);
    $results = $query->fetchAll();

    $table = '<table class="table table-hover">';
    foreach ($results as $result){
        $table .= '<tr>';
        foreach ($result as $key){
            $table .= '<td>'.$key.'</td>';
        }
        $table .= '</tr>';
    }
    $table .= '</table>';
    echo $table;
    ?>
</body>
</html>
